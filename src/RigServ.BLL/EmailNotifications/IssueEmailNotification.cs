﻿using System.Data;
using System.Threading.Tasks;
using System.Configuration;
using Mailer.Abstractions;
using RigServe.DAL;
using System;

namespace RigServ.BLL.EmailNotifications
{
   public class IssueEmailNotification : BaseEmailNotification
    {
        private DataTable ToDetails;
        public DataTable ResultDT { get; set; }
        public string FacilityId { get; set; }
        public string Items { get; set; }
        private string FacilityName { get; set; }
        public override async Task Send()
        {
            this.EmailMessage.EntityType = "ISSUE";
            this.EmailMessage.EntityId = FacilityId;
            this.TemplateCode = "ISSUE_NOTIFICATION";
            this.UserName = "System";
            this.PopulateFromTemplate();
            await base.Send();
        }



        public override void GetData()
        {
            base.GetData();

            if (ResultDT != null && ResultDT.Rows.Count == 1)
            {
                Items = ResultDT.Rows[0]["IssueItems"].ToString();
                FacilityName = ResultDT.Rows[0]["Description"].ToString();

                  ToDetails = FacilityDB.GetFacility(FacilityId);

                this.EmailInfo.Add("ITEMS", Items);
                this.EmailInfo.Add("FACILITY_NAME", FacilityName);
                this.EmailInfo.Add("DATE", DateTime.Now.ToLongDateString());
            }

        }

        public override void PopulateFromAddress()
        {
            base.PopulateFromAddress();

            EmailMessage.From.Address = "DoNotReply@RigServ.com";
            EmailMessage.From.DisplayName = "RigServ Automatic Email";
        }

        public override void PopulateToAddress()
        {
            base.PopulateToAddress();

            foreach (DataRow row in ToDetails.Rows)
            {
                var toAddress = new EmailAddress();
                toAddress.Address = row["EmailAddress"].ToString();
                toAddress.DisplayName = row["Description"].ToString();
                EmailMessage.To.Add(toAddress);
            }

        }
    }
}
