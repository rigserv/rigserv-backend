﻿using RigServe.DAL;
using Mailer.Abstractions;
using Mailer.Sql;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace RigServ.BLL.EmailNotifications
{
    public class BaseEmailNotification
    {
        private Dictionary<string, object> _emailInfo = new Dictionary<string, object>();
        public Dictionary<string, object> EmailInfo
        {
            get { return _emailInfo; }
            set { _emailInfo = value; }
        }

        private List<String> _DocumentIDs = new List<string>();
        public List<String> DocumentIDs
        {
            get { return _DocumentIDs; }
            set { _DocumentIDs = value; }
        }


        public string UserName
        {
            get;
            set;
        }

        public string TemplateCode
        {
            get;
            set;
        }

        public string TestHeader { get; set; }
        public bool IncludeTestHeaderAsReplacementToken { get; set; }

        public DbTransaction Transaction
        {
            get;
            set;
        }

        private EmailMessage _EmailMessage = new EmailMessage();
        public EmailMessage EmailMessage
        {
            get { return _EmailMessage; }
            set { _EmailMessage = value; }
        }

        public virtual void PopulateFromAddress()
        {
            _EmailMessage.From.Address = GetAdminEmailAddress();
            _EmailMessage.From.DisplayName = GetAdminEmailName();
        }

        public string GetAdminEmailAddress()
        {
            return ConfigurationManager.AppSettings["AdminMailboxEmail"];
        }

        public string GetAdminEmailName()
        {
            return ConfigurationManager.AppSettings["AdminMailboxName"];
        }

        public virtual void PopulateToAddress()
        {
        }

        public virtual void PopulateCCAddress()
        {
        }

        public virtual void PopulateBCCAddress()
        {
        }

        public virtual void PopulateAttachments()
        {
        }

        public virtual void PopulateFromTemplate()
        {
            if (!String.IsNullOrEmpty(this.TemplateCode))
            {
                DataTable dt = MailDB.GetTemplateByCode(this.TemplateCode);

                if (dt.Rows.Count == 1)
                {
                    DataRow row = dt.Rows[0];

                    this.EmailMessage.Subject = row["TemplateSubject"].ToString();
                    this.EmailMessage.Body = row["TemplateBody"].ToString();
                    this.EmailMessage.Priority = (MailPriority)Enum.Parse(typeof(MailPriority), row["Priority"].ToString(), true);
                    this.EmailMessage.IsBodyHtml = Convert.ToBoolean(row["IsBodyHTML"]);
                }

            }
        }



        public virtual async Task Send()
        {
            try
            {

                GetData();
                PopulateFromAddress();
                PopulateToAddress();
                PopulateCCAddress();
                PopulateBCCAddress();
                PopulateAttachments();
                BuildSubject();
                BuildBody();
                await SendEmailToQueue();


            }
            catch (Exception ex)
            {
                //Log.LogException(ex, False);
                throw new Exception(String.Format("Error occured while sending email notification - {0} - {1}", this.TemplateCode, ex.Message), ex);
            }
        }

        private async Task SendEmailToQueue()
        {
            EmailMessage.Template = TemplateCode;

            if (ConfigurationManager.AppSettings["InTestMode"] == "1")
            {
                //prepend body with the following message
                EmailMessage.Body = DebugEmailHeader() + EmailMessage.Body;
            }

            if (ConfigurationManager.AppSettings["InTestMode"] == "1")
            {
                EmailMessage.From.Address = "support@eminenttechnology.com";
            }

            if (ConfigurationManager.AppSettings["InTestMode"] == "1")
            {

                EmailMessage.To.Clear();
                EmailMessage.CC.Clear();
                EmailMessage.BCC.Clear();
                EmailMessage.ReplyTo.Clear();

                string testEmailGroup = "EMAIL_TEST_GROUP";

                DataTable dtTestGroup = LookupDB.GetActiveItemsByType(testEmailGroup);

                foreach (DataRow row in dtTestGroup.Rows)
                {
                    if (!(string.IsNullOrEmpty(row["LookupValue"].ToString())))
                    {
                        EmailMessage.To.Add(new EmailAddress(row["LookupValue"].ToString(), row["LookupDescription"].ToString()));
                    }
                }

            }

            IEmailQueue queue = new SqlQueue("name=MS_TableConnectionString");
            await queue.QueueMessage(EmailMessage);
        }

        private string DebugEmailHeader()
        {
            StringBuilder retval = new StringBuilder();

            retval.AppendLine("<br>FOR TEST PURPOSES ONLY:");
            retval.AppendLine("<br>-------------------------------------------------");
            retval.AppendLine("<br>TO:");

            foreach (EmailAddress address in this.EmailMessage.To)
                retval.AppendFormat("<br/>{0}", address.Address);

            retval.AppendLine("<br>CC:");

            foreach (EmailAddress address in this.EmailMessage.CC)
                retval.AppendFormat("<br/>{0}", address.Address);

            retval.AppendLine("<br>BCC:");

            foreach (EmailAddress address in this.EmailMessage.BCC)
                retval.AppendFormat("<br/>{0}", address.Address);

            retval.AppendLine("<br>-------------------------------------------------<br>");

            return retval.ToString();
        }

        public virtual void BuildSubject()
        {
            StringBuilder subject = new StringBuilder(EmailMessage.Subject);

            foreach (string key in _emailInfo.Keys)
            {
                var val = _emailInfo[key];

                if (val == null)
                {
                    val = String.Empty;
                }

                subject.Replace("{" + key + "}", val.ToString());
            }

            this.EmailMessage.Subject = subject.ToString();
        }

        public virtual void BuildBody()
        {
            StringBuilder body = new StringBuilder(EmailMessage.Body);

            foreach (string key in _emailInfo.Keys)
            {
                var val = _emailInfo[key];

                if (val == null)
                {
                    val = String.Empty;
                }

                body.Replace("{" + key + "}", val.ToString());
            }

            this.EmailMessage.Body = body.ToString();
        }



        public virtual void GetData()
        {

        }

    }
}
