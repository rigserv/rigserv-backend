﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using RigServe.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RigServ.BLL
{
    public class RigServLoad
    {

        public static void LoadStagingData()
        {
            var InventoryStage = InventoryStageDB.GetInventoryStageData();
            var ItemImagesStage = ItemImagesStageDB.GetItemImagesStageData();

            Database db = DatabaseFactory.CreateDatabase("MS_TableConnectionString");
            DbTransaction tran = null;

            using (DbConnection connection = db.CreateConnection())
            {
                connection.Open();

               // Delete and perform InventoryStage bulk load
               try
                {
                    tran = connection.BeginTransaction();

                    InventoryStageDB.DeleteData(db, tran);

                    InventoryStageDB.LoadInventoryStageData(connection, tran, InventoryStage);

                    tran.Commit();
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    throw ex;
                }

                // Delete and perform InventoryStage bulk load
                try
                {
                    tran = connection.BeginTransaction();

                    ItemImagesStageDB.DeleteData(db, tran);

                    ItemImagesStageDB.LoadItemImagesStageData(connection, tran, ItemImagesStage);

                    tran.Commit();
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    throw ex;
                }
                
            }
        }
        
        public static void CategoryStageLoad()
        {
            Database db = DatabaseFactory.CreateDatabase("MS_TableConnectionString");
            DbTransaction tran = null;

            using (DbConnection connection = db.CreateConnection())
            {
                connection.Open();

                //load into Category table
                try
                {
                    tran = connection.BeginTransaction();

                    CategoryStageDB.LoadData(db, tran);

                    tran.Commit();
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    throw ex;
                }
            }
        }

        public static void CategoryLoad()
        {
            Database db = DatabaseFactory.CreateDatabase("MS_TableConnectionString");
            DbTransaction tran = null;

            using (DbConnection connection = db.CreateConnection())
            {
                connection.Open();
                
                //load into Category table
                try
                {
                    tran = connection.BeginTransaction();

                    CategoryDB.LoadData(db, tran);

                    tran.Commit();
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    throw ex;
                }
            }
        }

        public static void FacilityLoad()
        {
            Database db = DatabaseFactory.CreateDatabase("MS_TableConnectionString");
            DbTransaction tran = null;


            using (DbConnection connection = db.CreateConnection())
            {
                connection.Open();
                //load into Facility table
                try
                {
                    tran = connection.BeginTransaction();

                    FacilityDB.LoadData(db, tran);

                    tran.Commit();
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    throw ex;
                }
            }
        }

        public static void LocationLoad()
        {
            Database db = DatabaseFactory.CreateDatabase("MS_TableConnectionString");
            DbTransaction tran = null;

            using (DbConnection connection = db.CreateConnection())
            {
                connection.Open();
                //load into Location table
                try
                {
                    tran = connection.BeginTransaction();

                    LocationDB.LoadData(db, tran);

                    tran.Commit();
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    throw ex;
                }
            }
        }

        public static void ProductLoad()
        {
            Database db = DatabaseFactory.CreateDatabase("MS_TableConnectionString");
            DbTransaction tran = null;

            using (DbConnection connection = db.CreateConnection())
            {
                connection.Open();
                //load into Product table
                try
                {
                    tran = connection.BeginTransaction();

                    ProductDB.LoadData(db, tran);

                    tran.Commit();
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    throw ex;
                }
            }
        }

        public static void ProductPhotoLoad()
        {
            Database db = DatabaseFactory.CreateDatabase("MS_TableConnectionString");
            DbTransaction tran = null;

            using (DbConnection connection = db.CreateConnection())
            {
                connection.Open();
                //load into Product table
                try
                {
                    tran = connection.BeginTransaction();

                    ProductPhotoDB.LoadData(db, tran);

                    tran.Commit();
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    throw ex;
                }
            }
        }

        public static void ProductCategoryLoad()
        {
            Database db = DatabaseFactory.CreateDatabase("MS_TableConnectionString");
            DbTransaction tran = null;

            using (DbConnection connection = db.CreateConnection())
            {
                connection.Open();
                //load into Category table
                try
                {
                    tran = connection.BeginTransaction();

                    ProductCategoryDB.LoadData(db, tran);

                    tran.Commit();
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    throw ex;
                }
            }
        }

        public static void RigInventoryLoad()
        {
            Database db = DatabaseFactory.CreateDatabase("MS_TableConnectionString");
            DbTransaction tran = null;

            using (DbConnection connection = db.CreateConnection())
            {
                connection.Open();
                //load into Inventory table
                try
                {
                    tran = connection.BeginTransaction();

                    RigInventoryDB.LoadData(db, tran);

                    tran.Commit();
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    throw ex;
                }
            }
        }

        public static void UsersLoad()
        {
            Database db = DatabaseFactory.CreateDatabase("MS_TableConnectionString");
            DbTransaction tran = null;

            using (DbConnection connection = db.CreateConnection())
            {
                connection.Open();
                //load into Inventory table
                try
                {
                    tran = connection.BeginTransaction();

                    UsersDB.LoadData(db, tran);

                    tran.Commit();
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    throw ex;
                }
            }
        }

        public static void UpdateFirstRecord()
        {
            Database db = DatabaseFactory.CreateDatabase("MS_TableConnectionString");
            DbTransaction tran = null;

            using (DbConnection connection = db.CreateConnection())
            {
                connection.Open();

                //load into Category table
                try
                {
                    tran = connection.BeginTransaction();

                    GeneralDB.UpdateFirstRecord(db, tran);

                    tran.Commit();
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    throw ex;
                }
            }
        }


    }
}
