﻿using RigServe.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RigServ.BLL.EmailNotifications;

namespace RigServ.BLL
{
    public class IssueEmail
    {
        public static void RunDailyIssueNotification()
        {
            var Facilities = FacilityDB.GetAllFacilities();

            foreach (DataRow row in Facilities.Rows)
            {
                string FacilityId = row["Id"].ToString();

                //Get Issue for the facility and send email if found
                var Data = IssueItemDB.GetDailyIssues(FacilityId);
                if (Convert.ToInt32(Data.Rows[0]["ItemCount"]) > 0)
                {
                    var IssueEmailNotification = new IssueEmailNotification();
                    IssueEmailNotification.ResultDT = Data;
                    IssueEmailNotification.FacilityId = FacilityId;
                    IssueEmailNotification.Send();
                }

                //Get return for the facility and send email if found
                var ReturnData = ReturnItemDB.GetDailyReturns(FacilityId);
                if (Convert.ToInt32(ReturnData.Rows[0]["ItemCount"]) > 0)
                {
                    var ReturnEmailNotification = new ReturnEmailNotification();
                    ReturnEmailNotification.ResultDT = ReturnData;
                    ReturnEmailNotification.FacilityId = FacilityId;
                    ReturnEmailNotification.Send();
                }
            }
        }
    }
}
