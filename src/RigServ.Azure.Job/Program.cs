﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using RigServ.BLL;
namespace RigServe.Azure.Job
{
    // To learn more about Microsoft Azure WebJobs SDK, please see http://go.microsoft.com/fwlink/?LinkID=320976
    public class Program
    {
        // Please set the following connection strings in app.config for this WebJob to run:
        // AzureWebJobsDashboard and AzureWebJobsStorage
        static void Main()
        {
            var host = new JobHost();
            // The following code ensures that the WebJob will be running continuously
            //host.RunAndBlock();
            host.Call(typeof(Program).GetMethod("RunTask"));
        }

        [NoAutomaticTrigger]
        public static void RunTask()
        {
            try
            {
                Console.WriteLine("Start - Loading all RigServ tables");

                Console.WriteLine("Begin Load Category Stage Table");
                RigServLoad.CategoryStageLoad();
                Console.WriteLine("End Load Category Stage Table");

                Console.WriteLine("Begin Load Facility Table");
                RigServLoad.FacilityLoad();
                Console.WriteLine("End Load Facility Table");

                Console.WriteLine("Begin Load Location Table");
                RigServLoad.LocationLoad();
                Console.WriteLine("End Load Location Table");

                Console.WriteLine("Begin Load Product Table");
                RigServLoad.ProductLoad();
                Console.WriteLine("End Load Product Table");

                Console.WriteLine("Begin Load Product Photo Table");
                RigServLoad.ProductPhotoLoad();
                Console.WriteLine("End Load Product Photo Table");

                Console.WriteLine("Begin Load ProductCategory Table");
                RigServLoad.ProductCategoryLoad();
                Console.WriteLine("End Load ProductCategory Table");

                Console.WriteLine("Begin Load RigInventory Table");
                RigServLoad.RigInventoryLoad();
                Console.WriteLine("End Load RigInventory Table");

                Console.WriteLine("Begin Load Category Table");
                RigServLoad.CategoryLoad();
                Console.WriteLine("End Load Category Table");

                Console.WriteLine("Begin Load Users Table");
                RigServLoad.UsersLoad();
                Console.WriteLine("End Load Users Table");

                Console.WriteLine("Begin Update First Record on Tables");//Category, ProductCategory, Product, Location, RigInventory
                RigServLoad.UpdateFirstRecord();
                Console.WriteLine("Begin Update First Record on Tables");

                Console.WriteLine("Begin Send Daily Issues and Returns Report Email");
                IssueEmail.RunDailyIssueNotification();
                Console.WriteLine("End Send Daily Issues and Returns Report Email");


                Console.WriteLine("Finished - Loading all RigServ tables"); ;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"The following Error Occurred: {ex.Message}");
            }
        }
    }
}
