﻿





-- =============================================
-- Author:		Murugan Andezuthu Dharmaratnam
-- Create date: March  31 2017
-- Description:	Stored Procedure to Create [dbo].[Manifest]  from ManifestH, Received
-- =============================================
CREATE PROCEDURE [dbo].[sp_createmanifest] 
@ManifestId as uniqueidentifier, 
@ReceivedId as uniqueidentifier,
@CreatedBy nvarchar(max),
@IPAddress nvarchar(max)
AS

DECLARE 
@ManifestName nvarchar(max),
@ManifestDate datetime,
@CompanyId uniqueidentifier,
@CompanyName nvarchar(max),
@newuid as uniqueidentifier,
@PODataId as uniqueidentifier,
@UOM nvarchar(max) ,
@MfgID nvarchar(max),
@MfgPart nvarchar(max),
@ICN nvarchar(max),
@UnitPrice nvarchar(max),
@RigId uniqueidentifier,
@RigName nvarchar(max),
@RigCode nvarchar(max),
@PONumber nvarchar(max),
@POLine nvarchar(max)
BEGIN

select @PODataId = PODataId from Received where Id = @ReceivedId
select @newuid = newid()
select @ManifestName= Name,@ManifestDate = ManifestDate,@CompanyId = CompanyId, @CompanyName = CompanyName from ManifestH where Id =  @ManifestId 
select @RigCode=TLAName,@UnitPrice = POUnitPrice, @UOM = ICNUOM, @MfgID= Manufacturer, @MfgPart = PartNumber, @ICN = ICN from POData where Id =  @PODataId  
select @RigId=Id, @RigName = Name from Rig where Code = @RigCode
insert into Manifest(
Id,
ManifestQuantity,POLine,PONumber,Quantity,SmartContainer,Tote,RigID,RigName,
ManifestId,ManifestName,ManifestDate,
BinToDefaultLocation,BinToNewLocation,QuantityDamaged,QuantityShort,IsException,IsReceivedFully,	
[Status],CreatedBy,CreatedOn,ModifiedBy,ModifiedOn,IsActive,IsDeleted,IPAddress,CompanyId,CompanyName,
ImageUrl, NewLocation, IsDefault, Comment, 
[Description],RIN,DefaultLocation,
UOM,MfgID,MfgPart,ICN,UnitPrice	
) select 
@newuid as Id, QuantityReceived,PurchaseOrderLine,PurchaseOrderNumber,QuantityReceived,SmartContainer,Tote,@RigId as RigId, @RigName as RigName,
@ManifestId as ManifestId,@ManifestName as ManifestName,@ManifestDate as ManifestDate,
'0','0','0','0','false','false',
'',@CreatedBy as CreatedBy,getdate(),@CreatedBy as ModifiedBy,getdate(),'true','false',@IPAddress as IPAddress,@CompanyId as CompanyId,@CompanyName as CompanyName,    
'' as ImageUrl, '' as NewLocation, 'false' as IsDefault, '' as comment,
CatalogDescription  as Description, RIN,DefaultLocation,
@UOM as UOM,  @MfgID as MfgID, @MfgPart as MfgPart,@ICN as ICN, @UnitPrice as UnitPrice  
from Received where Id = @ReceivedId


update [dbo].[Received] set [Status] = 'AddedToManifest',ManifestId=@newuid,ManifestName=@ManifestName,RigId=@RigId,RigName=@RigName where Id = @ReceivedId

select @PONumber= PurchaseOrderNumber,@POLine = PurchaseOrderLine from [Received] where Id = @ReceivedId

IF EXISTS (select * from ReceivedException where PONumber=@PONumber and POLine=@POLine)
    UPDATE ReceivedException SET RigCode=@RigCode,RigId=@RigId,RigName=@RigName where PONumber=@PONumber and POLine=@POLine




END