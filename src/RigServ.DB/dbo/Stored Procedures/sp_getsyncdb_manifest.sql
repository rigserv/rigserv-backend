﻿



-- =============================================
-- Author:		Murugan Andezuthu Dharmaratnam
-- Create date: Dec 19 2017
-- Description:	Stored Procedure to return SyncDB 
-- =============================================
CREATE PROCEDURE [dbo].[sp_getsyncdb_manifest] 
@NumericIdLastSync int,
@UserId [uniqueidentifier],
@RigId [uniqueidentifier]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	--create table #RigIds(
	--  [Id] [uniqueidentifier] NOT NULL,
	--);		
	--insert into #RigIds(Id)
	--(select Id from [dbo].[Rig] where 
	--CompanyId in (select CompanyId from [dbo].[AspNetUsers] where Id=@UserId) and 
	--Id In (select RigId from [dbo].[Rig.User] where UserId = @UserId))

	-- Will give list of all manifestdetail for the rigs 
	-- ***************************
	--select * from Manifest where RigId in (select Id from #RigIds)


	-- Will give list of all manifests to sync
	select x.* from 
	
	
(SELECT sdb1.* FROM [dbo].[SyncDB] sdb1
  JOIN (SELECT TableRowId, MAX(NumericId) NumericId FROM [dbo].[SyncDB] GROUP BY TableRowId) sdb2
    ON sdb1.TableRowId = sdb2.TableRowId AND sdb1.NumericId = sdb2.NumericId) as x	

	where TableRowId in 
	(
	select Id as TableRowId from [Manifest] where RigId in (@RigId) and NumericId > @NumericIdLastSync
	) order by NumericId
	RETURN
    -- Insert statements for procedure here
END