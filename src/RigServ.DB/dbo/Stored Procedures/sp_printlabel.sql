﻿
-- =============================================
-- Author:		Murugan Andezuthu Dharmaratnam
-- Create date: Dec 19 2017
-- Description:	Stored Procedure to return SyncDB 
-- =============================================
CREATE PROCEDURE [dbo].[sp_printlabel] 
--@RigCode nvarchar(max),
--@RIN nvarchar(max),
--@Location nvarchar(max)
@Id as uniqueidentifier 
AS
BEGIN
	SET NOCOUNT ON;

		select
		OpenPO.ICN,
		OpenPO.RIN,
		OpenPO.CatalogDescription as [Description],
		OpenPO.Manufacturer,
		OpenPO.PartNumber as MPN,
		OpenPO.[DefaultIndexCodeDescription] as IndexDescription,
		OpenPo.DefaultLocation as Location,
		OpenPO.ICNUOM as UOM,
		OpenPO.RequestedByFirstName + ' ' + OpenPO.RequestedByLastName as RequestedBy,
		OpenPO.PurchaseOrderNumber as PO, 
		OpenPO.TLAName +'-'+ OpenPO.RIN as DTH 
		from [POData] OpenPO
		where Id=@Id
		--OpenPO.TLAName=@RigCode and OpenPO.RIN=@RIN  and OpenPO.DefaultLocation = @Location 

END