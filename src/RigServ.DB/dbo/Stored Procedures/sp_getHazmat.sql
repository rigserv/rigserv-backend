﻿

-- =============================================
-- Author:		Murugan Andezuthu Dharmaratnam
-- Create date: July 13 2017
-- Description:	Stored Procedure to return ManifestH 
-- =============================================
CREATE PROCEDURE [dbo].[sp_getHazmat] 
@ManifestId uniqueidentifier,
@CompanyId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
select Hazmat.* from Manifest INNER JOIN 
Hazmat on Manifest.RIN = Hazmat.RIN 
where Manifest.HZD='true' and Manifest.ManifestId = @ManifestId and Hazmat.CompanyId = @CompanyId and Manifest.CompanyId = @CompanyId




	RETURN
    -- Insert statements for procedure here
END