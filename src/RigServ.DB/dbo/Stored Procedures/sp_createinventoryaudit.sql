﻿





-- =============================================
-- Author:		Murugan Andezuthu Dharmaratnam
-- Create date: March  31 2017
-- Description:	Stored Procedure to Create [dbo].[Manifest]  from ManifestH, Received
-- =============================================
CREATE PROCEDURE [dbo].[sp_createinventoryaudit] 
@RigCode nvarchar(max),
@Locations nvarchar(max),
@Name nvarchar(max)
AS
DECLARE 
@RigId uniqueidentifier,
@RigName nvarchar(max),
@InventoryAuditId uniqueidentifier,
@sql nvarchar(max)
BEGIN
select @RigId=Id, @RigName = Name from Rig where Code = @RigCode
select @InventoryAuditId=newid()

INSERT INTO InventoryAudit(
Id,
Name,
InventoryAuditDate,
CompletedPercentage,
Comments,
RigId,
RigName,
RigCode,
[Status],
CreatedBy,
CreatedOn,
ModifiedBy,
ModifiedOn,
IsActive,
IsDeleted,
IPAddress,
CompanyId,
CompanyName)
VALUES(
@InventoryAuditId,
@Name,
getdate(),
'0',
'',
@RigId,
@RigName,
@RigCode,
'',
'',
getdate(),
'',
getdate(),
'true',
'false',
'',
'27524CD2-FD93-4D16-AB05-AC3003853485',
'Transocean'
)

SET @sql = 
'insert into [dbo].[InventoryAuditDetails](Id,InventoryAuditId,Name,Location,CPN,Manufacturer,ManufacturerPN,CatalogDescription,UOM,Class,ExceptedQuantity,ActualQuantity,Comments,RigId,RigName,RigCode,[Status])
select newid() as Id, '''+CAST(@InventoryAuditId AS VARCHAR(36))+''' as InventoryAuditId, '''+@Name+''' as Name,
I.Location,C.RIN as CPN,C.ManufacturerCode as Manufacturer,C.PartNumber as ManufacturerPN,C.CatalogDescription as CatalogDescription,'''' as UOM,I.Class,I.Qty as ExceptedQuantity,I.Qty as ActualQuantity,'''' as Comments
,'''+CAST(@RigId as VARCHAR(36))+''' as RigId,'''+@RigName+''' as RigName,+'''+@RigCode+''' as RigCode,'''' as [Status]
from Inventory I
LEFT JOIN [Catalog] C
ON I.CatalogId = C.CatalogId
where TLAName='''+@RigCode+'''' + ' and Location IN ('+@Locations+')'

EXEC sp_executeSQL @sql




END