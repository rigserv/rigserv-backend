﻿




-- =============================================
-- Author:		Murugan Andezuthu Dharmaratnam
-- Create date: Jan 1 2017
-- Description:	Stored Procedure to return SyncDB 
-- =============================================
CREATE PROCEDURE [dbo].[sp_updatemanifesth] 
@ManifestId  [uniqueidentifier]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

create table #ManifestDetailData(
  [ManifestId] [uniqueidentifier] NOT NULL,
  [IsClosed] [bit] NOT NULL,
  [NumberOpenItems] [int] NOT NULL,
  [NumberClosedManifests] [int] NOT NULL,
  [NumberExceptions] [int] NOT NULL,
);

insert into #ManifestDetailData([ManifestId],[IsClosed],[NumberOpenItems],[NumberClosedManifests],[NumberExceptions]) 
(
select @ManifestId  as ManifestId, * from 
   (select CASE WHEN count(*) = 0  THEN 'false' else (select CASE WHEN COUNT(*) > 0 THEN 'False' ELSE 'True' END AS IsClosed from Manifest where ManifestId = 'A9414BCC-7646-48AA-8BA2-F66EB414A730' and IsReceivedFully='false') end as IsClosed from Manifest where ManifestId = 'A9414BCC-7646-48AA-8BA2-F66EB414A730') as a
, (select COUNT(Quantity) as  NumberOpenItems from Manifest where ManifestId = @ManifestId and IsReceivedFully='false') as c
, (select COUNT(*) as NumberClosedManifests from Manifest where ManifestId = @ManifestId and IsReceivedFully='true') as b 
, (select count(*) as NumberExceptions from Manifest where ManifestId = @ManifestId and IsException='true') as d 
)


update [dbo].[ManifestH] 
set 
[dbo].[ManifestH].IsClosed = #ManifestDetailData.IsClosed,
[dbo].[ManifestH].NumberOpenItems = #ManifestDetailData.NumberOpenItems,
[dbo].[ManifestH].NumberClosedManifests = #ManifestDetailData.NumberClosedManifests,
[dbo].[ManifestH].NumberExceptions = #ManifestDetailData.NumberExceptions
from [dbo].[ManifestH] JOIN #ManifestDetailData
on [dbo].[ManifestH].Id = #ManifestDetailData.ManifestId
where  #ManifestDetailData.ManifestId = @ManifestId


drop table #ManifestDetailData
	RETURN
    -- Insert statements for procedure here
END