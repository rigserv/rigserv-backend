﻿
-- =============================================
-- Author:		Murugan Andezuthu Dharmaratnam
-- Create date: Dec 19 2017
-- Description:	Stored Procedure to return SyncDB 
-- =============================================
CREATE PROCEDURE [dbo].[sp_printlabelinventory] 
@RigCode nvarchar(max),
@RIN nvarchar(max),
@Location nvarchar(max)
AS
BEGIN
	SET NOCOUNT ON;

		select
		Inventory.ICN,
		Inventory.RIN,
		Inventory.CatalogDescription as [Description],
		Inventory.Manufacturer,
		Inventory.PartNumber as MPN,
		Inventory.[IndexDesc] as IndexDescription,
		Inventory.Location as Location,
		Inventory.ICNUOM as UOM,
		'' as RequestedBy,
		'' as PO, 
		Inventory.TLAName +'-'+ Inventory.RIN as DTH 
		from [Inventory] Inventory
		where Inventory.TLAName=@RigCode and Inventory.RIN=@RIN  and Inventory.Location = @Location 

END