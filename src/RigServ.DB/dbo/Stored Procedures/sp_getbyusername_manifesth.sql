﻿

-- =============================================
-- Author:		Murugan Andezuthu Dharmaratnam
-- Create date: Dec 19 2017
-- Description:	Stored Procedure to return ManifestH 
-- =============================================
CREATE PROCEDURE [dbo].[sp_getbyusername_manifesth] 
@UserName nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	select mh.* from AspNetUsers as usr 
	INNER JOIN 
	[Rig.User] as rgusr on usr.Id = rgusr.UserId
	INNER JOIN ManifestH as mh
	on rgusr.RigId = mh.RigId
	where usr.UserName = @UserName

	RETURN
    -- Insert statements for procedure here
END