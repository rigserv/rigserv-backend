﻿

CREATE PROCEDURE [dbo].[sp_updateopenpo] 
AS
BEGIN

MERGE INTO [dbo].[POData] WITH (HOLDLOCK) AS Target
USING [dbo].[StagePOData] AS Source
ON (Target.[PurchaseOrderNumber] =Source.[PurchaseOrderNumber] and Target.[PurchaseOrderLine] =Source.[PurchaseOrderLine] ) /* and Target.[Default Index Code] = Source.[Default Index Code] */

WHEN MATCHED THEN 
UPDATE 
SET 
Target.[Name] = Source.[Name] ,
Target.[Facility] = Source.[Facility] ,
Target.[CostCenter] =Source.[CostCenter],
Target.[TLAName] =Source.[TLAName],
Target.[RequisitionType] =Source.[RequisitionType],
Target.[RequisitionNumber] =Source.[RequisitionNumber],
Target.[RequisitionLine] =Source.[RequisitionLine],
Target.[RequisitionStatusCode] =Source.[RequisitionStatusCode],
Target.[PurchaseOrderNumber] =Source.[PurchaseOrderNumber],
Target.[PurchaseOrderLine] =Source.[PurchaseOrderLine],
Target.[POLineQty] =Source.[POLineQty],
Target.[GoodsReceiptsQty] =Source.[GoodsReceiptsQty],
Target.[LastReceiptDate] =Source.[LastReceiptDate],
Target.[TotalBinnedQty] =Source.[TotalBinnedQty],
Target.[POUnitPrice] =Source.[POUnitPrice],
Target.[POUOM] =Source.[POUOM],
Target.[RIN] =Source.[RIN],
Target.[CatalogDescription] =Source.[CatalogDescription],
Target.[Manufacturer] =Source.[Manufacturer],
Target.[PartNumber] =Source.[PartNumber],
Target.[Class] =Source.[Class],
Target.[DefaultIndexCode] =Source.[DefaultIndexCode],
Target.[DefaultIndexCodeDescription] =Source.[DefaultIndexCodeDescription],
Target.[ICN] =Source.[ICN],
Target.[ICNUOM] =Source.[ICNUOM],
Target.[WarehouseFacility] =Source.[WarehouseFacility],
Target.[Warehouse] =Source.[Warehouse],
Target.[DefaultLocation] =Source.[DefaultLocation],
Target.[RequestedByFirstName] =Source.[RequestedByFirstName],
Target.[RequestedByLastName] =Source.[RequestedByLastName],
Target.[RequestReason] =Source.[RequestReason],
Target.[RMSJobNumber] =Source.[RMSJobNumber],
Target.[IsActive] = 1,
Target.[Status]  =Source.[Status] ,
Target.[RequisitionHeaderComments] =Source.[RequisitionHeaderComments],
Target.[ReqLinePOComments]=Source.[ReqLinePOComments],
Target.[ReqLineComments]=Source.[ReqLineComments],
Target.[CreatedBy]=Source.[CreatedBy],
Target.[CreatedOn]=Source.[CreatedOn],
Target.[ModifiedBy]=Source.[ModifiedBy],
Target.[ModifiedOn]=Source.[ModifiedOn],
Target.[IsDeleted]=Source.[IsDeleted],
Target.[IPAddress]=Source.[IPAddress],
Target.[CompanyId]=Source.[CompanyId],
Target.[CompanyName]=Source.[CompanyName],
Target.[OpenPOId]=Source.[OpenPOId],
Target.[IsCritical]=Source.[IsCritical],
Target.[Hazardous]=Source.[Hazardous]

WHEN NOT MATCHED BY TARGET THEN
INSERT (
[Id],
[Name],
[Facility],
[CostCenter],
[TLAName],
[RequisitionType],
[RequisitionNumber],
[RequisitionLine],
[RequisitionStatusCode],
[PurchaseOrderNumber],
[PurchaseOrderLine],
[POLineQty],
[GoodsReceiptsQty],
[LastReceiptDate],
[TotalBinnedQty],
[POUnitPrice],
[POUOM],
[RIN],
[CatalogDescription],
[Manufacturer],
[PartNumber],
[Class],
[DefaultIndexCode],
[DefaultIndexCodeDescription],
[ICN],
[ICNUOM],
[WarehouseFacility],
[Warehouse],
[DefaultLocation],
[RequestedByFirstName],
[RequestedByLastName],
[RequestReason],
[RMSJobNumber],
[IsActive],
[Status],
[RequisitionHeaderComments],
[ReqLinePOComments],
[ReqLineComments],
[CreatedBy],
[CreatedOn],
[ModifiedBy],
[ModifiedOn],
[IsDeleted],
[IPAddress],
[CompanyId],
[CompanyName],
[OpenPOId],
[IsCritical],
[Hazardous]
)


VALUES (
Source.[Id],
Source.[Name],
Source.[Facility],
Source.[CostCenter],
Source.[TLAName],
Source.[RequisitionType],
Source.[RequisitionNumber],
Source.[RequisitionLine],
Source.[RequisitionStatusCode],
Source.[PurchaseOrderNumber],
Source.[PurchaseOrderLine],
Source.[POLineQty],
Source.[GoodsReceiptsQty],
Source.[LastReceiptDate],
Source.[TotalBinnedQty],
Source.[POUnitPrice],
Source.[POUOM],
Source.[RIN],
Source.[CatalogDescription],
Source.[Manufacturer],
Source.[PartNumber],
Source.[Class],
Source.[DefaultIndexCode],
Source.[DefaultIndexCodeDescription],
Source.[ICN],
Source.[ICNUOM],
Source.[WarehouseFacility],
Source.[Warehouse],
Source.[DefaultLocation],
Source.[RequestedByFirstName],
Source.[RequestedByLastName],
Source.[RequestReason],
Source.[RMSJobNumber],
1,
'',
Source.[RequisitionHeaderComments],
Source.[ReqLinePOComments],
Source.[ReqLineComments],
Source.[CreatedBy],
Source.[CreatedOn],
Source.[ModifiedBy],
Source.[ModifiedOn],
Source.[IsDeleted],
Source.[IPAddress],
Source.[CompanyId],
Source.[CompanyName],
Source.[OpenPOId],
Source.[IsCritical],
Source.[Hazardous]
)

WHEN NOT MATCHED BY SOURCE THEN
UPDATE SET Target.[IsActive] = 0
;


END

--truncate table [dbo].[StagePOData]



delete from [StagePOData]