﻿CREATE TABLE [dbo].[MailMessageAddress]
(
[MailMessageAddressID] [uniqueidentifier] NOT NULL,
	[MailMessageID] [uniqueidentifier] NOT NULL,
	[AddressType] [nvarchar](50) NOT NULL,
	[EmailAddress] [nvarchar](200) NOT NULL,
	[DisplayName] [nvarchar](200) NULL,
	[CreatedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_MailMessageAddress] PRIMARY KEY  
(
	[MailMessageAddressID] ASC
)
)
