﻿CREATE TABLE [dbo].[Setup.SmartContainer] (
    [Id]          UNIQUEIDENTIFIER NOT NULL,
    [NumericId]   INT              IDENTITY (1, 1) NOT NULL,
    [Name]        NVARCHAR (MAX)   NULL,
    [Description] NVARCHAR (MAX)   NULL,
    [Status]      NVARCHAR (MAX)   NULL,
    [CreatedBy]   NVARCHAR (MAX)   NULL,
    [CreatedOn]   DATETIME         NULL,
    [ModifiedBy]  NVARCHAR (MAX)   NULL,
    [ModifiedOn]  DATETIME         NULL,
    [IsActive]    BIT              NULL,
    [IsDeleted]   BIT              NULL,
    [IPAddress]   NVARCHAR (MAX)   NULL,
    [CompanyId]   UNIQUEIDENTIFIER NULL,
    [CompanyName] NVARCHAR (MAX)   NULL,
    [Type]        NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_SmartContainer] PRIMARY KEY CLUSTERED ([Id] ASC)
);





