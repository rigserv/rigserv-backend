﻿CREATE TABLE [dbo].[MailAddress]
(
	[MailAddressID] [uniqueidentifier] NOT NULL PRIMARY KEY  ,
	[MailMessageID] [uniqueidentifier] NOT NULL,
	[AddressType] [nvarchar](50) NOT NULL,
	[EmailAddress] [nvarchar](200) NOT NULL,
	[DisplayName] [nvarchar](200) NULL,
	[CreatedOn] [datetime] NOT NULL,
 	CONSTRAINT [FK_MailAddress_MailMessage] FOREIGN KEY (MailMessageID) REFERENCES dbo.MailMessage(MailMessageID)
)
