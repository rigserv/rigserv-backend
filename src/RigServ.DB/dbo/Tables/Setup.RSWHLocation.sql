﻿CREATE TABLE [dbo].[Setup.RSWHLocation] (
    [Id]         UNIQUEIDENTIFIER NOT NULL,
    [NumericId]  INT              IDENTITY (1, 1) NOT NULL,
    [Name]       NVARCHAR (MAX)   NULL,
    [Status]     NVARCHAR (MAX)   NULL,
    [CreatedBy]  NVARCHAR (MAX)   NULL,
    [CreatedOn]  DATETIME         NULL,
    [ModifiedBy] NVARCHAR (MAX)   NULL,
    [ModifiedOn] DATETIME         NULL,
    [IsActive]   BIT              NULL,
    [IsDeleted]  BIT              NULL,
    [IPAddress]  NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_Setup.RSWHLocation] PRIMARY KEY CLUSTERED ([Id] ASC)
);



