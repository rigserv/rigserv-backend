﻿CREATE TABLE [dbo].[MailMessage]
(
	[MailMessageID] [uniqueidentifier] NOT NULL,
	[MailSubject] [nvarchar](500) NOT NULL,
	[MailBody] [nvarchar](max) NOT NULL,
	[Priority] [nvarchar](50) NOT NULL,
	[IsBodyHtml] [bit] NOT NULL,
	[IsSent] [bit] NOT NULL,
	[MailSentDate] [datetime] NULL,
	[Template] [nvarchar](50) NULL,
	[EntityType] [nvarchar](50) NULL,
	[EntityID] [nvarchar](50) NULL,
	[ErrorMessage] [nvarchar](500) NULL,
	[Status] INT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](255) NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[ModifiedBy] [nvarchar](255) NOT NULL,
CONSTRAINT [PK_MailMessage] PRIMARY KEY 
(
	[MailMessageID] ASC
)
)
