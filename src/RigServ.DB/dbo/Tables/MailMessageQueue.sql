﻿CREATE TABLE [dbo].[MailMessageQueue](
	[Id] [bigint] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[EntityType] [varchar](100) NULL,
	[EntityID] [varchar](100) NULL,
	[Template] [varchar](100) NULL,
	[Subject] [nvarchar](255) NULL,
	[Payload] [xml] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NOT NULL,
	[CreatedBy] [nvarchar](255) NOT NULL
 )