﻿CREATE TABLE [dbo].[AspNetPageRoleHTMLElements] (
    [Id]        UNIQUEIDENTIFIER NOT NULL,
    [PageId]    UNIQUEIDENTIFIER NOT NULL,
    [RoleId]    UNIQUEIDENTIFIER NOT NULL,
    [Selector]  NVARCHAR (MAX)   NULL,
    [Attribute] NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_AspNetPageRoleHTMLElements] PRIMARY KEY CLUSTERED ([Id] ASC)
);

