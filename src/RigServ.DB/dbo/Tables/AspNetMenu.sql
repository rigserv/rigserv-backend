﻿CREATE TABLE [dbo].[AspNetMenu] (
    [Id]                  UNIQUEIDENTIFIER NOT NULL,
    [Name]                NVARCHAR (MAX)   NULL,
    [ParentId]            UNIQUEIDENTIFIER NULL,
    [AspNetPagesId]       UNIQUEIDENTIFIER NULL,
    [URL]                 NVARCHAR (MAX)   NULL,
    [IsMenuItemContainer] BIT              NULL,
    [DisplayOrder]        INT              NULL,
    [CSSClass]            NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_Menu] PRIMARY KEY CLUSTERED ([Id] ASC)
);

