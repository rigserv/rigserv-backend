﻿CREATE TABLE [dbo].[Catalog] (
    [Id]                 UNIQUEIDENTIFIER NOT NULL,
    [NumericId]          INT              IDENTITY (1, 1) NOT NULL,
    [RIN]                NVARCHAR (MAX)   NULL,
    [CatalogDescription] NVARCHAR (MAX)   NULL,
    [Manufacturer]       NVARCHAR (MAX)   NULL,
    [ManufacturerCode]   NVARCHAR (MAX)   NULL,
    [RINDefaultUOM]      NVARCHAR (MAX)   NULL,
    [PartNumber]         NVARCHAR (MAX)   NULL,
    [StripPartNumber]    NVARCHAR (MAX)   NULL,
    [CatalogId]          UNIQUEIDENTIFIER NULL,
    [Status]             NVARCHAR (MAX)   NULL,
    [CreatedBy]          NVARCHAR (MAX)   NULL,
    [CreatedOn]          DATETIME         NULL,
    [ModifiedBy]         NVARCHAR (MAX)   NULL,
    [ModifiedOn]         DATETIME         NULL,
    [IsActive]           BIT              NULL,
    [IsDeleted]          BIT              NULL,
    [IPAddress]          NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_Catalog] PRIMARY KEY CLUSTERED ([Id] ASC)
);

