﻿CREATE TABLE [dbo].[Dates] (
    [Date]             DATE          NULL,
    [Year]             INT           NULL,
    [QuarterNumber]    INT           NULL,
    [MonthNumber]      INT           NULL,
    [DayOfMonth]       INT           NULL,
    [DateInt]          INT           NULL,
    [Month]            NVARCHAR (50) NULL,
    [Month Of Year]    NVARCHAR (50) NULL,
    [Quarter of Year]  NVARCHAR (50) NULL,
    [DayInWeek]        INT           NULL,
    [Day Of Week Name] NVARCHAR (50) NULL,
    [Week Starting]    DATE          NULL,
    [Week Ending]      DATE          NULL,
    [MonthYrSort]      INT           NULL,
    [QuarterYrSort]    INT           NULL,
    [Week]             NVARCHAR (50) NULL
);

