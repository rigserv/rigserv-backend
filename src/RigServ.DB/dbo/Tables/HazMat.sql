﻿CREATE TABLE [dbo].[HazMat] (
    [Id]                 UNIQUEIDENTIFIER NOT NULL,
    [NumericId]          INT              IDENTITY (1, 1) NOT NULL,
    [Name]               NVARCHAR (MAX)   NULL,
    [RIN]                NVARCHAR (MAX)   NULL,
    [UN]                 NVARCHAR (MAX)   NULL,
    [ProperShippingName] NVARCHAR (MAX)   NULL,
    [Class]              NVARCHAR (MAX)   NULL,
    [PG]                 NVARCHAR (MAX)   NULL,
    [GenericName]        NVARCHAR (MAX)   NULL,
    [Status]             NVARCHAR (MAX)   NULL,
    [CreatedBy]          NVARCHAR (MAX)   NULL,
    [CreatedOn]          DATETIME         NULL,
    [ModifiedBy]         NVARCHAR (MAX)   NULL,
    [ModifiedOn]         DATETIME         NULL,
    [IsActive]           BIT              NULL,
    [IsDeleted]          BIT              NULL,
    [IPAddress]          NVARCHAR (MAX)   NULL,
    [CompanyId]          UNIQUEIDENTIFIER NULL,
    [CompanyName]        NVARCHAR (MAX)   NULL
);



