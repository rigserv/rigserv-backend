﻿CREATE TABLE [dbo].[Rig.User] (
    [Id]        UNIQUEIDENTIFIER NOT NULL,
    [CompanyId] UNIQUEIDENTIFIER NULL,
    [RigId]     UNIQUEIDENTIFIER NULL,
    [UserId]    NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_User.Rig] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Rig.User_Company] FOREIGN KEY ([CompanyId]) REFERENCES [dbo].[Company] ([Id])
);

