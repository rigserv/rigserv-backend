﻿CREATE TABLE [dbo].[RigItemLocation] (
    [Id]          UNIQUEIDENTIFIER NOT NULL,
    [NumericId]   INT              IDENTITY (1, 1) NOT NULL,
    [RigId]       UNIQUEIDENTIFIER NULL,
    [Name]        NVARCHAR (MAX)   NULL,
    [Status]      NVARCHAR (MAX)   NULL,
    [CreatedBy]   NVARCHAR (MAX)   NULL,
    [CreatedOn]   DATETIME         NULL,
    [ModifiedBy]  NVARCHAR (MAX)   NULL,
    [ModifiedOn]  DATETIME         NULL,
    [IsActive]    BIT              NULL,
    [IsDeleted]   BIT              NULL,
    [IPAddress]   NVARCHAR (MAX)   NULL,
    [CompanyId]   UNIQUEIDENTIFIER NULL,
    [CompanyName] NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_RigLocation] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_RigItemLocation_Company] FOREIGN KEY ([CompanyId]) REFERENCES [dbo].[Company] ([Id]),
    CONSTRAINT [FK_RigItemLocation_Rig] FOREIGN KEY ([RigId]) REFERENCES [dbo].[Rig] ([Id])
);


GO
CREATE TRIGGER  rigrtemlocation_insert 
ON [dbo].[rigitemlocation]
AFTER INSERT AS
BEGIN

       SET NOCOUNT ON;
 
       DECLARE @Id uniqueidentifier
       DECLARE @TableNumericId int
	   DECLARE @UserId uniqueidentifier
	   DECLARE @CompanyId uniqueidentifier
	   DECLARE @CreatedOn datetime
 
       SELECT @Id = INSERTED.Id       
       FROM INSERTED
	   
	   SELECT @TableNumericId = INSERTED.NumericId       
       FROM INSERTED
	  
	   SELECT @UserId = CAST(CAST(0 as BINARY) AS UNIQUEIDENTIFIER)
 
	   SELECT @CompanyId = INSERTED.CompanyId
	   FROM INSERTED

	   SELECT @CreatedOn = INSERTED.CreatedOn
	   FROM INSERTED

	   IF @Id IS NOT NULL 
	   BEGIN 
       INSERT INTO [dbo].[SyncDB]
       VALUES(NEWID(),'insert','rigitemlocation', @Id,@TableNumericId, 'server',@UserId,@CompanyId,@CreatedOn,'false','false')
	   END 

END
GO
CREATE TRIGGER rigitemlocation_update
ON [dbo].[rigitemlocation]
AFTER UPDATE AS
BEGIN

       SET NOCOUNT ON;
 
       DECLARE @Id uniqueidentifier
       DECLARE @TableNumericId int
	   DECLARE @UserId uniqueidentifier
	   DECLARE @CompanyId uniqueidentifier
	   DECLARE @CreatedOn datetime
 
       SELECT @Id = DELETED .Id       
       FROM DELETED 

	   SELECT @TableNumericId = INSERTED.NumericId       
       FROM INSERTED

	   SELECT @UserId = CAST(CAST(0 as BINARY) AS UNIQUEIDENTIFIER) 
 
	   SELECT @CompanyId = DELETED.CompanyId
	   FROM DELETED 

	   SELECT @CreatedOn = DELETED.CreatedOn
	   FROM DELETED 

	   IF @Id IS NOT NULL 
	   BEGIN 
	   UPDATE [dbo].[SyncDB] set IsDeleted='true'  where Id IN (SELECT Id from SyncDB where TableRowID=@Id);
       INSERT INTO [dbo].[SyncDB]
       VALUES(NEWID(),'update','rigitemlocation', @Id,@TableNumericId, 'server',@UserId,@CompanyId,@CreatedOn,'false','false')
	   END
END