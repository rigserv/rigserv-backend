﻿CREATE TABLE [dbo].[ReceivedException] (
    [Id]                 UNIQUEIDENTIFIER NOT NULL,
    [NumericId]          INT              IDENTITY (1, 1) NOT NULL,
    [Name]               NVARCHAR (MAX)   NULL,
    [PONumber]           NVARCHAR (MAX)   NULL,
    [POLine]             NVARCHAR (MAX)   NULL,
    [QtyExpected]        DECIMAL (18)     NULL,
    [QtyReceived]        DECIMAL (18)     NULL,
    [Exception]          NVARCHAR (MAX)   NULL,
    [CatalogDescription] NVARCHAR (MAX)   NULL,
    [ReceivedBy]         NVARCHAR (MAX)   NULL,
    [ReceivedOn]         DATETIME         NULL,
    [Status]             NVARCHAR (MAX)   NULL,
    [CreatedBy]          NVARCHAR (MAX)   NULL,
    [CreatedOn]          DATETIME         NULL,
    [ModifiedBy]         NVARCHAR (MAX)   NULL,
    [ModifiedOn]         DATETIME         NULL,
    [IsActive]           BIT              NULL,
    [IsDeleted]          BIT              NULL,
    [IsApproved]         BIT              NULL,
    [IPAddress]          NVARCHAR (MAX)   NULL,
    [CompanyId]          UNIQUEIDENTIFIER NULL,
    [CompanyName]        NVARCHAR (MAX)   NULL,
    [QuantityDamaged]    DECIMAL (18)     NULL,
    [RigId]              UNIQUEIDENTIFIER NULL,
    [RigName]            NVARCHAR (MAX)   NULL,
    [RigCode]            NVARCHAR (MAX)   NULL
);





