﻿CREATE TABLE [dbo].[MailMessageAttachment]
(
	[MailMessageAttachmentID] [uniqueidentifier] NOT NULL,
	[MailMessageID] [uniqueidentifier] NOT NULL,
	[DocumentID] [uniqueidentifier] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_MailMessageAttachment] PRIMARY KEY  
(
	[MailMessageAttachmentID] ASC
)
)
