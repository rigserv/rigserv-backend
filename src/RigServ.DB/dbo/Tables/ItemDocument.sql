﻿CREATE TABLE [dbo].[ItemDocument] (
    [Id]                  UNIQUEIDENTIFIER NOT NULL,
    [NumericId]           INT              IDENTITY (1, 1) NOT NULL,
    [ItemId]              UNIQUEIDENTIFIER NULL,
    [SetupDocumentTypeId] UNIQUEIDENTIFIER NOT NULL,
    [Name]                NVARCHAR (MAX)   NULL,
    [Description]         NVARCHAR (MAX)   NULL,
    [Url]                 NVARCHAR (MAX)   NULL,
    [Status]              NVARCHAR (MAX)   NULL,
    [CreatedBy]           NVARCHAR (MAX)   NULL,
    [CreatedOn]           DATETIME         NULL,
    [ModifiedBy]          NVARCHAR (MAX)   NULL,
    [ModifiedOn]          DATETIME         NULL,
    [IsDefault]           BIT              NULL,
    [IsActive]            BIT              NULL,
    [IsDeleted]           BIT              NULL,
    [IPAddress]           NVARCHAR (MAX)   NULL,
    [CompanyId]           UNIQUEIDENTIFIER NULL,
    [CompanyName]         NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_ItemDocument] PRIMARY KEY CLUSTERED ([Id] ASC)
);

