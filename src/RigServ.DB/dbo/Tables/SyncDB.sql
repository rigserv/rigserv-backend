﻿CREATE TABLE [dbo].[SyncDB] (
    [Id]             UNIQUEIDENTIFIER NOT NULL,
    [NumericId]      INT              IDENTITY (1, 1) NOT NULL,
    [Command]        NVARCHAR (MAX)   NULL,
    [TableName]      NVARCHAR (MAX)   NULL,
    [TableRowId]     UNIQUEIDENTIFIER NULL,
    [TableNumericId] INT              NULL,
    [DataSource]     NVARCHAR (MAX)   NULL,
    [UserId]         UNIQUEIDENTIFIER NULL,
    [CompanyId]      UNIQUEIDENTIFIER NULL,
    [CreatedOn]      DATETIME         NULL,
    [IsCompleted]    BIT              NULL,
    [IsDeleted]      BIT              NULL,
    CONSTRAINT [PK_SyncDB] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_SyncDB_Company] FOREIGN KEY ([CompanyId]) REFERENCES [dbo].[Company] ([Id])
);

