﻿CREATE TABLE [dbo].[Manifest] (
    [Id]                   UNIQUEIDENTIFIER NOT NULL,
    [NumericId]            INT              IDENTITY (1, 1) NOT NULL,
    [ManifestId]           UNIQUEIDENTIFIER NULL,
    [ManifestName]         NVARCHAR (MAX)   NULL,
    [ManifestDate]         DATETIME         NULL,
    [ReceiptNumber]        FLOAT (53)       NULL,
    [ManifestQuantity]     FLOAT (53)       NULL,
    [Quantity]             FLOAT (53)       NULL,
    [UOM]                  NVARCHAR (MAX)   NULL,
    [Description]          NVARCHAR (MAX)   NULL,
    [VendorName]           NVARCHAR (MAX)   NULL,
    [ReqID]                FLOAT (53)       NULL,
    [Tote]                 NVARCHAR (MAX)   NULL,
    [SmartContainer]       NVARCHAR (MAX)   NULL,
    [OffshoreContainer]    NVARCHAR (MAX)   NULL,
    [Weight]               NUMERIC (18)     NULL,
    [ExtendedPrice]        NUMERIC (18)     NULL,
    [PONumber]             NVARCHAR (MAX)   NULL,
    [POLine]               FLOAT (53)       NULL,
    [UnitPrice]            NUMERIC (18)     NULL,
    [HZD]                  NVARCHAR (MAX)   NULL,
    [MfgID]                NVARCHAR (MAX)   NULL,
    [MfgPart]              NVARCHAR (MAX)   NULL,
    [Currency]             NVARCHAR (MAX)   NULL,
    [BinToDefaultLocation] INT              NULL,
    [BinToNewLocation]     INT              NULL,
    [QuantityDamaged]      INT              NULL,
    [QuantityShort]        INT              NULL,
    [IsException]          BIT              NULL,
    [IsReceivedFully]      BIT              NULL,
    [RigID]                UNIQUEIDENTIFIER NULL,
    [RigName]              NVARCHAR (MAX)   NULL,
    [RIN]                  NVARCHAR (MAX)   NULL,
    [ICN]                  NVARCHAR (MAX)   NULL,
    [Comment]              NVARCHAR (MAX)   NULL,
    [DefaultLocation]      NVARCHAR (MAX)   NULL,
    [NewLocation]          NVARCHAR (MAX)   NULL,
    [IsDefault]            BIT              NULL,
    [ImageUrl]             NVARCHAR (MAX)   NULL,
    [Status]               NVARCHAR (MAX)   NULL,
    [CreatedBy]            NVARCHAR (MAX)   NULL,
    [CreatedOn]            DATETIME         NULL,
    [ModifiedBy]           NVARCHAR (MAX)   NULL,
    [ModifiedOn]           DATETIME         NULL,
    [IsActive]             BIT              NULL,
    [IsDeleted]            BIT              NULL,
    [IPAddress]            NVARCHAR (MAX)   NULL,
    [CompanyId]            UNIQUEIDENTIFIER NULL,
    [CompanyName]          NVARCHAR (MAX)   NULL,
    [IsFinalized]          BIT              NULL,
    [QCCheck]              BIT              NULL,
    CONSTRAINT [PK_Manifest] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Manifest_Company] FOREIGN KEY ([CompanyId]) REFERENCES [dbo].[Company] ([Id]),
    CONSTRAINT [FK_Manifest_ManifestH] FOREIGN KEY ([ManifestId]) REFERENCES [dbo].[ManifestH] ([Id])
);




GO
CREATE TRIGGER rigitemlocation_delete
ON [dbo].[Manifest]
AFTER DELETE AS
BEGIN

       SET NOCOUNT ON;
 
       DECLARE @Id uniqueidentifier
       DECLARE @TableNumericId int
	   DECLARE @UserId uniqueidentifier
	   DECLARE @CompanyId uniqueidentifier
	   DECLARE @ModifiedOn datetime
 
       SELECT @Id = DELETED.Id       
       FROM DELETED

	   SELECT @TableNumericId = INSERTED.NumericId       
       FROM INSERTED

	   SELECT @UserId = CAST(CAST(0 as BINARY) AS UNIQUEIDENTIFIER)
 
	   SELECT @CompanyId = DELETED.CompanyId
	   FROM DELETED

	   SELECT @ModifiedOn = GETDATE()
	   FROM DELETED

	   IF @Id IS NOT NULL 
	   BEGIN 
	   UPDATE [dbo].[SyncDB] set IsDeleted='true'  where Id IN (SELECT Id from SyncDB where TableRowID=@Id);
       INSERT INTO [dbo].[SyncDB] VALUES(NEWID(),'delete','rigitemlocation', @Id,@TableNumericId, 'server',@UserId,@CompanyId,@ModifiedOn,'false','false')
	   END
END
GO
CREATE TRIGGER [dbo].[manifest_update]
ON [dbo].[Manifest]
AFTER UPDATE AS
BEGIN

       SET NOCOUNT ON;
 
       DECLARE @Id uniqueidentifier
       DECLARE @TableNumericId int
	   DECLARE @UserId uniqueidentifier
	   DECLARE @CompanyId uniqueidentifier
	   DECLARE @CreatedOn datetime
 
       SELECT @Id = DELETED .Id       
       FROM DELETED 

	   SELECT @TableNumericId = INSERTED.NumericId       
       FROM INSERTED

	   SELECT @UserId = CAST(CAST(0 as BINARY) AS UNIQUEIDENTIFIER) 
 
	   SELECT @CompanyId = DELETED.CompanyId
	   FROM DELETED 

	   SELECT @CreatedOn = DELETED.CreatedOn
	   FROM DELETED 

	   IF @Id IS NOT NULL 
	   BEGIN 
	   UPDATE [dbo].[SyncDB] set IsDeleted='true'  where Id IN (SELECT Id from SyncDB where TableRowID=@Id);
       INSERT INTO [dbo].[SyncDB]
       VALUES(NEWID(),'update','manifest', @Id,@TableNumericId, 'server',@UserId,@CompanyId,@CreatedOn,'false','false')
	   END
END
GO
CREATE TRIGGER  [dbo].[manifest_insert] 
ON [dbo].[Manifest]
AFTER INSERT AS
BEGIN

       SET NOCOUNT ON;
 
       DECLARE @Id uniqueidentifier
       DECLARE @TableNumericId int
	   DECLARE @UserId uniqueidentifier
	   DECLARE @CompanyId uniqueidentifier
	   DECLARE @CreatedOn datetime
 
       SELECT @Id = INSERTED.Id       
       FROM INSERTED
	   
	   SELECT @TableNumericId = INSERTED.NumericId       
       FROM INSERTED
	  
	   SELECT @UserId = CAST(CAST(0 as BINARY) AS UNIQUEIDENTIFIER)
 
	   SELECT @CompanyId = INSERTED.CompanyId
	   FROM INSERTED

	   SELECT @CreatedOn = INSERTED.CreatedOn
	   FROM INSERTED

	   IF @Id IS NOT NULL 
	   BEGIN 
       INSERT INTO [dbo].[SyncDB]
       VALUES(NEWID(),'insert','manifest', @Id,@TableNumericId, 'server',@UserId,@CompanyId,@CreatedOn,'false','false')
	   END 

END
GO
CREATE TRIGGER manifest_delete
ON [dbo].[Manifest]
AFTER DELETE AS
BEGIN

       SET NOCOUNT ON;
 
       DECLARE @Id uniqueidentifier
       DECLARE @TableNumericId int
	   DECLARE @UserId uniqueidentifier
	   DECLARE @CompanyId uniqueidentifier
	   DECLARE @ModifiedOn datetime
 
       SELECT @Id = DELETED.Id       
       FROM DELETED

	   SELECT @TableNumericId = INSERTED.NumericId       
       FROM INSERTED

	   SELECT @UserId = CAST(CAST(0 as BINARY) AS UNIQUEIDENTIFIER)
 
	   SELECT @CompanyId = DELETED.CompanyId
	   FROM DELETED

	   SELECT @ModifiedOn = GETDATE()
	   FROM DELETED

	   IF @Id IS NOT NULL 
	   BEGIN 
	   UPDATE [dbo].[SyncDB] set IsDeleted='true'  where Id IN (SELECT Id from SyncDB where TableRowID=@Id);
       INSERT INTO [dbo].[SyncDB] VALUES(NEWID(),'delete','manifest', @Id,@TableNumericId, 'server',@UserId,@CompanyId,@ModifiedOn,'false','false')
	   END
END