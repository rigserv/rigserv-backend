﻿CREATE TABLE [dbo].[IndexCode] (
    [Id]          UNIQUEIDENTIFIER NOT NULL,
    [NumericId]   INT              IDENTITY (1, 1) NOT NULL,
    [Name]        NVARCHAR (MAX)   NULL,
    [IndexCode]   NVARCHAR (MAX)   NULL,
    [Description] NVARCHAR (MAX)   NULL,
    [Status]      NVARCHAR (MAX)   NULL,
    [CreatedBy]   NVARCHAR (MAX)   NULL,
    [CreatedOn]   DATETIME         NULL,
    [ModifiedBy]  NVARCHAR (MAX)   NULL,
    [ModifiedOn]  DATETIME         NULL,
    [IsActive]    BIT              NULL,
    [IsDeleted]   BIT              NULL,
    [IsApproved]  BIT              NULL,
    [IPAddress]   NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_IndexCode] PRIMARY KEY CLUSTERED ([Id] ASC)
);

