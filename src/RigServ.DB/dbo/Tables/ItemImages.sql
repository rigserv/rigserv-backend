﻿CREATE TABLE [dbo].[ItemImages] (
    [Id]          UNIQUEIDENTIFIER NOT NULL,
    [NumericId]   INT              IDENTITY (1, 1) NOT NULL,
    [ItemId]      UNIQUEIDENTIFIER NULL,
    [Name]        NVARCHAR (MAX)   NULL,
    [Url]         NVARCHAR (MAX)   NULL,
    [Status]      NVARCHAR (MAX)   NULL,
    [CreatedBy]   NVARCHAR (MAX)   NULL,
    [CreatedOn]   DATETIME         NULL,
    [ModifiedBy]  NVARCHAR (MAX)   NULL,
    [ModifiedOn]  DATETIME         NULL,
    [IsDefault]   BIT              NULL,
    [IsActive]    BIT              NULL,
    [IsDeleted]   BIT              NULL,
    [IPAddress]   NVARCHAR (MAX)   NULL,
    [CompanyId]   UNIQUEIDENTIFIER NULL,
    [CompanyName] NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_ItemImages] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_ItemImages_Company] FOREIGN KEY ([CompanyId]) REFERENCES [dbo].[Company] ([Id]),
    CONSTRAINT [FK_ItemImages_Item] FOREIGN KEY ([ItemId]) REFERENCES [dbo].[Item] ([Id])
);

