﻿CREATE TABLE [dbo].[CategoryImage]
(
	[IndexCode] nvarchar(10) NOT NULL,
	[ImageUrl] nvarchar(max) NULL
)
