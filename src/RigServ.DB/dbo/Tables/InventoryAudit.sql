﻿CREATE TABLE [dbo].[InventoryAudit] (
    [Id]                  UNIQUEIDENTIFIER NOT NULL,
    [NumericId]           INT              IDENTITY (1, 1) NOT NULL,
    [Name]                NVARCHAR (MAX)   NULL,
    [InventoryAuditDate]  DATETIME         NULL,
    [CompletedPercentage] DECIMAL (18)     NULL,
    [Comments]            NVARCHAR (MAX)   NULL,
    [RigId]               UNIQUEIDENTIFIER NULL,
    [RigName]             NVARCHAR (MAX)   NULL,
    [RigCode]             NVARCHAR (MAX)   NULL,
    [Status]              NVARCHAR (MAX)   NULL,
    [CreatedBy]           NVARCHAR (MAX)   NULL,
    [CreatedOn]           DATETIME         NULL,
    [ModifiedBy]          NVARCHAR (MAX)   NULL,
    [ModifiedOn]          DATETIME         NULL,
    [IsActive]            BIT              NULL,
    [IsDeleted]           BIT              NULL,
    [IPAddress]           NVARCHAR (MAX)   NULL,
    [CompanyId]           UNIQUEIDENTIFIER NULL,
    [CompanyName]         NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_InventoryAudit] PRIMARY KEY CLUSTERED ([Id] ASC)
);

