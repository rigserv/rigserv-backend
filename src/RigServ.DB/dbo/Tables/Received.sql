﻿CREATE TABLE [dbo].[Received] (
    [Id]                  UNIQUEIDENTIFIER NOT NULL,
    [NumericId]           INT              IDENTITY (1, 1) NOT NULL,
    [Name]                NVARCHAR (MAX)   NULL,
    [RigId]               UNIQUEIDENTIFIER NULL,
    [RigName]             NVARCHAR (MAX)   NULL,
    [ManifestId]          UNIQUEIDENTIFIER NULL,
    [ManifestName]        NVARCHAR (MAX)   NULL,
    [Manufacturer]        NVARCHAR (MAX)   NULL,
    [PartNumber]          NVARCHAR (MAX)   NULL,
    [RigCode]             NVARCHAR (MAX)   NULL,
    [PurchaseOrderNumber] NVARCHAR (MAX)   NULL,
    [PurchaseOrderLine]   NVARCHAR (MAX)   NULL,
    [RIN]                 NVARCHAR (MAX)   NULL,
    [CatalogDescription]  NVARCHAR (MAX)   NULL,
    [DefaultLocation]     NVARCHAR (MAX)   NULL,
    [Tote]                NVARCHAR (MAX)   NULL,
    [SmartContainer]      NVARCHAR (MAX)   NULL,
    [ReceivedBy]          NVARCHAR (MAX)   NULL,
    [ReceivedOn]          DATETIME         NULL,
    [QuantityReceived]    DECIMAL (18)     NULL,
    [QuantityExpected]    DECIMAL (18)     NULL,
    [IsCritical]          BIT              NULL,
    [Status]              NVARCHAR (MAX)   NULL,
    [CreatedBy]           NVARCHAR (MAX)   NULL,
    [CreatedOn]           DATETIME         NULL,
    [ModifiedBy]          NVARCHAR (MAX)   NULL,
    [ModifiedOn]          DATETIME         NULL,
    [IsActive]            BIT              NULL,
    [IsDeleted]           BIT              NULL,
    [IPAddress]           NVARCHAR (MAX)   NULL,
    [CompanyId]           UNIQUEIDENTIFIER NULL,
    [CompanyName]         NVARCHAR (MAX)   NULL,
    [PODataId]            UNIQUEIDENTIFIER NULL,
    [Hazardous]           BIT              NULL,
    [QuantityDamaged]     DECIMAL (18)     NULL,
    [InvoiceNumber]       NVARCHAR (MAX)   NULL,
    [DeliveryDS]          NVARCHAR (25)    NULL
);





