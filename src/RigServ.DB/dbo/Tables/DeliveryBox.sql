﻿CREATE TABLE [dbo].[DeliveryBox] (
    [Id]           UNIQUEIDENTIFIER NOT NULL,
    [NumericId]    INT              IDENTITY (1, 1) NOT NULL,
    [Name]         NVARCHAR (MAX)   NULL,
    [BoxID]        NVARCHAR (MAX)   NULL,
    [RSWHLocation] NVARCHAR (MAX)   NULL,
    [Timestamp]    DATETIME         NULL,
    [Status]       NVARCHAR (MAX)   NULL,
    [CreatedBy]    NVARCHAR (MAX)   NULL,
    [CreatedOn]    DATETIME         NULL,
    [ModifiedBy]   NVARCHAR (MAX)   NULL,
    [ModifiedOn]   DATETIME         NULL,
    [IsActive]     BIT              NULL,
    [IsDeleted]    BIT              NULL,
    [IPAddress]    NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_DeliverBox] PRIMARY KEY CLUSTERED ([Id] ASC)
);

