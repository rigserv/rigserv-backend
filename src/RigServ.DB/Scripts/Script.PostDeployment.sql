﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

:r .\Data\lookup.SQL
:r .\Email\ISSUE_NOTIFICATION.SQL
:r .\Email\RETURN_NOTIFICATION.SQL


UPDATE kiosk.Facility SET EmailAddress='MaterialsCoordinator.DPN@deepwater.com' WHERE Id='DPN'
UPDATE kiosk.Facility SET EmailAddress='MaterialsCoordinator.DPS@deepwater.com' WHERE Id='DPS'
UPDATE kiosk.Facility SET EmailAddress='MaterialsSup.DVS@deepwater.com' WHERE Id='DVS'
UPDATE kiosk.Facility SET EmailAddress='MaterialsCoordinator.DTH@deepwater.com' WHERE Id='DTH'
UPDATE kiosk.Facility SET EmailAddress='materialscoordinator.dpt@deepwater.com' WHERE Id='DPT'

