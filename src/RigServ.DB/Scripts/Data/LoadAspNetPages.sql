﻿
GO
IF NOT EXISTS(SELECT ID FROM [AspNetPages])
BEGIN
INSERT [dbo].[AspNetPages] ([Id], [Name], [URL]) VALUES (N'2c3069cb-5fce-4062-a944-01ac62fd4028', N'Manifest Report', N'/admin/manifest-report')

INSERT [dbo].[AspNetPages] ([Id], [Name], [URL]) VALUES (N'c89242c7-d74d-481e-8a0a-081084ca4c4f', N'ToTe', N'/admin/setup-tote')

INSERT [dbo].[AspNetPages] ([Id], [Name], [URL]) VALUES (N'b65c8513-b335-4f29-9927-0b330fd4b858', N'Received', N'/admin/received')

INSERT [dbo].[AspNetPages] ([Id], [Name], [URL]) VALUES (N'4c3be547-6f14-488a-b7dc-0d5d9f2500e1', N'Dashboard', N'/admin/dashboard')

INSERT [dbo].[AspNetPages] ([Id], [Name], [URL]) VALUES (N'f39cb6e6-9aaf-48d3-ab4c-13205a83c994', N'Inventory Details', N'/admin/inventory-details')

INSERT [dbo].[AspNetPages] ([Id], [Name], [URL]) VALUES (N'53f5ec1c-786c-4562-a6c3-1353f2d73de4', N'Upload  Index Code', N'/admin/index-codes-file_upload')

INSERT [dbo].[AspNetPages] ([Id], [Name], [URL]) VALUES (N'84e06a89-58ca-4bf8-a160-147d436f33df', N'Exception', N'/admin/exception-rig')

INSERT [dbo].[AspNetPages] ([Id], [Name], [URL]) VALUES (N'5775d326-820d-46bf-aab0-20c0970146b7', N'Item', N'/admin/item')

INSERT [dbo].[AspNetPages] ([Id], [Name], [URL]) VALUES (N'eb1c709c-edec-44ca-8330-2182af804b91', N'Document Type', N'/admin/setup-documenttype')

INSERT [dbo].[AspNetPages] ([Id], [Name], [URL]) VALUES (N'c6c62388-531e-4724-bd3f-2e44316c7df3', N'Rig Item Location', N'/admin/rigitemlocation')

INSERT [dbo].[AspNetPages] ([Id], [Name], [URL]) VALUES (N'40b51b27-5bb4-4185-8cc6-33fa33fb7446', N'Upload  Inventory', N'/admin/inventory-file_upload')

INSERT [dbo].[AspNetPages] ([Id], [Name], [URL]) VALUES (N'ad6551bb-22f5-4d22-b52a-36f15f86be25', N'Inventory', N'/admin/inventory')

INSERT [dbo].[AspNetPages] ([Id], [Name], [URL]) VALUES (N'2a6318ba-b303-4ee3-80e8-3fcb055813ac', N'Manifest', N'/admin/manifestH')

INSERT [dbo].[AspNetPages] ([Id], [Name], [URL]) VALUES (N'b3386af5-93d4-40ea-a8b4-4d6bbbb4fd9d', N'Upload  Catalog', N'/admin/catalog-data-file_upload')

INSERT [dbo].[AspNetPages] ([Id], [Name], [URL]) VALUES (N'b4d37052-5e47-465c-8807-51d99b1b66db', N'Upload  Open PO', N'/admin/openpo-data-file_upload')

INSERT [dbo].[AspNetPages] ([Id], [Name], [URL]) VALUES (N'e3974375-b54e-4ca1-84e4-5daa6ae42709', N'Users', N'/admin/users')

INSERT [dbo].[AspNetPages] ([Id], [Name], [URL]) VALUES (N'6d9ee23c-4144-48b2-9009-634013739466', N'Manifest Detail', N'/admin/manifest')

INSERT [dbo].[AspNetPages] ([Id], [Name], [URL]) VALUES (N'207752f1-6c48-4bb0-a0f2-67cac7a83db2', N'Pages', N'/admin/user-page')

INSERT [dbo].[AspNetPages] ([Id], [Name], [URL]) VALUES (N'6e0dfeb1-94db-4344-87eb-72a617912d9d', N'Rig Report', N'/admin/rig-report')

INSERT [dbo].[AspNetPages] ([Id], [Name], [URL]) VALUES (N'a083078b-5bc0-460d-bb73-761166b2477f', N'Roles', N'/admin/roles')

INSERT [dbo].[AspNetPages] ([Id], [Name], [URL]) VALUES (N'5bb8e2bf-d15a-4bbe-9792-7d8dab804a69', N'Upload  Manifest', N'/admin/manifest-file_upload')

INSERT [dbo].[AspNetPages] ([Id], [Name], [URL]) VALUES (N'bbabcbd3-575f-4f59-8f31-7e0f102bf1d4', N'Open PO', N'/admin/po-data')

INSERT [dbo].[AspNetPages] ([Id], [Name], [URL]) VALUES (N'37f2d17d-8fbd-43a9-b128-81e10e80e21a', N'Shipping Container', N'/admin/shipping-container')

INSERT [dbo].[AspNetPages] ([Id], [Name], [URL]) VALUES (N'47925851-6e28-4e8b-a40e-839006bbe262', N'Menu', N'/admin/aspnet-menu')

INSERT [dbo].[AspNetPages] ([Id], [Name], [URL]) VALUES (N'70e0ca3e-88c9-48b3-b1a9-8a42ae4f897a', N'Print Label', N'/admin/print-label')

INSERT [dbo].[AspNetPages] ([Id], [Name], [URL]) VALUES (N'f7abe222-d8ed-4c29-bf18-95bbeb551895', N'Company', N'/admin/company')

INSERT [dbo].[AspNetPages] ([Id], [Name], [URL]) VALUES (N'5a7eae9d-4e53-4f67-99fb-98b39a003976', N'Inventory Audit', N'/admin/inventory-audit')

INSERT [dbo].[AspNetPages] ([Id], [Name], [URL]) VALUES (N'f31f6676-1f8a-43c4-b93a-9bdbeed22ccb', N'Smart Container', N'/admin/setup-smartContainer')

INSERT [dbo].[AspNetPages] ([Id], [Name], [URL]) VALUES (N'634709a0-ff5b-4a25-bb80-a277141cafc3', N'Upload Rig', N'/admin/rig-file_upload')

INSERT [dbo].[AspNetPages] ([Id], [Name], [URL]) VALUES (N'906732e6-eecc-4977-91a7-ac6061135204', N'Inventory Report', N'/admin/inventory-report')

INSERT [dbo].[AspNetPages] ([Id], [Name], [URL]) VALUES (N'2dd0c4ec-a468-45f2-9731-ec7e88e05c37', N'Company Report', N'/admin/company-report')

INSERT [dbo].[AspNetPages] ([Id], [Name], [URL]) VALUES (N'5ee93a8e-6624-4e0a-9424-f3ac362c9dcf', N'Rig', N'/admin/rig')

INSERT [dbo].[AspNetPages] ([Id], [Name], [URL]) VALUES (N'31ac3465-30fb-4cef-b843-f5ccfaac631e', N'Shipping Location', N'/admin/setup-shippinglocation')

INSERT [dbo].[AspNetPages] ([Id], [Name], [URL]) VALUES (N'c1fda290-48f6-45e5-8ebc-fa41d93b6e02', N'Received Exception', N'/admin/received-exception')

END
GO

