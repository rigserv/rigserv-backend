﻿GO
IF NOT EXISTS(SELECT Id FROM AspNetRoles)
BEGIN
INSERT [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'C0590311-6D84-4F5C-9748-B397FF98E298', N'Administrator')
INSERT [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'f6deaf2f-48b7-4d60-8adf-405404d762c7', N'Company User')
INSERT [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'E2A47E33-94D0-449F-84F8-E79A49CDDA0C', N'Rig User')
END
GO