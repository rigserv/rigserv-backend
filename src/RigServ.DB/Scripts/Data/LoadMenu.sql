﻿GO
IF NOT EXISTS(SELECT ID FROM AspNetMenu)
BEGIN
INSERT [dbo].[AspNetMenu] ([Id], [Name], [ParentId], [AspNetPagesId], [URL], [IsMenuItemContainer], [DisplayOrder], [CSSClass]) VALUES (N'df8fc04e-6cb5-4fcf-99e8-04b8686e02df', N'Company Report', N'428dd440-2312-430b-ba76-8550f7e71f6c', N'2dd0c4ec-a468-45f2-9731-ec7e88e05c37', N'/admin/company-report', 0, 6, NULL)

INSERT [dbo].[AspNetMenu] ([Id], [Name], [ParentId], [AspNetPagesId], [URL], [IsMenuItemContainer], [DisplayOrder], [CSSClass]) VALUES (N'cd23a4c9-8706-4e40-94d8-07d57a97cc39', N'Manifest Report', N'428dd440-2312-430b-ba76-8550f7e71f6c', N'2c3069cb-5fce-4062-a944-01ac62fd4028', N'/admin/manifest-report', 0, 6, NULL)

INSERT [dbo].[AspNetMenu] ([Id], [Name], [ParentId], [AspNetPagesId], [URL], [IsMenuItemContainer], [DisplayOrder], [CSSClass]) VALUES (N'daa74342-059d-40dc-88b4-166d70eece73', N'Smart Container', N'3ffdce40-1c2d-4f2a-bc9d-76de0dffdfca', N'f31f6676-1f8a-43c4-b93a-9bdbeed22ccb', N'/admin/setup-smartContainer', 0, 6, NULL)

INSERT [dbo].[AspNetMenu] ([Id], [Name], [ParentId], [AspNetPagesId], [URL], [IsMenuItemContainer], [DisplayOrder], [CSSClass]) VALUES (N'3c6ec037-abad-4db5-8744-2465cfd5e205', N'Shipping Location', N'3ffdce40-1c2d-4f2a-bc9d-76de0dffdfca', N'7dd40b20-1bb5-437c-9ceb-407faceca61d', N'admin/setup-shippingLocation', 0, 6, NULL)

INSERT [dbo].[AspNetMenu] ([Id], [Name], [ParentId], [AspNetPagesId], [URL], [IsMenuItemContainer], [DisplayOrder], [CSSClass]) VALUES (N'b8379175-38d3-4b3b-b727-256be280c354', N'Upload  Open PO', N'c5f8ac33-40ba-4684-ba47-68617c28a660', N'b4d37052-5e47-465c-8807-51d99b1b66db', N'/admin/openpo-data-file_upload', 0, 6, NULL)

INSERT [dbo].[AspNetMenu] ([Id], [Name], [ParentId], [AspNetPagesId], [URL], [IsMenuItemContainer], [DisplayOrder], [CSSClass]) VALUES (N'b1349deb-9d20-434e-b157-2cbb7979f6b6', N'Pages', N'9845306c-3a3c-41a2-87fd-75c826b51a19', N'207752f1-6c48-4bb0-a0f2-67cac7a83db2', N'/admin/user-page', 0, 6, NULL)

INSERT [dbo].[AspNetMenu] ([Id], [Name], [ParentId], [AspNetPagesId], [URL], [IsMenuItemContainer], [DisplayOrder], [CSSClass]) VALUES (N'eb36f6de-19ef-416c-b637-3b2dc8b2b6cf', N'Upload  Inventory', N'c5f8ac33-40ba-4684-ba47-68617c28a660', N'40b51b27-5bb4-4185-8cc6-33fa33fb7446', N'/admin/inventory-file_upload', 0, 6, NULL)

INSERT [dbo].[AspNetMenu] ([Id], [Name], [ParentId], [AspNetPagesId], [URL], [IsMenuItemContainer], [DisplayOrder], [CSSClass]) VALUES (N'75315489-83d4-4032-9d7f-3b78d7d57c4d', N'Inventory Audit', N'1234274c-7e0f-4b80-b938-97a7a832aeb3', N'5a7eae9d-4e53-4f67-99fb-98b39a003976', N'/admin/inventory-audit', 0, 6, NULL)

INSERT [dbo].[AspNetMenu] ([Id], [Name], [ParentId], [AspNetPagesId], [URL], [IsMenuItemContainer], [DisplayOrder], [CSSClass]) VALUES (N'70cabaef-b836-422d-aa82-3cb7bb37c38a', N'Open PO', N'00000000-0000-0000-0000-000000000000', N'00000000-0000-0000-0000-000000000000', N'', 1, 6, NULL)

INSERT [dbo].[AspNetMenu] ([Id], [Name], [ParentId], [AspNetPagesId], [URL], [IsMenuItemContainer], [DisplayOrder], [CSSClass]) VALUES (N'fc306c47-33a5-4fa2-8105-3dfa13405ce3', N'Users', N'9845306c-3a3c-41a2-87fd-75c826b51a19', N'e3974375-b54e-4ca1-84e4-5daa6ae42709', N'/admin/users', 0, 6, NULL)

INSERT [dbo].[AspNetMenu] ([Id], [Name], [ParentId], [AspNetPagesId], [URL], [IsMenuItemContainer], [DisplayOrder], [CSSClass]) VALUES (N'78216fef-58a6-48f1-aeca-41f75e175081', N'Received Exception', N'70cabaef-b836-422d-aa82-3cb7bb37c38a', N'c1fda290-48f6-45e5-8ebc-fa41d93b6e02', N'/admin/received-exception', 0, 6, NULL)

INSERT [dbo].[AspNetMenu] ([Id], [Name], [ParentId], [AspNetPagesId], [URL], [IsMenuItemContainer], [DisplayOrder], [CSSClass]) VALUES (N'9d7fa51f-27b6-480d-9fe7-44b481bfc3ed', N'Manifest Detail', N'3ffdce40-1c2d-4f2a-bc9d-76de0dffdfca', N'6d9ee23c-4144-48b2-9009-634013739466', N'/admin/manifest', 0, 6, NULL)

INSERT [dbo].[AspNetMenu] ([Id], [Name], [ParentId], [AspNetPagesId], [URL], [IsMenuItemContainer], [DisplayOrder], [CSSClass]) VALUES (N'5c693948-9e2d-45f4-a4fd-45974cd4e948', N'Dashboard', N'00000000-0000-0000-0000-000000000000', N'4c3be547-6f14-488a-b7dc-0d5d9f2500e1', N'/admin/dashboard', 0, 6, NULL)

INSERT [dbo].[AspNetMenu] ([Id], [Name], [ParentId], [AspNetPagesId], [URL], [IsMenuItemContainer], [DisplayOrder], [CSSClass]) VALUES (N'961794f4-f0f3-4fbf-8242-46422160eed3', N'Dashboard', N'00000000-0000-0000-0000-000000000000', N'00000000-0000-0000-0000-000000000000', N'/admin/dashboard', 0, 6, NULL)

INSERT [dbo].[AspNetMenu] ([Id], [Name], [ParentId], [AspNetPagesId], [URL], [IsMenuItemContainer], [DisplayOrder], [CSSClass]) VALUES (N'b1e8cf6d-b850-4771-b5c7-48563fb2e0d2', N'Inventory Audits', N'1234274c-7e0f-4b80-b938-97a7a832aeb3', N'7efa4097-19ed-4fe8-8c20-89033ff9dc7b', N'/admin/inventory-audit', 0, 6, NULL)

INSERT [dbo].[AspNetMenu] ([Id], [Name], [ParentId], [AspNetPagesId], [URL], [IsMenuItemContainer], [DisplayOrder], [CSSClass]) VALUES (N'4d006b24-6fd5-47c9-86f4-51daa8e6571b', N'Company', N'3ffdce40-1c2d-4f2a-bc9d-76de0dffdfca', N'f7abe222-d8ed-4c29-bf18-95bbeb551895', N'/admin/company', 0, 6, NULL)

INSERT [dbo].[AspNetMenu] ([Id], [Name], [ParentId], [AspNetPagesId], [URL], [IsMenuItemContainer], [DisplayOrder], [CSSClass]) VALUES (N'f669c1c8-0745-404d-abeb-523241bb208f', N'Inventory Details', N'1234274c-7e0f-4b80-b938-97a7a832aeb3', N'f39cb6e6-9aaf-48d3-ab4c-13205a83c994', N'/admin/inventory-details', 0, 6, NULL)

INSERT [dbo].[AspNetMenu] ([Id], [Name], [ParentId], [AspNetPagesId], [URL], [IsMenuItemContainer], [DisplayOrder], [CSSClass]) VALUES (N'4d8abbb9-d789-4811-a40a-5246a79eda53', N'Rig Report', N'428dd440-2312-430b-ba76-8550f7e71f6c', N'6e0dfeb1-94db-4344-87eb-72a617912d9d', N'/admin/rig-report', 0, 6, NULL)

INSERT [dbo].[AspNetMenu] ([Id], [Name], [ParentId], [AspNetPagesId], [URL], [IsMenuItemContainer], [DisplayOrder], [CSSClass]) VALUES (N'19478d2d-f303-4595-9ddd-524fea982faf', N'Upload  Catalog', N'c5f8ac33-40ba-4684-ba47-68617c28a660', N'b3386af5-93d4-40ea-a8b4-4d6bbbb4fd9d', N'/admin/catalog-data-file_upload', 0, 6, NULL)

INSERT [dbo].[AspNetMenu] ([Id], [Name], [ParentId], [AspNetPagesId], [URL], [IsMenuItemContainer], [DisplayOrder], [CSSClass]) VALUES (N'0a7d9998-ee3c-4b57-a3e6-5dcf9099925a', N'HazMat', N'00000000-0000-0000-0000-000000000000', N'00000000-0000-0000-0000-000000000000', N'', 1, 6, NULL)

INSERT [dbo].[AspNetMenu] ([Id], [Name], [ParentId], [AspNetPagesId], [URL], [IsMenuItemContainer], [DisplayOrder], [CSSClass]) VALUES (N'a7eb5611-2179-4cf5-956c-631b8c409e55', N'Cycle Count', N'1234274c-7e0f-4b80-b938-97a7a832aeb3', N'4209c41c-0cde-4a84-9f5e-58b3774bd74b', N'/admin/cycle-count', 0, 6, NULL)

INSERT [dbo].[AspNetMenu] ([Id], [Name], [ParentId], [AspNetPagesId], [URL], [IsMenuItemContainer], [DisplayOrder], [CSSClass]) VALUES (N'c5f8ac33-40ba-4684-ba47-68617c28a660', N'Excel Upload', N'00000000-0000-0000-0000-000000000000', N'00000000-0000-0000-0000-000000000000', N'', 1, 6, NULL)

INSERT [dbo].[AspNetMenu] ([Id], [Name], [ParentId], [AspNetPagesId], [URL], [IsMenuItemContainer], [DisplayOrder], [CSSClass]) VALUES (N'30097f8d-5865-41a5-9474-68bcb320eeff', N'Received', N'70cabaef-b836-422d-aa82-3cb7bb37c38a', N'b65c8513-b335-4f29-9927-0b330fd4b858', N'/admin/received', 0, 6, NULL)

INSERT [dbo].[AspNetMenu] ([Id], [Name], [ParentId], [AspNetPagesId], [URL], [IsMenuItemContainer], [DisplayOrder], [CSSClass]) VALUES (N'f252f1ae-ace3-45c6-850f-699c602d3b77', N'Print Label', N'00000000-0000-0000-0000-000000000000', N'70e0ca3e-88c9-48b3-b1a9-8a42ae4f897a', N'/admin/print-label', 0, 6, NULL)

INSERT [dbo].[AspNetMenu] ([Id], [Name], [ParentId], [AspNetPagesId], [URL], [IsMenuItemContainer], [DisplayOrder], [CSSClass]) VALUES (N'ce5c352f-1414-49fb-aa5e-721f68f40cb3', N'Manifest', N'dae88caf-897b-47be-b4da-ca0a18a4eda4', N'2a6318ba-b303-4ee3-80e8-3fcb055813ac', N'/admin/manifestH', 0, 6, NULL)

INSERT [dbo].[AspNetMenu] ([Id], [Name], [ParentId], [AspNetPagesId], [URL], [IsMenuItemContainer], [DisplayOrder], [CSSClass]) VALUES (N'9845306c-3a3c-41a2-87fd-75c826b51a19', N'Manage Users & Rights', N'00000000-0000-0000-0000-000000000000', N'00000000-0000-0000-0000-000000000000', N'', 1, 6, NULL)

INSERT [dbo].[AspNetMenu] ([Id], [Name], [ParentId], [AspNetPagesId], [URL], [IsMenuItemContainer], [DisplayOrder], [CSSClass]) VALUES (N'3ffdce40-1c2d-4f2a-bc9d-76de0dffdfca', N'Setup Pages', N'00000000-0000-0000-0000-000000000000', N'00000000-0000-0000-0000-000000000000', N'', 1, 6, NULL)

INSERT [dbo].[AspNetMenu] ([Id], [Name], [ParentId], [AspNetPagesId], [URL], [IsMenuItemContainer], [DisplayOrder], [CSSClass]) VALUES (N'af8d1073-a18d-4584-8808-80b822910d61', N'Roles', N'9845306c-3a3c-41a2-87fd-75c826b51a19', N'a083078b-5bc0-460d-bb73-761166b2477f', N'/admin/roles', 0, 6, NULL)

INSERT [dbo].[AspNetMenu] ([Id], [Name], [ParentId], [AspNetPagesId], [URL], [IsMenuItemContainer], [DisplayOrder], [CSSClass]) VALUES (N'428dd440-2312-430b-ba76-8550f7e71f6c', N'Reports', N'00000000-0000-0000-0000-000000000000', N'00000000-0000-0000-0000-000000000000', N'', 1, 6, NULL)

INSERT [dbo].[AspNetMenu] ([Id], [Name], [ParentId], [AspNetPagesId], [URL], [IsMenuItemContainer], [DisplayOrder], [CSSClass]) VALUES (N'75a39ca2-830e-44e9-b092-8bc1b4526e13', N'Inventory Report', N'428dd440-2312-430b-ba76-8550f7e71f6c', N'906732e6-eecc-4977-91a7-ac6061135204', N'/admin/inventory-report', 0, 6, NULL)

INSERT [dbo].[AspNetMenu] ([Id], [Name], [ParentId], [AspNetPagesId], [URL], [IsMenuItemContainer], [DisplayOrder], [CSSClass]) VALUES (N'67327f18-b3b4-49e6-8b16-8cb7a3b7084a', N'Open PO', N'70cabaef-b836-422d-aa82-3cb7bb37c38a', N'bbabcbd3-575f-4f59-8f31-7e0f102bf1d4', N'/admin/po-data', 0, 6, NULL)

INSERT [dbo].[AspNetMenu] ([Id], [Name], [ParentId], [AspNetPagesId], [URL], [IsMenuItemContainer], [DisplayOrder], [CSSClass]) VALUES (N'1234274c-7e0f-4b80-b938-97a7a832aeb3', N'Inventory', N'00000000-0000-0000-0000-000000000000', N'00000000-0000-0000-0000-000000000000', N'', 1, 6, NULL)

INSERT [dbo].[AspNetMenu] ([Id], [Name], [ParentId], [AspNetPagesId], [URL], [IsMenuItemContainer], [DisplayOrder], [CSSClass]) VALUES (N'cdb2856f-5d80-4de1-96c4-98d653643218', N'Upload Rig', N'c5f8ac33-40ba-4684-ba47-68617c28a660', N'634709a0-ff5b-4a25-bb80-a277141cafc3', N'/admin/rig-file_upload', 0, 6, NULL)

INSERT [dbo].[AspNetMenu] ([Id], [Name], [ParentId], [AspNetPagesId], [URL], [IsMenuItemContainer], [DisplayOrder], [CSSClass]) VALUES (N'2aa7d0c9-e7d0-4923-a42a-a099564270f4', N'Shipping Location', N'3ffdce40-1c2d-4f2a-bc9d-76de0dffdfca', N'31ac3465-30fb-4cef-b843-f5ccfaac631e', N'/admin/setup-shippinglocation', 0, 6, NULL)

INSERT [dbo].[AspNetMenu] ([Id], [Name], [ParentId], [AspNetPagesId], [URL], [IsMenuItemContainer], [DisplayOrder], [CSSClass]) VALUES (N'b55fc1cf-0b66-40f6-bf34-a55149045fdb', N'Manifest Detail', N'dae88caf-897b-47be-b4da-ca0a18a4eda4', N'6d9ee23c-4144-48b2-9009-634013739466', N'/admin/manifest', 0, 6, NULL)

INSERT [dbo].[AspNetMenu] ([Id], [Name], [ParentId], [AspNetPagesId], [URL], [IsMenuItemContainer], [DisplayOrder], [CSSClass]) VALUES (N'7f88c994-a177-4719-a15c-a77ea7ceff7b', N'Exception', N'00000000-0000-0000-0000-000000000000', N'84e06a89-58ca-4bf8-a160-147d436f33df', N'/admin/exception-rig', 0, 6, NULL)

INSERT [dbo].[AspNetMenu] ([Id], [Name], [ParentId], [AspNetPagesId], [URL], [IsMenuItemContainer], [DisplayOrder], [CSSClass]) VALUES (N'16e6c4ce-c789-4140-a096-a943e1ae0a90', N'HazMat', N'0a7d9998-ee3c-4b57-a3e6-5dcf9099925a', N'634e435a-a79e-4b4d-85d3-5e0c978490f8', N'/admin/hazmat', 0, 6, NULL)

INSERT [dbo].[AspNetMenu] ([Id], [Name], [ParentId], [AspNetPagesId], [URL], [IsMenuItemContainer], [DisplayOrder], [CSSClass]) VALUES (N'5cc415da-847f-4e00-9844-aaf882468a7b', N'Upload  Manifest', N'c5f8ac33-40ba-4684-ba47-68617c28a660', N'5bb8e2bf-d15a-4bbe-9792-7d8dab804a69', N'/admin/manifest-file_upload', 0, 6, NULL)

INSERT [dbo].[AspNetMenu] ([Id], [Name], [ParentId], [AspNetPagesId], [URL], [IsMenuItemContainer], [DisplayOrder], [CSSClass]) VALUES (N'8cdc816a-a555-40f5-a38b-ae0ea14a3409', N'Rig', N'3ffdce40-1c2d-4f2a-bc9d-76de0dffdfca', N'5ee93a8e-6624-4e0a-9424-f3ac362c9dcf', N'/admin/rig', 0, 6, NULL)

INSERT [dbo].[AspNetMenu] ([Id], [Name], [ParentId], [AspNetPagesId], [URL], [IsMenuItemContainer], [DisplayOrder], [CSSClass]) VALUES (N'18b71cee-3282-4d71-9c2f-b25cd5ee976d', N'Document Type', N'3ffdce40-1c2d-4f2a-bc9d-76de0dffdfca', N'a576a506-7843-4fa1-acf1-0d4859eba05f', N'admin/setup-documenttype', 0, 6, NULL)

INSERT [dbo].[AspNetMenu] ([Id], [Name], [ParentId], [AspNetPagesId], [URL], [IsMenuItemContainer], [DisplayOrder], [CSSClass]) VALUES (N'6bece2be-40be-48f8-bc53-bcf3d2350fe9', N'Shipping Container', N'3ffdce40-1c2d-4f2a-bc9d-76de0dffdfca', N'37f2d17d-8fbd-43a9-b128-81e10e80e21a', N'/admin/shipping-container', 0, 6, NULL)

INSERT [dbo].[AspNetMenu] ([Id], [Name], [ParentId], [AspNetPagesId], [URL], [IsMenuItemContainer], [DisplayOrder], [CSSClass]) VALUES (N'52d4d5fc-cfd6-409a-93f2-bd21215ce1eb', N'Exception', N'00000000-0000-0000-0000-000000000000', N'00000000-0000-0000-0000-000000000000', N'/admin/exception-rig', 0, 6, NULL)

INSERT [dbo].[AspNetMenu] ([Id], [Name], [ParentId], [AspNetPagesId], [URL], [IsMenuItemContainer], [DisplayOrder], [CSSClass]) VALUES (N'625540a1-0e8c-4fe6-98d3-bda330d1e1e1', N'Document Type', N'3ffdce40-1c2d-4f2a-bc9d-76de0dffdfca', N'eb1c709c-edec-44ca-8330-2182af804b91', N'/admin/setup-documenttype', 0, 6, NULL)

INSERT [dbo].[AspNetMenu] ([Id], [Name], [ParentId], [AspNetPagesId], [URL], [IsMenuItemContainer], [DisplayOrder], [CSSClass]) VALUES (N'f787d077-243d-4a64-a10e-c2afc1d49c33', N'Rig Item Location', N'3ffdce40-1c2d-4f2a-bc9d-76de0dffdfca', N'c6c62388-531e-4724-bd3f-2e44316c7df3', N'/admin/rigitemlocation', 0, 6, NULL)

INSERT [dbo].[AspNetMenu] ([Id], [Name], [ParentId], [AspNetPagesId], [URL], [IsMenuItemContainer], [DisplayOrder], [CSSClass]) VALUES (N'670eea62-a960-4d26-8042-c616835144c4', N'Upload  Index Code', N'c5f8ac33-40ba-4684-ba47-68617c28a660', N'53f5ec1c-786c-4562-a6c3-1353f2d73de4', N'/admin/index-codes-file_upload', 0, 6, NULL)

INSERT [dbo].[AspNetMenu] ([Id], [Name], [ParentId], [AspNetPagesId], [URL], [IsMenuItemContainer], [DisplayOrder], [CSSClass]) VALUES (N'28db28cb-4dce-4e83-910c-c83641545cce', N'Print Label', N'00000000-0000-0000-0000-000000000000', N'00000000-0000-0000-0000-000000000000', N'/admin/print-label', 0, 6, NULL)

INSERT [dbo].[AspNetMenu] ([Id], [Name], [ParentId], [AspNetPagesId], [URL], [IsMenuItemContainer], [DisplayOrder], [CSSClass]) VALUES (N'dae88caf-897b-47be-b4da-ca0a18a4eda4', N'Manifest', N'00000000-0000-0000-0000-000000000000', N'00000000-0000-0000-0000-000000000000', N'', 1, 6, NULL)

INSERT [dbo].[AspNetMenu] ([Id], [Name], [ParentId], [AspNetPagesId], [URL], [IsMenuItemContainer], [DisplayOrder], [CSSClass]) VALUES (N'9fa29cb8-67eb-4f6f-a0b3-d40bef008b94', N'Item', N'3ffdce40-1c2d-4f2a-bc9d-76de0dffdfca', N'5775d326-820d-46bf-aab0-20c0970146b7', N'/admin/item', 0, 6, NULL)

INSERT [dbo].[AspNetMenu] ([Id], [Name], [ParentId], [AspNetPagesId], [URL], [IsMenuItemContainer], [DisplayOrder], [CSSClass]) VALUES (N'7519c124-d887-4a1c-9489-e55a1a9b6ee1', N'Menu', N'9845306c-3a3c-41a2-87fd-75c826b51a19', N'47925851-6e28-4e8b-a40e-839006bbe262', N'/admin/aspnet-menu', 0, 6, NULL)

INSERT [dbo].[AspNetMenu] ([Id], [Name], [ParentId], [AspNetPagesId], [URL], [IsMenuItemContainer], [DisplayOrder], [CSSClass]) VALUES (N'252b8299-e965-4b16-8e88-f422c7b187ef', N'Tote', N'3ffdce40-1c2d-4f2a-bc9d-76de0dffdfca', N'c89242c7-d74d-481e-8a0a-081084ca4c4f', N'/admin/setup-tote', 0, 6, NULL)
END
GO

