﻿
if NOT EXISTS(Select IndexCode from IndexCodesStage WHERE IndexCode='110000')
begin
INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (110000, N'MAJOR RIG EQUIPMENT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (110100, N'MAJOR RIG EQUIPMENT - MISCELLANEOUS ITEMS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (110200, N'MAJOR RIG EQUIPMENT - DERRICK / MAST')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (110300, N'MAJOR RIG EQUIPMENT - DERRICK SHEAVES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (110400, N'MAJOR RIG EQUIPMENT - CROWN BLOCK WITH SHEAVES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (110600, N'MAJOR RIG EQUIPMENT - GUIDE RAILS WITH SNUBBERS AND RIGGING')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (110700, N'MAJOR RIG EQUIPMENT - MAST ELEVATING EQUIPMENT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (110800, N'MAJOR RIG EQUIPMENT - HOOK')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (110900, N'MAJOR RIG EQUIPMENT - TRAVELING BLOCK')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (111000, N'MAJOR RIG EQUIPMENT - TRAVELING HOOK BLOCK ASSEMBLY')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (111100, N'MAJOR RIG EQUIPMENT - TOP DRIVE DRILLING')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (111101, N'MAJOR RIG EQUIPMENT - TOP DRIVE DRILLING - TOP DRIVE  MOTOR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (111200, N'MAJOR RIG EQUIPMENT - RAISED BACKUP SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (111400, N'MAJOR RIG EQUIPMENT - GUIDE ROLLER ASSEMBLY')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (111500, N'MAJOR RIG EQUIPMENT - MOTION COMPENSATOR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (111501, N'MAJOR RIG EQUIPMENT - MOTION COMPENSATOR - AIR COMPRESSOR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (111502, N'MAJOR RIG EQUIPMENT - MOTION COMPENSATOR - AIR DRYER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (111503, N'MAJOR RIG EQUIPMENT - MOTION COMPENSATOR - AIR RECEIVERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (111504, N'MAJOR RIG EQUIPMENT - MOTION COMPENSATOR - DERRICK ACCUMULATOR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (111505, N'MAJOR RIG EQUIPMENT - MOTION COMPENSATOR - CONTROL PANEL')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (111506, N'MAJOR RIG EQUIPMENT - MOTION COMPENSATOR - SAFETY DECELERATION VALVE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (111508, N'MAJOR RIG EQUIPMENT - MOTION COMPENSATOR - PIPING AND VALVES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (111509, N'MAJOR RIG EQUIPMENT - MOTION COMPENSATOR - HYDRAULIC POWER UNIT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (111550, N'MAJOR RIG EQUIPMENT - MOTION COMPENSATOR ACTIVE SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (111600, N'MAJOR RIG EQUIPMENT - SWIVEL')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (111800, N'MAJOR RIG EQUIPMENT - WIRE LINE GUIDE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (112000, N'MAJOR RIG EQUIPMENT - DEAD LINE STABILIZER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (112200, N'MAJOR RIG EQUIPMENT - CASING STABBING BOARD')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (112600, N'MAJOR RIG EQUIPMENT - DRAWWORKS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (112700, N'MAJOR RIG EQUIPMENT - DRAWWORKS DRIVE UNIT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (112701, N'MAJOR RIG EQUIPMENT - DRAWWORKS DRIVE UNIT - D.C. ELECTRIC MOTOR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (112800, N'MAJOR RIG EQUIPMENT - DRAWWORKS OVER-RUNNING CLUTCH')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (113000, N'MAJOR RIG EQUIPMENT - AUXILIARY DRAWWORKS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (113400, N'MAJOR RIG EQUIPMENT - CROWN BLOCK PROTECTOR ASSEMBLY (CROWN-O-MATIC)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (113500, N'MAJOR RIG EQUIPMENT - AUTOMATIC DRILLER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (113600, N'MAJOR RIG EQUIPMENT - DRILLERS CONSOLE ')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (113601, N'MAJOR RIG EQUIPMENT - DRILLERS CONSOLE  - WEIGHT INDICATOR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (113602, N'MAJOR RIG EQUIPMENT - DRILLERS CONSOLE  - ELECTRIC TORQUE METER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (113603, N'MAJOR RIG EQUIPMENT - DRILLERS CONSOLE  - RPM METER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (113604, N'MAJOR RIG EQUIPMENT - DRILLERS CONSOLE  - SPM METER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (113605, N'MAJOR RIG EQUIPMENT - DRILLERS CONSOLE  - MUD PUMP PRESSURE GAUGE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (113606, N'MAJOR RIG EQUIPMENT - DRILLERS CONSOLE  - RECORD-O-GRAPH AIR CLUTCH VALVE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (113607, N'MAJOR RIG EQUIPMENT - DRILLERS CONSOLE  - MUD FLOW FILL AND STROKES PANEL')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (113608, N'MAJOR RIG EQUIPMENT - DRILLERS CONSOLE  - PRESSURIZATION CONTROL VALVES, GAUGES & REGULATORS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (113609, N'MAJOR RIG EQUIPMENT - DRILLERS CONSOLE  - MUD PIT VOLUME GAUGE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (113610, N'MAJOR RIG EQUIPMENT - DRILLERS CONSOLE  - TONG TORQUE GAUGE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (113611, N'MAJOR RIG EQUIPMENT - DRILLERS CONSOLE  - TON-MILE INDICATOR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (113650, N'MAJOR RIG EQUIPMENT - ZONE MANAGEMENT SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (113700, N'MAJOR RIG EQUIPMENT - DRILLING RECORDER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (113800, N'MAJOR RIG EQUIPMENT - DRILLING LINE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (114000, N'MAJOR RIG EQUIPMENT - DRILLING LINE REEL HOLDER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (114100, N'MAJOR RIG EQUIPMENT - CORING REEL (SAND REEL)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (114101, N'MAJOR RIG EQUIPMENT - CORING REEL (SAND REEL) - SAND LINE SPOOLER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (114102, N'MAJOR RIG EQUIPMENT - CORING REEL (SAND REEL) - SAND LINE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (114103, N'MAJOR RIG EQUIPMENT - CORING REEL (SAND REEL) - SAND LINE WIPER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (114400, N'MAJOR RIG EQUIPMENT - WIRE LINE ANCHOR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (115000, N'MAJOR RIG EQUIPMENT - ROTARY TABLE ASSEMBLY')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (115001, N'MAJOR RIG EQUIPMENT - ROTARY TABLE ASSEMBLY - ROTARY TABLE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (115002, N'MAJOR RIG EQUIPMENT - ROTARY TABLE ASSEMBLY - ROTARY TABLE COUPLING & GUARDS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (115003, N'MAJOR RIG EQUIPMENT - ROTARY TABLE ASSEMBLY - ROTARY TABLE DRIVE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (115004, N'MAJOR RIG EQUIPMENT - ROTARY TABLE ASSEMBLY - ROTARY TABLE SKID')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (115005, N'MAJOR RIG EQUIPMENT - ROTARY TABLE ASSEMBLY - ROTARY TRANSMISSION')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (115100, N'MAJOR RIG EQUIPMENT - MASTER BUSHING')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (115101, N'MAJOR RIG EQUIPMENT - MASTER BUSHING - INSERT BOWLS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (115200, N'MAJOR RIG EQUIPMENT - CASING BUSHINGS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (115800, N'MAJOR RIG EQUIPMENT - POWER SUB / KELLY SPINNER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (115801, N'MAJOR RIG EQUIPMENT - POWER SUB / KELLY SPINNER - HYDRAULIC SPINNER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (115802, N'MAJOR RIG EQUIPMENT - POWER SUB / KELLY SPINNER - ELECTRIC SPINNER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (115803, N'MAJOR RIG EQUIPMENT - POWER SUB / KELLY SPINNER - AIR OPERATED SPINNER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (116200, N'MAJOR RIG EQUIPMENT - RIG FLOOR HYDRAULIC')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (116201, N'MAJOR RIG EQUIPMENT - RIG FLOOR HYDRAULIC - POWER UNITS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (116202, N'MAJOR RIG EQUIPMENT - RIG FLOOR HYDRAULIC - CONTROLS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (116203, N'MAJOR RIG EQUIPMENT - RIG FLOOR HYDRAULIC - PIPING AND VALVES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (116400, N'MAJOR RIG EQUIPMENT - RAT HOLE ASSEMBLY')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (116600, N'MAJOR RIG EQUIPMENT - MOUSE HOLE ASSEMBLY')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (116800, N'MAJOR RIG EQUIPMENT - TONG COUNTERWEIGHTS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (117000, N'MAJOR RIG EQUIPMENT - TONG BACKUP POST / TONG LATCH')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (117200, N'MAJOR RIG EQUIPMENT - TRAVELING T BAR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (120000, N'MUD SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (120100, N'MUD SYSTEM - MISCELLANEOUS ITEMS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (120300, N'MUD SYSTEM - SLUSH PUMPS (MUD)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (120400, N'MUD SYSTEM - SLUSH PUMPS (MUD) - PULSATION DAMPENERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (120401, N'MUD SYSTEM - SLUSH PUMPS (MUD) - SUCTION MODULES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (120402, N'MUD SYSTEM - SLUSH PUMPS (MUD) - DISCHARGE MODULES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (120500, N'MUD SYSTEM - SLUSH PUMPS DRIVE UNIT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (120501, N'MUD SYSTEM - SLUSH PUMPS DRIVE UNIT - MUD SYSTEM  ELECTRIC MOTORS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (120600, N'MUD SYSTEM - MUD CHARGING PUMPS (WITH MOTORS)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (120700, N'MUD SYSTEM - MUD ROOM CONTROLS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (120800, N'MUD SYSTEM - BULK MUD / CEMENT PIPING WITH VALVES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (120900, N'MUD SYSTEM - BULK MUD / CEMENT TANKS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (120901, N'MUD SYSTEM - BULK MUD / CEMENT TANKS - BULK MUD-CEMENT STORAGE TANKS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (120902, N'MUD SYSTEM - BULK MUD / CEMENT TANKS - BULK MUD-CEMENT SURGE TANKS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (120903, N'MUD SYSTEM - BULK MUD / CEMENT TANKS - BULK MUD-CEMENT MIX TANKS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (120904, N'MUD SYSTEM - BULK MUD / CEMENT TANKS - OIL BASE MUD TANKS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (120950, N'MUD SYSTEM - LIQUID MUD TANKS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (120951, N'MUD SYSTEM - LIQUID MUD TANKS - ACTIVE MUD PITS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (120952, N'MUD SYSTEM - LIQUID MUD TANKS - RESERVE MUD PITS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (120953, N'MUD SYSTEM - LIQUID MUD TANKS - SLUGGING PIT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (120954, N'MUD SYSTEM - LIQUID MUD TANKS - MUD PRE-MIX TANKS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (120955, N'MUD SYSTEM - LIQUID MUD TANKS - CHEMICAL MIXING TANKS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (120956, N'MUD SYSTEM - LIQUID MUD TANKS - MUD PROCESSING PITS (SHAKER PITS)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (120957, N'MUD SYSTEM - LIQUID MUD TANKS - AUXILIARY MUD PITS (DECK MOUNTED TANKS)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (120958, N'MUD SYSTEM - LIQUID MUD TANKS - MUD PIT DUMP VALVES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (121000, N'MUD SYSTEM - AUTOMATED MUD SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (121100, N'MUD SYSTEM - BULK MUD WEIGHING / LEVEL INDICATING SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (121300, N'MUD SYSTEM - BULK AIR SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (121301, N'MUD SYSTEM - BULK AIR SYSTEM - MISCELLANEOUS ITEMS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (121302, N'MUD SYSTEM - BULK AIR SYSTEM - AIR COMPRESSORS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (121303, N'MUD SYSTEM - BULK AIR SYSTEM - AFTER COOLERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (121304, N'MUD SYSTEM - BULK AIR SYSTEM - AIR DRYER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (121305, N'MUD SYSTEM - BULK AIR SYSTEM - AIR RECEIVERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (121306, N'MUD SYSTEM - BULK AIR SYSTEM - PIPING AND VALVES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (121400, N'MUD SYSTEM - TRIP TANK WITH REMOTE INDICATOR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (121500, N'MUD SYSTEM - TRIP TANK PUMP WITH MOTOR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (121600, N'MUD SYSTEM - MUD GUN SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (121800, N'MUD SYSTEM - CHEMICAL MIXING PUMP WITH MOTOR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (121900, N'MUD SYSTEM - CHEMICAL MIXING TANK')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (122000, N'MUD SYSTEM - MUD SHEARING PUMP')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (122300, N'MUD SYSTEM - AUTOMATIC MUD MIXER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (122500, N'MUD SYSTEM - MUD MIXING HOPPERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (122600, N'MUD SYSTEM - DUST COLLECTION SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (122700, N'MUD SYSTEM - MUD MIXING / TRANSFER PUMPS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (122701, N'MUD SYSTEM - MUD MIXING / TRANSFER PUMPS - BASE OIL TRANSFER PUMPS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (122702, N'MUD SYSTEM - MUD MIXING / TRANSFER PUMPS - BRINE / COMPLETION FLUID TRANSFER PUMP')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (122800, N'MUD SYSTEM - MUD CIRCULATING PUMPS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (122900, N'MUD SYSTEM - MUD-BILGE CLEAN OUT PUMP WITH MOTOR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (123000, N'MUD SYSTEM - OIL SKIMMER UNIT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (123100, N'MUD SYSTEM - MUD MIXERS (AGITATORS)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (123200, N'MUD SYSTEM - LOW PRESSURE MUD SYSTEM PIPING WITH VALVES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (123201, N'MUD SYSTEM - LOW PRESSURE MUD SYSTEM PIPING WITH VALVES - GATE VALVES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (123202, N'MUD SYSTEM - LOW PRESSURE MUD SYSTEM PIPING WITH VALVES - RELIEF VALVES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (123203, N'MUD SYSTEM - LOW PRESSURE MUD SYSTEM PIPING WITH VALVES - PRESSURE GAUGES (MUD)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (123204, N'MUD SYSTEM - LOW PRESSURE MUD SYSTEM PIPING AND VALVES - PIPING, FLANGES, TEES, CROSSES, ETC.')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (123300, N'MUD SYSTEM - H.P. MUD SYSTEM PIPING')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (123301, N'MUD SYSTEM - H.P. MUD SYSTEM PIPING - GATE VALVES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (123302, N'MUD SYSTEM - H.P. MUD SYSTEM PIPING - RELIEF VALVES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (123303, N'MUD SYSTEM - H.P. MUD SYSTEM PIPING - MUD PRESSURE GAUGES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (123304, N'MUD SYSTEM - H.P. MUD SYSTEM PIPING - PIPING, FLANGES, TEES, CROSSES, ETC.')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (123400, N'MUD SYSTEM - MUD FLOW RETURN LINE / BELL NIPPLE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (123401, N'MUD SYSTEM - MUD FLOW RETURN LINE / BELL NIPPLE - MUD LINE FLOW INDICATOR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (123410, N'MUD SYSTEM - GUMBO BUSTER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (123500, N'MUD SYSTEM - SHALE SHAKER UNIT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (123502, N'MUD SYSTEM - SHALE SHAKER UNIT - SHALE SHAKER SCREENS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (123600, N'MUD SYSTEM - CUTTINGS CLEANER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (123601, N'MUD SYSTEM - CUTTINGS CLEANER - CUTTINGS CONVEYOR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (123700, N'MUD SYSTEM - DEGASSER UNIT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (123701, N'MUD SYSTEM - DEGASSER UNIT - PUMP WITH MOTOR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (123800, N'MUD SYSTEM - DESANDER UNIT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (123801, N'MUD SYSTEM - DESANDER UNIT - PUMP WITH MOTOR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (123900, N'MUD SYSTEM - DESILTER / MUD CLEANER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (123901, N'MUD SYSTEM - DESILTER / MUD CLEANER - PUMP WITH MOTOR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (123902, N'MUD SYSTEM - DESILTER / MUD CLEANER - MUD CLEANER SCREENS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (123950, N'MUD SYSTEM - AUXILIARY PUMP FOR USE WITH DEGASSER, DESANDER, DESILTER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (123951, N'MUD SYSTEM - AUXILIARY PUMP FOR USE WITH DEGASSER, DESANDER, DESILTER - PUMP WITH MOTOR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (124000, N'MUD SYSTEM - MUD GAS SEPARATOR (POOR BOY)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (124100, N'MUD SYSTEM - MUD CENTRIFUGE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (124101, N'MUD SYSTEM - MUD CENTRIFUGE - CENTRIFUGE POWER UNIT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (124200, N'MUD SYSTEM - ROTARY HOSE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (124300, N'MUD SYSTEM - VIBRATOR HOSE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (124500, N'MUD SYSTEM - SWIVEL JOINTS (CHIKSANS)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (124600, N'MUD SYSTEM - MUD (MUD CHEMICALS)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (124601, N'MUD SYSTEM - MUD (MUD CHEMICALS) - BENTONITE PRODUCTS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (124602, N'MUD SYSTEM - MUD (MUD CHEMICALS) - BARITE WEIGHT ADDITIVES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (124603, N'MUD SYSTEM - MUD (MUD CHEMICALS) - MUD ADDITIVES / CHEMICALS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (124604, N'MUD SYSTEM - MUD (MUD CHEMICALS) - OIL BASE MUD (OBM)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (124700, N'MUD SYSTEM - MUD VACUUM UNITS (FIXED / PORTABLE)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (124900, N'MUD SYSTEM - SUBSEA CUTTINGS TRANSPORT / REMOVAL SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (125000, N'CEMENT SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (125100, N'CEMENT SYSTEM - MISCELLANEOUS ITEMS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (125300, N'CEMENT SYSTEM - CEMENT UNIT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (125400, N'CEMENT SYSTEM - CEMENT LIQUID ADDITIVE SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (125500, N'CEMENT SYSTEM - CEMENT UNIT DRIVE UNIT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (125501, N'CEMENT SYSTEM - CEMENT UNIT DRIVE UNIT - ELECTRIC PUMPS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (125502, N'CEMENT SYSTEM - CEMENT UNIT DRIVE UNIT - DIESEL PUMPS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (125600, N'CEMENT SYSTEM - CEMENT MIXING PUMP')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (125700, N'CEMENT SYSTEM - CEMENT ROOM CONTROL')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (125800, N'CEMENT SYSTEM - CEMENT RECIRCULATION')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (125900, N'CEMENT SYSTEM - CEMENT SUCTION / MIXING')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (126000, N'CEMENT SYSTEM - CEMENT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (126001, N'CEMENT SYSTEM - CEMENT - CEMENT ADDITIVES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (127002, N'CEMENT SYSTEM - CEMENT - RTTS TOOL')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (127004, N'CEMENT SYSTEM - CEMENT - EZSV TOOL')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (130000, N'SURFACE CONTROL / SUBSEA')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (130100, N'SURFACE CONTROL / SUBSEA - MISCELLANEOUS ITEMS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (130300, N'SURFACE CONTROL / SUBSEA - BOP CONTROL SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (130301, N'SURFACE CONTROL / SUBSEA - BOP CONTROL SYSTEM - CONTROL MANIFOLD')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (130302, N'SURFACE CONTROL / SUBSEA - BOP CONTROL SYSTEM - POWER UNIT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (130303, N'SURFACE CONTROL / SUBSEA - BOP CONTROL SYSTEM - ACCUMULATOR BOTTLES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (130304, N'SURFACE CONTROL / SUBSEA - BOP CONTROL SYSTEM - HYDRAULIC TEST PANEL')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (130305, N'SURFACE CONTROL / SUBSEA - BOP CONTROL SYSTEM - HYDRAULIC TEST PUMP')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (130306, N'SURFACE CONTROL / SUBSEA - BOP CONTROL SYSTEM - UNINTERRUPTABLE POWER SUPPLIES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (130307, N'SURFACE CONTROL / SUBSEA - BOP CONTROL SYSTEM - ELECTRONIC DISTRIBUTION BOXES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (130308, N'SURFACE CONTROL / SUBSEA - BOP CONTROL SYSTEM - EVENT LOGGER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (130400, N'SURFACE CONTROL / SUBSEA - BOP CONTROL FLUID MIXING SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (130500, N'SURFACE CONTROL / SUBSEA - BOP CONTROL UNITS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (130501, N'SURFACE CONTROL / SUBSEA - BOP CONTROL UNITS - DRILL FLOOR CONTROL')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (130502, N'SURFACE CONTROL / SUBSEA - BOP CONTROL UNITS - REMOTE CONTROL PANEL')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (130513, N'SURFACE CONTROL / SUBSEA – DIVERTER SYSTEM -  FLOWLINE SEALS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (130600, N'SURFACE CONTROL / SUBSEA - BOP CONTROL EMERGENCY POWER PACK (BATTERIES)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (130700, N'SURFACE CONTROL / SUBSEA - BOP CONTROL PIPING')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (130701, N'SURFACE CONTROL / SUBSEA - BOP CONTROL PIPING - JUMPER HOSE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (130702, N'SURFACE CONTROL / SUBSEA - BOP CONTROL PIPING - FITTINGS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (130800, N'SURFACE CONTROL / SUBSEA - BOP CONTROL HOSE / CABLE SHEAVES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (130900, N'SURFACE CONTROL / SUBSEA - BOP CONTROL HOSE REELS WITH MOTORS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (130901, N'SURFACE CONTROL / SUBSEA - BOP CONTROL HOSE REELS WITH MOTORS - BOP SUBSEA HOSE BUNDLES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (130902, N'SURFACE CONTROL / SUBSEA - BOP CONTROL HOSE REELS WITH MOTORS - BOP SUBSEA HOSE FITTINGS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (131000, N'SURFACE CONTROL / SUBSEA - BOP CONTROL CABLE REELS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (131001, N'SURFACE CONTROL / SUBSEA - BOP CONTROL CABLE REELS - BOP CONTROL MULTIPLEX CABLE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (131002, N'SURFACE CONTROL / SUBSEA - BOP CONTROL CABLE REELS - BOP CONTROL MULTIPLEX CABLE TERMINATIONS / FITTINGS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (131100, N'SURFACE CONTROL / SUBSEA - BOP CONTROL AIR SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (131101, N'SURFACE CONTROL / SUBSEA - BOP CONTROL AIR SYSTEM - AIR COMPRESSOR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (131102, N'SURFACE CONTROL / SUBSEA - BOP CONTROL AIR SYSTEM - AIR RECEIVERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (131103, N'SURFACE CONTROL / SUBSEA - BOP CONTROL AIR SYSTEM - AFTER COOLERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (131104, N'SURFACE CONTROL / SUBSEA - BOP CONTROL AIR SYSTEM - AIR DRYER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (131300, N'SURFACE CONTROL / SUBSEA - BOP CONTROL POD RETRIEVING SHEAVES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (131400, N'SURFACE CONTROL / SUBSEA - BOP HANDLING EQUIPMENT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (131500, N'SURFACE CONTROL / SUBSEA - BOP TRANSFER SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (131501, N'SURFACE CONTROL / SUBSEA - BOP TRANSFER SYSTEM - TRANSFER CARTS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (131502, N'SURFACE CONTROL / SUBSEA - BOP TRANSFER SYSTEM - STORAGE CARTS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (131503, N'SURFACE CONTROL / SUBSEA - BOP TRANSFER SYSTEM - TRACKS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (131504, N'SURFACE CONTROL / SUBSEA - BOP TRANSFER SYSTEM - CART JACKING SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (132000, N'SURFACE CONTROL / SUBSEA - CHOKE AND KILL MANIFOLD / CHOKE AND KILL VALVES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (132001, N'SURFACE CONTROL / SUBSEA - CHOKE AND KILL MANIFOLD / CHOKE AND KILL VALVES - CHOKE AND KILL PIPING')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (132002, N'SURFACE CONTROL / SUBSEA - CHOKE AND KILL MANIFOLD / CHOKE AND KILL VALVES - MANUAL CHOKES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (132003, N'SURFACE CONTROL / SUBSEA - CHOKE AND KILL MANIFOLD / CHOKE AND KILL VALVES - POWER CHOKES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (132004, N'SURFACE CONTROL / SUBSEA - CHOKE AND KILL MANIFOLD / CHOKE AND KILL VALVES - VALVES (C&K MANIFOLD / BOP STACK MOUNTED)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (132005, N'CHEMICAL INJECTION SYSTEM (CHOKE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (132006, N'SURFACE CONTROL / SUBSEA – CHOKE AND KILL MANIFOLD -  FAILSAFE VALVE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (132100, N'SURFACE CONTROL / SUBSEA - CHOKE AND KILL LINE SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (132101, N'SURFACE CONTROL / SUBSEA - FLEX LOOPS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (132102, N'SURFACE CONTROL / SUBSEA - STABBING SUBS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (132103, N'SURFACE CONTROL / SUBSEA - HOSES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (132104, N'SURFACE CONTROL / SUBSEA - HYDRAULIC CONNECTOR (C&K LINE)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (132105, N'SURFACE CONTROL/SUBSEA - HYDRAULIC CONNECTOR (C&K LINE) - POLYPAK SEALS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (132500, N'SURFACE CONTROL / SUBSEA - RISER TENSIONING SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (132501, N'SURFACE CONTROL / SUBSEA - RISER TENSIONING SYSTEM - AIR COMPRESSOR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (132502, N'SURFACE CONTROL / SUBSEA - RISER TENSIONING SYSTEM - AIR DRYER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (132503, N'SURFACE CONTROL / SUBSEA - RISER TENSIONING SYSTEM - AIR RECEIVERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (132504, N'SURFACE CONTROL / SUBSEA - RISER TENSIONING SYSTEM - STORAGE REELS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (132505, N'SURFACE CONTROL / SUBSEA - RISER TENSIONING SYSTEM - SHEAVES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (132506, N'SURFACE CONTROL / SUBSEA - RISER TENSIONING SYSTEM - CONTROL PANEL')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (132507, N'SURFACE CONTROL / SUBSEA - RISER TENSIONING SYSTEM - TENSIONERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (132508, N'SURFACE CONTROL / SUBSEA - RISER TENSIONING SYSTEM - WIRE LINE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (132509, N'SURFACE CONTROL / SUBSEA - RISER TENSIONING SYSTEM - PIPING AND VALVES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (132550, N'SURFACE CONTROL / SUBSEA - ACTIVE RISER RECOIL SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (132600, N'SURFACE CONTROL / SUBSEA - RISER BUOYANCY COMPRESSED AIR SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (132601, N'SURFACE CONTROL / SUBSEA - RISER BUOYANCY COMPRESSED AIR SYSTEM - AIR COMPRESSOR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (132602, N'SURFACE CONTROL / SUBSEA - RISER BUOYANCY COMPRESSED AIR SYSTEM - AIR RECEIVERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (132603, N'SURFACE CONTROL / SUBSEA - RISER BUOYANCY COMPRESSED AIR SYSTEM - HOSES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (132604, N'SURFACE CONTROL / SUBSEA - RISER BUOYANCY COMPRESSED AIR SYSTEM - PIPING AND VALVES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (133100, N'SURFACE CONTROL / SUBSEA - GUIDE LINE / POD LINE TENSIONING SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (133101, N'SURFACE CONTROL / SUBSEA - GUIDE LINE / POD LINE TENSIONING SYSTEM - AIR COMPRESSOR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (133102, N'SURFACE CONTROL / SUBSEA - GUIDE LINE / POD LINE TENSIONING SYSTEM - AIR DRYER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (133103, N'SURFACE CONTROL / SUBSEA - GUIDE LINE / POD LINE TENSIONING SYSTEM - AIR RECEIVERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (133104, N'SURFACE CONTROL / SUBSEA - GUIDE LINE / POD LINE TENSIONING SYSTEM - STORAGE REELS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (133105, N'SURFACE CONTROL / SUBSEA - GUIDE LINE / POD LINE TENSIONING SYSTEM - SHEAVES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (133106, N'SURFACE CONTROL / SUBSEA - GUIDE LINE / POD LINE TENSIONING SYSTEM - CONTROL PANEL')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (133107, N'SURFACE CONTROL / SUBSEA - GUIDE LINE / POD LINE TENSIONING SYSTEM - TENSIONERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (133108, N'SURFACE CONTROL / SUBSEA - GUIDE LINE / POD LINE TENSIONING SYSTEM - WIRE LINE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (133109, N'SURFACE CONTROL / SUBSEA - GUIDE LINE / POD LINE TENSIONING SYSTEM - PIPING AND VALVES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (133700, N'SURFACE CONTROL / SUBSEA - T.V. GUIDE LINE SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (133900, N'SURFACE CONTROL / SUBSEA - RISER ANGLE SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (134000, N'SURFACE CONTROL / SUBSEA - HYDRAULIC WRENCH (BOP STACK)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (134400, N'SURFACE CONTROL / SUBSEA - B.O.P. TEST UNIT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (134401, N'SURFACE CONTROL / SUBSEA - B.O.P. TEST UNIT - PUMP WITH MOTOR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (134402, N'SURFACE CONTROL / SUBSEA - B.O.P. TEST UNIT - HOSES AND FITTINGS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (135000, N'SURFACE CONTROL / SUBSEA - DIVERTER SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (135001, N'SURFACE CONTROL / SUBSEA - DIVERTER SYSTEM - DIVERTER HOUSING')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (135002, N'SURFACE CONTROL / SUBSEA - DIVERTER SYSTEM - FIXED DIVERTER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (135003, N'SURFACE CONTROL / SUBSEA - DIVERTER SYSTEM - CONNECTOR / LATCH 30"')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (135004, N'SURFACE CONTROL / SUBSEA - DIVERTER SYSTEM - BALL JOINT(S)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (135005, N'SURFACE CONTROL / SUBSEA - DIVERTER SYSTEM - HOSE REEL')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (135006, N'SURFACE CONTROL / SUBSEA - DIVERTER SYSTEM - CONTROL PANEL')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (135007, N'SURFACE CONTROL / SUBSEA - DIVERTER SYSTEM - LOST CIRCULATION DUMP SPOOL')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (135008, N'SURFACE CONTROL / SUBSEA - DIVERTER SYSTEM - PIPING AND VALVES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (135009, N'SURFACE CONTROL / SUBSEA - DIVERTER SYSTEM - DIVERTER SPOOL')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (135010, N'SURFACE CONTROL / SUBSEA - DIVERTER SYSTEM - LOGIC INTERFACE WITH ACCUMULATOR UNIT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (135011, N'SURFACE CONTROL / SUBSEA - DIVERTER SYSTEM - DIVERTER RUNNING / TEST TOOLS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (135012, N'SURFACE CONTROL / SUBSEA - DIVERTER SYSTEM - DIVERTER / SLIP JOINT CROSSOVER SPOOL')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (135013, N'SURFACE CONTROL/SUBSEA - DIVERTER SYSTEM - FLOWLINE SEALS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (136000, N'SURFACE CONTROL / SUBSEA - BOP CONTROL PODS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (136001, N'SURFACE CONTROL/SUBSEA - BOP CONTROL PODS - PACKER SEAL  ')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (136100, N'SURFACE CONTROL / SUBSEA - BOP CONTROL PODS - VALVES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (136101, N'SURFACE CONTROL / SUBSEA - BOP CONTROL PODS - VALVES - SPM SPOOL TYPE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (136102, N'SURFACE CONTROL / SUBSEA - BOP CONTROL PODS - VALVES - SPM SHEAR SEAL SLIDE TYPE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (136103, N'SURFACE CONTROL / SUBSEA - BOP CONTROL PODS - VALVES - SOLENOID / PILOT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (136104, N'SURFACE CONTROL / SUBSEA - BOP CONTROL PODS - VALVES - PILOT OPERATED CHECK')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (136105, N'SURFACE CONTROL / SUBSEA - BOP CONTROL PODS - VALVES - NEEDLE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (136106, N'SURFACE CONTROL / SUBSEA - BOP CONTROL PODS - VALVES - FLOW METERING')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (136200, N'SURFACE CONTROL / SUBSEA - BOP CONTROL PODS - REGULATORS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (140000, N'MARINE RISER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (140100, N'MARINE RISER - MISCELLANEOUS ITEMS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (140300, N'MARINE RISER - SLIP JOINT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (140301, N'MARINE RISER - SLIP JOINT - RISER TENSIONER RING')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (140302, N'MARINE RISER – SLIP JOINT – PACKERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (140500, N'MARINE RISER - CHOKE AND KILL RETURN')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (140600, N'MARINE RISER - RISER HANDLING TOOL')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (140601, N'MARINE RISER - RISER HANDLING TOOL - RISER SPIDER WITH GIMBAL')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (140602, N'MARINE RISER - RISER HANDLING TOOL - TOOLS, TEST PINS, ETC.')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (140700, N'MARINE RISER - RISER JOINTS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (140701, N'MARINE RISER – RISER JOINT – SEAL / PACKING')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (140800, N'MARINE RISER - RISER CRADLES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (141000, N'MARINE RISER - BUOYANCY MODULES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (141500, N'MARINE RISER - INSTRUMENTED RISER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (141600, N'MARINE RISER - MUD CIRCULATION LINE TERM. JOINT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (142500, N'MARINE RISER - LOWER MARINE RISER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (142501, N'MARINE RISER - LOWER MARINE RISER - LMRP-STAB PLATE ASSEMBLY')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (142502, N'MARINE RISER - LOWER MARINE RISER - LOWER MARINE RISER PACKAGE HARDWARE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (142503, N'MARINE RISER - LOWER MARINE RISER - LOWER MARINE RISER PACKAGE FLUSH VALVE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (142600, N'MARINE RISER - EMERGENCY LMRRS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (142700, N'MARINE RISER - RISER CONNECTOR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (142800, N'MARINE RISER - EMERGENCY RISER RELEASE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (142900, N'MARINE RISER - BALL / FLEX JOINT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (143000, N'MARINE RISER - RISER ADAPTERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (143100, N'MARINE RISER - CROSSOVER SPOOL')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (143500, N'MARINE RISER - CONNECTOR GUIDE FRAME')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (143700, N'MARINE RISER - PIN CONNECTOR 30')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (144100, N'MARINE RISER - BOP CONTROL PODS (HYDRAULIC / MULTIPLEX)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (144101, N'MARINE RISER - BOP CONTROL PODS (HYDRAULIC / MULTIPLEX) - HYDRAULIC BOP CONTROL POD ASSEMBLY')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (144102, N'MARINE RISER - BOP CONTROL PODS (HYDRAULIC / MULTIPLEX) - SUBSEA ELECTRIC MODULE-BOP CONTROL POD')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (144103, N'MARINE RISER - BOP CONTROL PODS (HYDRAULIC / MULTIPLEX) - EMERGENCY DISCONNECT SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (144104, N'MARINE RISER - BOP CONTROL PODS (HYDRAULIC / MULTIPLEX) - DEADMAN CONTROL SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (144105, N'MARINE RISER - BOP CONTROL PODS (HYDRAULIC / MULTIPLEX) - WEDGE SEAL RETAINER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (144200, N'MARINE RISER - BOP BACK-UP ACOUSTIC SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (144300, N'MARINE RISER - BOP CONTROL POD RECEPTACLE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (144301, N'MARINE RISER - BOP CONTROL POD RECEPTACLE - UPPER FEMALE (LMR)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (144302, N'MARINE RISER - BOP CONTROL POD RECEPTACLE - LOWER FEMALE (BOP)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (144303, N'MARINE RISER - TEST BLOCK / PEDASTAL')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (145000, N'BLOWOUT PREVENTION AND WELLHEAD')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (145100, N'BLOWOUT PREVENTION AND WELLHEAD - BOP MISCELLANEOUS ITEMS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (145300, N'BLOWOUT PREVENTION AND WELLHEAD - LMR CONNECTOR MANDREL')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (145600, N'BLOWOUT PREVENTION AND WELLHEAD - BOP STACK RECEIVER PLATE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (145700, N'BLOWOUT PREVENTION AND WELLHEAD - BOP GUIDE FRAME ASSEMBLY')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (145701, N'BLOWOUT PREVENTION AND WELLHEAD - BOP GUIDE FRAME ASSEMBLY - POST SYSTEM  - FOUR POST')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (145702, N'BLOWOUT PREVENTION AND WELLHEAD - BOP GUIDE FRAME ASSEMBLY - POST TOPS AND SPEAR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (145703, N'BLOWOUT PREVENTION AND WELLHEAD - BOP GUIDE FRAME ASSEMBLY - HANDLING POST ASSEMBLY')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (145900, N'BLOWOUT PREVENTION AND WELLHEAD - ANNULAR PREVENTER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (145901, N'BLOWOUT PREVENTION AND WELLHEAD - ANNULAR PREVENTER - ANNULAR PREVENTER 13-5/8')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (145903, N'BLOWOUT PREVENTION AND WELLHEAD - ANNULAR PREVENTER - ANNULAR PREVENTER 18-3/4')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (145904, N'BLOWOUT PREVENTION AND WELLHEAD - ANNULAR PREVENTER - ANNULAR PREVENTER 20-3/4')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (145905, N'BLOWOUT PREVENTION AND WELLHEAD - ANNULAR PREVENTER - ANNULAR PREVENTER 21-1/4')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (145906, N'BLOWOUT PREVENTION AND WELLHEAD - ANNULAR PREVENTER - ANNULAR PREVENTER 29-1/2')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (145907, N'BLOWOUT PREVENTION AND WELLHEAD - ANNULAR PREVENTER - ANNULAR PREVENTER 30')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (145908, N'BLOWOUT PREVENTION AND WELLHEAD – ANNULAR PREVENTER –  POLYPAK SEALS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (145909, N'BLOWOUT PREVENTION AND WELLHEAD – ANNULAR PREVENTER –  PACKING ELEMENT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (146000, N'BLOWOUT PREVENTION AND WELLHEAD - RAM TYPE PREVENTER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (146001, N'BLOWOUT PREVENTION AND WELLHEAD - RAM TYPE PREVENTER - SMALL BORE BOP (SMALL STRING)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (146002, N'BLOWOUT PREVENTION AND WELLHEAD - RAM TYPE PREVENTER - RAM PREVENTER 13-5/8')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (146004, N'BLOWOUT PREVENTION AND WELLHEAD - RAM TYPE PREVENTER - RAM PREVENTER 18 3/4')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (146005, N'BLOWOUT PREVENTION AND WELLHEAD - RAM TYPE PREVENTER - RAM PREVENTER 20-3/4')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (146006, N'BLOWOUT PREVENTION AND WELLHEAD - RAM TYPE PREVENTER - RAM PREVENTER 21-1/4')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (146007, N'BLOWOUT PREVENTION AND WELLHEAD - RAM TYPE PREVENTER - PACKER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (146008, N'BLOWOUT PREVENTION AND WELLHEAD - RAM TYPE PREVENTER - BLADE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (146100, N'BLOWOUT PREVENTION AND WELLHEAD - RAM TYPE PREVENTER - BONNETS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (146300, N'BLOWOUT PREVENTION AND WELLHEAD - BOP STACK MOUNTED CONTROL PIPING')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (146301, N'BLOWOUT PREVENTION AND WELLHEAD - BOP STACK MOUNTED CONTROL PIPING - SHUTTLE VALVES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (146302, N'BLOWOUT PREVENTION AND WELLHEAD - BOP STACK MOUNTED CONTROL PIPING - HOSES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (146303, N'BLOWOUT PREVENTION AND WELLHEAD - BOP STACK MOUNTED CONTROL PIPING - FITTINGS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (146304, N'BLOWOUT PREVENTION AND WELLHEAD - BOP STACK MOUNTED CONTROL PIPING - DUMP VALVES - HYDRAULIC')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (146305, N'BLOWOUT PREVENTION AND WELLHEAD - BOP STACK MOUNTED CONTROL PIPING - REMOTE OPERATED VEHICLE INTERVENTION (HOT LINE) SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (146600, N'BLOWOUT PREVENTION AND WELLHEAD - SUBSEA ACCUMULATOR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (146700, N'BLOWOUT PREVENTION AND WELLHEAD - SPACER & ADAPTER SPOOLS / DOUBLE STUD ADAPTER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (146800, N'BLOWOUT PREVENTION AND WELLHEAD - WELLHEAD ASSEMBLY')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (146802, N'BLOWOUT PREVENTION AND WELLHEAD - WELLHEAD ASSEMBLY - SURFACE WELLHEAD')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (146804, N'BLOWOUT PREVENTION AND WELLHEAD - WELLHEAD ASSEMBLY - SUBSEA WELLHEAD')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (146806, N'BLOWOUT PREVENTION AND WELLHEAD - WELLHEAD ASSEMBLY - GLYCOL INJECTION SYSTEM (SURFACE / SUBSURFACE)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (146900, N'BLOWOUT PREVENTION AND WELLHEAD - PLUG & ABANDONMENT EQUIPMENT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (146902, N'BLOWOUT PREVENTION AND WELLHEAD - PLUG & ABANDONMENT EQUIPMENT - TEMPORARY ABANDONMENT CAPS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (146904, N'BLOWOUT PREVENTION AND WELLHEAD - PLUG & ABANDONMENT EQUIPMENT - TRASH CAPS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (147100, N'BLOWOUT PREVENTION AND WELLHEAD - HARDWARE (CLAMPS, FLANGES, GASKETS, HUBS, ETC.)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (147300, N'BLOWOUT PREVENTION AND WELLHEAD - WELLHEAD CONNECTOR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (147301, N'BLOWOUT PREVENTION AND WELLHEAD - WELLHEAD CONNECTOR - H4 CONNECTOR - WEAR RING')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (147302, N'BLOWOUT PREVENTION AND WELLHEAD – WELLHEAD CONNECTOR - SEALS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (147400, N'BLOWOUT PREVENTION AND WELLHEAD - BOP TEST TOOLS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (147500, N'BLOWOUT PREVENTION AND WELLHEAD - TEST STUMP')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (147600, N'BLOWOUT PREVENTION AND WELLHEAD - STORAGE STUMP')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (147700, N'BLOWOUT PREVENTION AND WELLHEAD - BOP RUNNING TOOLS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (147800, N'BLOWOUT PREVENTION AND WELLHEAD - GUIDELINE RE-ESTABLISHMENT SYSTEM TOOLS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (148000, N'BLOWOUT PREVENTION AND WELLHEAD - WELLHEAD GUIDE BASE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (148001, N'BLOWOUT PREVENTION AND WELLHEAD - WELLHEAD GUIDE BASE - TEMPORARY WELLHEAD')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (148002, N'BLOWOUT PREVENTION AND WELLHEAD - WELLHEAD GUIDE BASE - PERMANENT WELLHEAD')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (148003, N'BLOWOUT PREVENTION AND WELLHEAD - WELLHEAD GUIDE BASE - MUD MAT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (148100, N'BLOWOUT PREVENTION AND WELLHEAD - BOP EMERGENCY RECOVERY SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (150000, N'DRILL STRING AND WELL TUBULARS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (150100, N'DRILL STRING AND WELL TUBULARS - MISCELLANEOUS ITEMS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (150101, N'DRILL STRING AND WELL TUBULARS - TUBULAR INSPECTIONS - TUBULAR INSPECTIONS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (150102, N'DRILL STRING AND WELL TUBULARS - TUBULAR INSPECTIONS - TUBULAR REPAIRS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (150300, N'DRILL STRING AND WELL TUBULARS - KELLY')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (150500, N'DRILL STRING AND WELL TUBULARS - DRILL PIPE HANG OFF')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (150600, N'DRILL STRING AND WELL TUBULARS - DROP IN CHECK VALVE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (150700, N'DRILL STRING AND WELL TUBULARS - UPPER KELLY COCK')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (150800, N'DRILL STRING AND WELL TUBULARS - LOWER KELLY COCK')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (150900, N'DRILL STRING AND WELL TUBULARS - MUD CHECK VALVE (SURFACE)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (151000, N'DRILL STRING AND WELL TUBULARS - KELLY DRIVE BUSHING')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (151100, N'DRILL STRING AND WELL TUBULARS - WIPER RUBBER - WIPER RUBBER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (151101, N'DRILL STRING AND WELL TUBULARS - WIPER RUBBER - KELLY')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (151102, N'DRILL STRING AND WELL TUBULARS - WIPER RUBBER - DRILL PIPE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (151200, N'DRILL STRING AND WELL TUBULARS - DRILL PIPE 3-1/2')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (151201, N'DRILL STRING AND WELL TUBULARS - DRILL PIPE 3-1/2 - GRADE E-75')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (151202, N'DRILL STRING AND WELL TUBULARS - DRILL PIPE 3-1/2 - GRADE G-105')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (151203, N'DRILL STRING AND WELL TUBULARS - DRILL PIPE 3-1/2 - GRADE S-135')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (151204, N'DRILL STRING AND WELL TUBULARS - DRILL PIPE 3-1/2 - HEAVY WEIGHT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (151206, N'DRILL STRING AND WELL TUBULARS - DRILL PIPE 3-1/2 - GRADE X-95')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (151301, N'DRILL STRING AND WELL TUBULARS - DRILL PIPE 4 - GRADE E-75')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (151302, N'DRILL STRING AND WELL TUBULARS - DRILL PIPE 4 - GRADE G-105')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (151303, N'DRILL STRING AND WELL TUBULARS - DRILL PIPE 4 - GRADE S-135')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (151304, N'DRILL STRING AND WELL TUBULARS - DRILL PIPE 4 - HEAVY WEIGHT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (151306, N'DRILL STRING AND WELL TUBULARS - DRILL PIPE 4 - GRADE E-105')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (151400, N'DRILL STRING AND WELL TUBULARS - DRILL PIPE 5')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (151401, N'DRILL STRING AND WELL TUBULARS - DRILL PIPE 5 - GRADE E-75')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (151402, N'DRILL STRING AND WELL TUBULARS - DRILL PIPE 5 - GRADE G-105')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (151403, N'DRILL STRING AND WELL TUBULARS - DRILL PIPE 5 - GRADE S-135')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (151404, N'DRILL STRING AND WELL TUBULARS - DRILL PIPE 5 - HEAVY WEIGHT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (151406, N'DRILL STRING AND WELL TUBULARS - DRILL PIPE 5 - GRADE X-95')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (151450, N'DRILL STRING AND WELL TUBULARS - DRILL PIPE 5-1/2')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (151451, N'DRILL STRING AND WELL TUBULARS - DRILL PIPE 5-1/2 - GRADE E-75')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (151452, N'DRILL STRING AND WELL TUBULARS - DRILL PIPE 5-1/2 - GRADE G-105')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (151453, N'DRILL STRING AND WELL TUBULARS - DRILL PIPE 5-1/2 - GRADE S-135')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (151454, N'DRILL STRING AND WELL TUBULARS - DRILL PIPE 5-1/2 - HEAVY WEIGHT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (151456, N'DRILL STRING AND WELL TUBULARS - DRILL PIPE 5-1/2 - GRADE X-95')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (151500, N'DRILL STRING AND WELL TUBULARS - DRILL PIPE 6-5/8')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (151501, N'DRILL STRING AND WELL TUBULARS - DRILL PIPE 6-5/8 - GRADE E-75')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (151502, N'DRILL STRING AND WELL TUBULARS - DRILL PIPE 6-5/8 - GRADE G-105')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (151503, N'DRILL STRING AND WELL TUBULARS - DRILL PIPE 6-5/8 - GRADE S-135')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (151504, N'DRILL STRING AND WELL TUBULARS - DRILL PIPE 6-5/8 - HEAVY WEIGHT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (151506, N'DRILL STRING AND WELL TUBULARS - DRILL PIPE 6-5/8 - GRADE X-95')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (151600, N'DRILL STRING AND WELL TUBULARS - DRILL COLLARS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (151601, N'DRILL STRING AND WELL TUBULARS - DRILL COLLARS - DRILL COLLAR 4-3/4')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (151602, N'DRILL STRING AND WELL TUBULARS - DRILL COLLARS - DRILL COLLAR 6-1/4')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (151603, N'DRILL STRING AND WELL TUBULARS - DRILL COLLARS - DRILL COLLAR 6-1/2')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (151605, N'DRILL STRING AND WELL TUBULARS - DRILL COLLARS - DRILL COLLAR 7-3/4')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (151606, N'DRILL STRING AND WELL TUBULARS - DRILL COLLARS - DRILL COLLAR 8')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (151607, N'DRILL STRING AND WELL TUBULARS - DRILL COLLARS - DRILL COLLAR 8-1/4')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (151609, N'DRILL STRING AND WELL TUBULARS - DRILL COLLARS - DRILL COLLAR 9')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (151610, N'DRILL STRING AND WELL TUBULARS - DRILL COLLARS - DRILL COLLAR 9-1/2')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (151700, N'DRILL STRING AND WELL TUBULARS - STABILIZERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (151701, N'DRILL STRING AND WELL TUBULARS - STABILIZERS - STABILIZERS  5 5/8-8 - 3/4')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (151702, N'DRILL STRING AND WELL TUBULARS - STABILIZERS - STABILIZERS  8 7/8 -12 1/4')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (151703, N'DRILL STRING AND WELL TUBULARS - STABILIZERS - STABILIZERS 13-1/2 - 18-1/2')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (151704, N'DRILL STRING AND WELL TUBULARS - STABILIZERS - STABILIZERS 20 - 36')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (151800, N'DRILL STRING AND WELL TUBULARS - DRILLPIPE SERVICE TOOLS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (151900, N'DRILL STRING AND WELL TUBULARS - SUBS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (151901, N'DRILL STRING AND WELL TUBULARS - SUBS - KELLY SAVER SUBS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (151902, N'DRILL STRING AND WELL TUBULARS - SUBS - CROSSOVER SUBS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (151903, N'DRILL STRING AND WELL TUBULARS - SUBS - LIFTING SUBS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (151904, N'DRILL STRING AND WELL TUBULARS - SUBS - SHOCK SUBS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (151905, N'DRILL STRING AND WELL TUBULARS - SUBS - TEST SUBS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (151906, N'DRILL STRING AND WELL TUBULARS - SUBS - INSIDE BOP(SURFACE) SUBS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (151907, N'DRILL STRING AND WELL TUBULARS - SUBS - BIT SUBS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (151908, N'DRILL STRING AND WELL TUBULARS - SUBS - THROWAWAY SUBS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (151909, N'DRILL STRING AND WELL TUBULARS - SUBS - FAST SHUT-OFF COUPLINGS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (152100, N'DRILL STRING AND WELL TUBULARS - BUMPER SUBS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (152101, N'DRILL STRING AND WELL TUBULARS - BUMPER SUBS - BIT SUB  8 OD X 3 ID')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (152102, N'DRILL STRING AND WELL TUBULARS - BUMPER SUBS - BIT SUB  8-1/4 OD X 4-1/8')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (152103, N'DRILL STRING AND WELL TUBULARS - BUMPER SUBS - BIT SUB  6-1/2 OD X 2 ID')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (152105, N'DRILL STRING AND WELL TUBULARS - BUMPER SUBS - BIT SUB  4-3/4 OD X 2 ID')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (152200, N'DRILL STRING AND WELL TUBULARS - DRILL STRING INSPECTION TOOLS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (152500, N'DRILL STRING AND WELL TUBULARS - CEMENTING TOOLS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (152502, N'DRILL STRING AND WELL TUBULARS - CEMENTING TOOLS - RTTS TOOL')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (152504, N'DRILL STRING AND WELL TUBULARS - CEMENTING TOOLS - EZSV TOOL')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (152600, N'DRILL STRING AND WELL TUBULARS - CIRCULATING HEAD')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (152900, N'DRILL STRING AND WELL TUBULARS - FLOAT VALVE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (153100, N'DRILL STRING AND WELL TUBULARS - BITS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (153101, N'DRILL STRING AND WELL TUBULARS - BITS - ROCK BITS / INSERT BITS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (153102, N'DRILL STRING AND WELL TUBULARS - BITS - PDC BITS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (153103, N'DRILL STRING AND WELL TUBULARS - BITS - DIAMOND BITS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (153200, N'DRILL STRING AND WELL TUBULARS - MWD (DIRECTIONAL TOOLS / MEASURES)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (153201, N'DRILL STRING AND WELL TUBULARS - MWD (DIRECTIONAL TOOLS / MEASURES) - TURBO MOTOR / DRILL')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (153202, N'DRILL STRING AND WELL TUBULARS - MWD (DIRECTIONAL TOOLS / MEASURES) - STEERING TOOL')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (153203, N'DRILL STRING AND WELL TUBULARS - MWD (DIRECTIONAL TOOLS / MEASURES) - OTHER DIRECTIONAL TOOLS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (153204, N'WHIPSTOCKS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (153300, N'DRILL STRING AND WELL TUBULARS - CORE BARRELS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (153700, N'DRILL STRING AND WELL TUBULARS - CASING')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (153702, N'DRILL STRING AND WELL TUBULARS - CASING - DRIVE PIPE / CASING 48')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (153704, N'DRILL STRING AND WELL TUBULARS - CASING - DRIVE PIPE / CASING 36')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (153706, N'DRILL STRING AND WELL TUBULARS - CASING - DRIVE PIPE / CASING 30')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (153708, N'DRILL STRING AND WELL TUBULARS - CASING - DRIVE PIPE / CASING 26')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (153710, N'DRILL STRING AND WELL TUBULARS - CASING - DRIVE PIPE / CASING 24')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (153712, N'DRILL STRING AND WELL TUBULARS - CASING - CASING 20')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (153714, N'DRILL STRING AND WELL TUBULARS - CASING - CASING 18-5/8')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (153716, N'DRILL STRING AND WELL TUBULARS - CASING - CASING 16')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (153718, N'DRILL STRING AND WELL TUBULARS - CASING - CASING 13-3/8')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (153720, N'DRILL STRING AND WELL TUBULARS - CASING - CASING 11-7/8')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (153722, N'DRILL STRING AND WELL TUBULARS - CASING - CASING 11-3/4')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (153724, N'DRILL STRING AND WELL TUBULARS - CASING - CASING 10-3/4')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (153726, N'DRILL STRING AND WELL TUBULARS - CASING - CASING 9-5/8')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (153728, N'DRILL STRING AND WELL TUBULARS - CASING - CASING 8-5/8')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (153730, N'DRILL STRING AND WELL TUBULARS - CASING - CASING 7-5/8')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (153732, N'DRILL STRING AND WELL TUBULARS - CASING - CASING 7')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (153734, N'DRILL STRING AND WELL TUBULARS - CASING - CASING 6-5/8')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (153736, N'DRILL STRING AND WELL TUBULARS - CASING - CASING 5-1/2')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (153738, N'DRILL STRING AND WELL TUBULARS - CASING - CASING 5')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (153740, N'DRILL STRING AND WELL TUBULARS - CASING - CASING 4-1/2')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (153742, N'DRILL STRING AND WELL TUBULARS - CASING - CASING 4')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (153744, N'DRILL STRING AND WELL TUBULARS - CASING - CASING  3-1/2')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (153746, N'DRILL STRING AND WELL TUBULARS - CASING - CASING 2-7/8')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (153748, N'DRILL STRING AND WELL TUBULARS - CASING - TUBING  2-3/8')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (153750, N'DRILL STRING AND WELL TUBULARS - CASING - SPAGHETTI TUBING')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (153800, N'DRILL STRING AND WELL TUBULARS - CASING EQUIPMENT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (153801, N'DRILL STRING AND WELL TUBULARS - CASING EQUIPMENT - CASING DRIVING EQUIPMENT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (153803, N'DRILL STRING AND WELL TUBULARS - CASING EQUIPMENT - HOT HEAD')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (153805, N'DRILL STRING AND WELL TUBULARS - CASING EQUIPMENT - MUD LINE SUSPENSION SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (153806, N'DRILL STRING AND WELL TUBULARS - CASING EQUIPMENT - MUD LINE SUSPENSION RUNNING & CLEANING')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (153807, N'DRILL STRING AND WELL TUBULARS - CASING EQUIPMENT - CENTRALIZERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (153809, N'DRILL STRING AND WELL TUBULARS - CASING EQUIPMENT - FLOAT EQUIPMENT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (153811, N'DRILL STRING AND WELL TUBULARS - CASING EQUIPMENT - LINER HANGER (MECHANICAL)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (153813, N'DRILL STRING AND WELL TUBULARS - CASING EQUIPMENT - LINER HANGER (HYDRAULIC)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (153900, N'DRILL STRING AND WELL TUBULARS - THREAD PROTECTORS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (154100, N'DRILL STRING AND WELL TUBULARS - DRILL PIPE / CASING RUBBERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (154101, N'DRILL STRING AND WELL TUBULARS - DRILL PIPE / CASING RUBBERS - DRILL PIPE RUBBER MACHINE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (154300, N'DRILL STRING AND WELL TUBULARS - MUD SAVER BUCKET')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (154500, N'DRILL STRING AND WELL TUBULARS - HOLE OPENERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (154600, N'DRILL STRING AND WELL TUBULARS - REAMERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (154700, N'DRILL STRING AND WELL TUBULARS - UNDER REAMERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (154800, N'DRILL STRING AND WELL TUBULARS - CASING SCRAPERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (154900, N'DRILL STRING AND WELL TUBULARS - CASING CUTTERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (155000, N'FISHING TOOLS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (155100, N'FISHING TOOLS - MISCELLANEOUS ITEMS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (155200, N'FISHING TOOLS - SAFETY JOINTS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (155300, N'FISHING TOOLS - JARS MECHANICAL')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (155500, N'FISHING TOOLS - JARS HYDRAULIC')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (155700, N'FISHING TOOLS - OVERSHOTS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (155900, N'FISHING TOOLS - TAPER TAP')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (156000, N'FISHING TOOLS - IMPRESSION BLOCK')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (156100, N'FISHING TOOLS - MILLS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (156300, N'FISHING TOOLS - JUNK SUB')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (156500, N'FISHING TOOLS - FISHING BUMPER SUB')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (156700, N'FISHING TOOLS - JUNK BASKET')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (156900, N'FISHING TOOLS - MAGNET')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (157100, N'FISHING TOOLS - WASHOVER EQUIPMENT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (160000, N'PIPE RACKER ASSEMBLY')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (160100, N'PIPE RACKER ASSEMBLY - MISCELLANEOUS ITEMS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (160200, N'PIPE RACKER ASSEMBLY - PIPE RACKER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (160300, N'PIPE RACKER ASSEMBLY - CONVEYOR ASSEMBLY')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (160500, N'PIPE RACKER ASSEMBLY - RATCHET ASSEMBLY')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (160700, N'PIPE RACKER ASSEMBLY - DRIVE TUBE ASSEMBLY')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (160900, N'PIPE RACKER ASSEMBLY - LIFT ARM ASSEMBLY')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (161100, N'PIPE RACKER ASSEMBLY - INDEXER ASSEMBLY')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (161500, N'PIPE RACKER ASSEMBLY - FORWARD THROWOUT ASSEMBLY')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (161700, N'PIPE RACKER ASSEMBLY - THROWOUT ASSEMBLY')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (161900, N'PIPE RACKER ASSEMBLY - SKATE DRIVE ASSEMBLY')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (162100, N'PIPE RACKER ASSEMBLY - SKATE ASSEMBLY')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (162300, N'PIPE RACKER ASSEMBLY - PICKUP POST ASSEMBLY')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (162500, N'PIPE RACKER ASSEMBLY - PIPE RACKER / STABBER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (163000, N'PIPE RACKER ASSEMBLY - VERTICAL PIPE RACKER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (165000, N'PIPE HANDLING TOOLS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (165100, N'PIPE HANDLING TOOLS - MISCELLANEOUS ITEMS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (165200, N'PIPE HANDLING TOOLS - CASING RUNNING / TEST TOOLS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (165300, N'PIPE HANDLING TOOLS - SLIPS, DRILL PIPE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (165500, N'PIPE HANDLING TOOLS - SLIPS, DRILL COLLAR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (165700, N'PIPE HANDLING TOOLS - SLIPS, DRILL PIPE (POWER)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (165900, N'PIPE HANDLING TOOLS - SLIPS, CASING')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (166100, N'PIPE HANDLING TOOLS - SLIPS, TUBING')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (166300, N'PIPE HANDLING TOOLS - ELEVATORS DRILL PIPE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (166301, N'PIPE HANDLING TOOLS - ELEVATORS DRILL PIPE - PNEUMATIC ELEVATORS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (166500, N'PIPE HANDLING TOOLS - ELEVATORS DRILL COLLARS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (166600, N'PIPE HANDLING TOOLS - CASING ELEVATOR / SPIDER SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (166601, N'PIPE HANDLING TOOLS - CASING ELEVATOR / SPIDER SYSTEM - PNEUMATIC ELEVATORS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (166602, N'PIPE HANDLING TOOLS - CASING ELEVATOR / SPIDER SYSTEM - MANUAL ELEVATORS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (166700, N'PIPE HANDLING TOOLS - ELEVATORS CASING')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (166800, N'PIPE HANDLING TOOLS - ELEVATORS TUBING')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (166900, N'PIPE HANDLING TOOLS - ELEVATOR LINKS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (167200, N'PIPE HANDLING TOOLS - ELEVATORS DUAL SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (167300, N'PIPE HANDLING TOOLS - SPIDERS, TUBING')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (167700, N'PIPE HANDLING TOOLS - TONGS, DRILL PIPE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (167701, N'PIPE HANDLING TOOLS - TONGS, DRILL PIPE - POWER TONGS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (167800, N'PIPE HANDLING TOOLS - TONGS, TUBING')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (168100, N'PIPE HANDLING TOOLS - TONGS, CASING (MANUAL)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (168300, N'PIPE HANDLING TOOLS - TONGS, CASING (POWER)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (168400, N'PIPE HANDLING TOOLS - TONGS, CHAIN')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (168500, N'PIPE HANDLING TOOLS - SAFETY CLAMPS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (168600, N'PIPE HANDLING TOOLS - BIT BREAKERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (168800, N'PIPE HANDLING TOOLS - SPINNING WRENCH')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (168900, N'PIPE HANDLING TOOLS - KELLY STABBER ASSEMBLY')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (169100, N'PIPE HANDLING TOOLS - PIPE STABBER ASSEMBLY')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (169200, N'PIPE HANDLING TOOLS - IRON ROUGHNECK')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (169400, N'PIPE HANDLING TOOLS - HYDRAULIC CATHEAD')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (169500, N'PIPE HANDLING TOOLS - HAMMER, DRIVE PIPE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (170000, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (170100, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - MISCELLANEOUS ITEMS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (170200, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - MUD LAB')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (170300, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - MUD ANALYSIS TEST EQUIP')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (170301, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - MUD ANALYSIS TEST EQUIP - HIGH TEMP / PRESSURE MUD MONITORING')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (170400, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - ELECTRIC LOGGING FACILITIES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (170401, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - ELECTRIC LOGGING FACILITIES - LOG WHILE DRILLING (LWD) TOOL')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (170402, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - ELECTRIC LOGGING FACILITIES - CORE GAMMA TOOL')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (170500, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - MUD LOGGING FACILITIES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (170600, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - MOTION COMPENSATOR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (170700, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - WIRE LINE UNIT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (170701, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - WIRE LINE UNIT - WIRE LINE GAUGE RING')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (171000, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - SWABBING AND BAILING EQUIPMENT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (171300, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - DIVING EQUIPMENT SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (171301, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - DIVING EQUIPMENT SYSTEM - DIVING BELL')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (171302, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - DIVING EQUIPMENT SYSTEM - HOIST')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (171303, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - DIVING EQUIPMENT SYSTEM - AUXILIARIES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (171304, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - DIVING EQUIPMENT SYSTEM - DECOMPRESSION CHAMBER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (171400, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - REMOTE OPERATED VEHICLES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (171500, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - T.V. SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (171501, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - T.V. SYSTEM - UNDERWATER CAMERA')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (171502, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - T.V. SYSTEM - SURFACE MONITOR / CONTROLS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (171503, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - T.V. SYSTEM - UNDERWATER LIGHTING')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (171504, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - T.V. SYSTEM - PAN & TILT UNIT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (171505, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - T.V. SYSTEM - WINCH ASSEMBLY')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (171506, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - T.V. SYSTEM - ARMORED TV CABLE ASSEMBLY')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (171507, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - T.V. SYSTEM - DECK CABLE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (171508, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - T.V. SYSTEM - TRIDENT PIGTAIL ASSEMBLY')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (171509, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - T.V. SYSTEM - T.V. SHEAVE  36')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (171510, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - T.V. SYSTEM - T.V. RISER RUNNING TOOL')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (172000, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - INSTRUMENTS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (172001, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - INSTRUMENTS - DOWN HOLE DRIFT INDICATOR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (175000, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - WELL CONTROL AND TESTING')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (175001, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - WELL CONTROL AND TESTING - TEST EQUIPMENT (DOWNHOLE)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (175002, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - WELL CONTROL AND TESTING - TEST PLUG')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (175003, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - WELL CONTROL AND TESTING - TEST JOINT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (175100, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - FORMATION TESTING TOOLS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (175300, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - WELL TEST PIPING')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (175301, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - WELL TEST PIPING - GSF PIPING AND VALVES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (175400, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - OIL GAS SEPARATOR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (175500, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - WELL TEST BURNERS SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (175501, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - WELL TEST BURNERS SYSTEM - BURNERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (175502, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - WELL TEST BURNERS SYSTEM - BURNER BOOMS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (175503, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - WELL TEST BURNERS SYSTEM - TEST HEATER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (175504, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - WELL TEST BURNERS SYSTEM - TEST SURGE TANK')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (175505, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - WELL TEST BURNERS SYSTEM - TRANSFER PUMP')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (175506, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - WELL TEST BURNERS SYSTEM - CONTROL CONSOLE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (175507, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - WELL TEST BURNERS SYSTEM - DERRICK BURNER PLATFORM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (175600, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - TANKS TEST')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (176000, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - PUMPING SYSTEM, TEST')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (177000, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - WELLHEAD RUNNING AND TEST')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (178000, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - COMPLETION TANGIBLES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (178100, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - SCREEN, SAND CONTROL')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (178200, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - SUBSURFACE CONTROL SUBSEA SAFETY VALVE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (178202, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - SUBSURFACE CONTROL SUBSEA SAFETY VALVE - HYDRAULIC CONTROL LINE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (178300, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - COMPLETION TUBING ACCESSORIES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (178302, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - COMPLETION TUBING ACCESSORIES - TUBING HANGER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (178304, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - COMPLETION TUBING ACCESSORIES - COMPLETION SUBS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (178306, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - COMPLETION TUBING ACCESSORIES - COMPLETION PUP JOINTS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (178308, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - COMPLETION TUBING ACCESSORIES - COMPLETION CROSSOVER JOINTS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (178310, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - COMPLETION TUBING ACCESSORIES - COMPLETION MANDRELS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (178312, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - COMPLETION TUBING ACCESSORIES - COMPLETION NIPPLES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (178314, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - COMPLETION TUBING ACCESSORIES - BLAST JOINT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (178316, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - COMPLETION TUBING ACCESSORIES - BRIDGE PLUG')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (178400, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - COMPLETION PACKER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (178402, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - COMPLETION PACKER - PRODUCTION PACKER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (178404, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - COMPLETION PACKER - HYDRAULIC SET PACKER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (178500, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - SLEEVES & COUPLINGS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (178502, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - SLEEVES & COUPLINGS - SLIDING SLEEVE DOOR (SSD)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (178504, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - SLEEVES & COUPLINGS - PRODUCTION / CIRCULATION SLEEVES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (178506, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - SLEEVES & COUPLINGS - LOCKING SLEEVE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (178508, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - SLEEVES & COUPLINGS - FLOW COUPLING')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (178600, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - TUBING CONVEYED PERFORATING TOOLS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (178700, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - SURFACE (WELLHEAD) VALVES (CHRISTMAS TREE)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (178701, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - SURFACE (WELLHEAD) VALVES (CHRISTMAS TREE) - SURFACE (WELLHEAD) CONTROLS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (178800, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - PRODUCTION FLOW LINE / CONTROL UMBILICAL')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (179000, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - COMPLETION EQUIPMENT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (179100, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - COMPLETION CONSUMABLES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (179102, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - COMPLETION CONSUMABLES - COMPLETION FLUIDS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (179104, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - COMPLETION CONSUMABLES - NITROGEN GAS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (179200, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - COILED TUBING UNIT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (179300, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - COMPLETION FLUIDS FILTRATION UNIT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (179400, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - COMPLETION PUMPING UNITS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (179402, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - COMPLETION PUMPING UNITS - ACID PUMP')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (179404, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - COMPLETION PUMPING UNITS - NITROGEN PUMP')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (179406, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - STIMULATION PUMP')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (179500, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - IN-HOLE NAVIGATION SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (179600, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - SLICKLINE TOOLS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (179602, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - SLICKLINE TOOLS - RUNNING TOOLS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (179604, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - SLICKLINE TOOLS - PULLING TOOLS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (179606, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - SLICKLINE TOOLS - DOWNHOLE TEST TOOLS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (179608, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - SLICKLINE TOOLS - POSITIONING TOOLS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (179610, N'WELL CHECK, TEST, & COMPLETION EQUIPMENT - SLICKLINE TOOLS - PERFORATING TOOLS & BAILERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (180000, N'JACKING / SKIDDING SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (180100, N'JACKING / SKIDDING SYSTEM - MISCELLANEOUS ITEMS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (180200, N'JACKING / SKIDDING SYSTEM - LEGS - LEGS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (180201, N'JACKING / SKIDDING SYSTEM - LEGS - CHORDS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (180202, N'JACKING / SKIDDING SYSTEM - LEGS - BRACES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (180203, N'JACKING / SKIDDING SYSTEM - LEGS - SPUD CAN')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (180204, N'JACKING / SKIDDING SYSTEM - LEGS - GEAR RACK')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (180205, N'JACKING / SKIDDING SYSTEM - LEGS - LEG GUIDES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (180300, N'JACKING / SKIDDING SYSTEM - LEG RACK CHOCK SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (180400, N'JACKING / SKIDDING SYSTEM - LEG GEAR ASSEMBLY')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (180401, N'JACKING / SKIDDING SYSTEM - LEG GEAR ASSEMBLY - LEG GEAR UNIT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (180404, N'JACKING / SKIDDING SYSTEM - LEG GEAR ASSEMBLY - SHOCK PADS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (180405, N'JACKING / SKIDDING SYSTEM - LEG GEAR ASSEMBLY - HULL WEIGHING SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (180600, N'JACKING / SKIDDING SYSTEM - SKIDDING CONTROL CONSOLE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (180700, N'JACKING / SKIDDING SYSTEM - HYDRAULIC SKIDDING UNIT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (180800, N'JACKING / SKIDDING SYSTEM - JACKING CONTROL CONSOLE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (181000, N'JACKING / SKIDDING SYSTEM - LONGITUDINAL SKIDDING ASSEMBLY')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (181001, N'JACKING / SKIDDING SYSTEM - LONGITUDINAL SKIDDING ASSEMBLY - SKIDDER GEAR UNIT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (181004, N'JACKING / SKIDDING SYSTEM - LONGITUDINAL SKIDDING ASSEMBLY - HOSES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (181005, N'JACKING / SKIDDING SYSTEM - LONGITUDINAL SKIDDING ASSEMBLY - CANTILEVER BEAMS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (181006, N'JACKING / SKIDDING SYSTEM - LONGITUDINAL SKIDDING ASSEMBLY - CANTILEVER EXTENSION BEAMS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (181100, N'JACKING / SKIDDING SYSTEM - TRAVERSE SKIDDING ASSEMBLY')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (181101, N'JACKING / SKIDDING SYSTEM - TRAVERSE SKIDDING ASSEMBLY - SKIDDER GEAR UNIT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (181400, N'JACKING / SKIDDING SYSTEM - RAW WATER TOWER ASSEMBLY')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (181401, N'JACKING / SKIDDING SYSTEM - REDUCTION GEARS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (181500, N'JACKING / SKIDDING SYSTEM - RAW WATER RISER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (182500, N'JACKING / SKIDDING SYSTEM - LEG DEPTH TRANSMITTER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (182600, N'JACKING / SKIDDING SYSTEM - TANK GAUGING SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (182800, N'JACKING / SKIDDING SYSTEM - OUT OF LEVEL WARNING')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (183000, N'JACKING / SKIDDING SYSTEM - LEG JETTING SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (183001, N'JACKING / SKIDDING SYSTEM - LEG JETTING SYSTEM - MANIFOLD VALVES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (183002, N'JACKING / SKIDDING SYSTEM - LEG JETTING SYSTEM - HOSES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (183003, N'JACKING / SKIDDING SYSTEM - LEG JETTING SYSTEM - PIPING FITTINGS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (183100, N'JACKING / SKIDDING SYSTEM - LEG JETTING SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (183101, N'JACKING / SKIDDING SYSTEM - LEG JETTING SYSTEM - COMPRESSORS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (183102, N'JACKING / SKIDDING SYSTEM - LEG JETTING SYSTEM - WINCHES / HOISTS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (183103, N'JACKING / SKIDDING SYSTEM - LEG JETTING SYSTEM - TUBES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (183104, N'JACKING / SKIDDING SYSTEM - LEG JETTING SYSTEM - HOSES PIPINGHARDWARE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (183500, N'JACKING / SKIDDING SYSTEM - DRIVE PIPE SUPPORT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (183501, N'JACKING / SKIDDING SYSTEM - DRIVE PIPE SUPPORT - DRIVE PIPE PLATFORM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (183502, N'JACKING / SKIDDING SYSTEM - DRIVE PIPE SUPPORT - TENSIONER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (183503, N'JACKING / SKIDDING SYSTEM - DRIVE PIPE SUPPORT - PIPING / HOSES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (183504, N'JACKING / SKIDDING SYSTEM - DRIVE PIPE SUPPORT - CONTROL UNIT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (200000, N'HULL')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (200100, N'HULL - ASPHALT DAM (GBSI ONLY)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (200200, N'HULL - SOFT PATCH (GBSI ONLY)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (200300, N'HULL - HULL SHELL PLATING AND STIFFENING')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (200400, N'HULL - LONGITUDINAL BULKHEADS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (200500, N'HULL - TRANSVERSE BULKHEADS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (200501, N'HULL - TRANSVERSE BULKHEADS - FRAMES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (200502, N'HULL - TRANSVERSE BULKHEADS - BULKHEADS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (200600, N'HULL - DECK PLATING')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (200700, N'HULL - TANKS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (200710, N'HULL - TANKS - BALLAST TANKS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (200720, N'HULL - TANKS - PRELOAD TANKS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (200730, N'HULL - TANKS - DRILL WATER TANKS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (200740, N'HULL - TANKS - POTABLE WATER TANKS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (200750, N'HULL - TANKS - FUEL OIL / LUBE OIL TANKS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (200760, N'HULL - TANKS - BRINE TANKS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (200770, N'HULL - TANKS - BASE OIL TANKS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (200790, N'HULL - TANKS - MISCELLANEOUS TANKS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (200800, N'HULL - EQUIPMENT & MATERIAL STORAGE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (200801, N'HULL - EQUIPMENT & MATERIAL STORAGE - ANCHOR / MOORING CHAIN LOCKER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (200900, N'HULL - DRILLWELL / MOONPOOL')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (200901, N'HULL - DRILLWELL / MOONPOOL - GUIDE RAILS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (200902, N'HULL - DRILLWELL / MOONPOOL - SECURING ASSEMBLY')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (200903, N'HULL - DRILLWELL / MOONPOOL - PLUG FABRICATED STRUCTURE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (200904, N'HULL - DRILLWELL / MOONPOOL - MOON POOL PLUG')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (201000, N'HULL - CATHODIC PROTECTION (ANODES)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (201100, N'HULL - SEA CHESTS AND STRAINERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (201200, N'HULL - BILGE KEELS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (201300, N'HULL - SKEGS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (201400, N'HULL - RUDDERS, STRUCTURAL')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (201500, N'HULL - BULWARKS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (201600, N'HULL - DOUBLER PLATES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (201601, N'HULL - DOUBLER PLATES - SHELL')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (201602, N'HULL - DOUBLER PLATES - DECKING')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (201700, N'HULL - FOUNDATIONS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (201900, N'HULL - TANK STABILIZATION SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (202200, N'HULL - SUPERSTRUCTURES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (202201, N'HULL - SUPERSTRUCTURES - FORWARD HOUSE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (202202, N'HULL - SUPERSTRUCTURES - AFT HOUSE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (202203, N'HULL - SUPERSTRUCTURES - HELICOPTER DECK')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (202500, N'HULL - DERRICK SUBSTRUCTURE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (202700, N'HULL - RISER STORAGE RACKS ')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (202800, N'HULL - PIPE RACKER SUBSTRUCTURE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (202900, N'HULL - CASING RACK')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (203000, N'HULL - ACTIVE HOUSE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (203100, N'HULL - DECK HOUSE ')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (203200, N'HULL - BREAKWATERS, SPLASH GUARDS, DRIP PANS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (203300, N'HULL - HATCHES AND COAMING')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (203400, N'HULL - EXHAUST STACKS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (203500, N'HULL - BOOBY HATCHES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (203700, N'HULL - WALKWAYS / PLATFORMS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (203800, N'HULL - OPENINGS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (203801, N'HULL - OPENINGS - WATERTIGHT DOORS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (203802, N'HULL - OPENINGS - HATCHES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (203803, N'HULL - OPENINGS - SCUTTLES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (203804, N'HULL - OPENINGS - MANHOLES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (203805, N'HULL - OPENINGS - WINDOWS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (203806, N'HULL - OPENINGS - PORTABLE LIGHTS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (203807, N'HULL - OPENINGS - SKYLIGHTS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (203808, N'HULL - OPENINGS - DOCKING PLUGS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (203809, N'HULL - OPENINGS - OSENECK VENTS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (203900, N'HULL - FIXED BOP HANDLING STRUCTURE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (204000, N'HULL - VENTILATION')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (204001, N'HULL - VENTILATION - WATERTIGHT BULKHEAD PENETRATIONS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (204002, N'HULL - VENTILATION - DUCTWORK')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (204100, N'HULL - ANCHOR RACKS (COWCATCHERS)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (204300, N'HULL - WEATHER PROTECTION')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (204301, N'HULL - WEATHER PROTECTION - PERMANENT WINDWALLS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (204302, N'HULL - WEATHER PROTECTION - TEMPORARY WINDWALLS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (204400, N'HULL - HULL CASTINGS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (204401, N'HULL - HULL CASTINGS - HAWSE PIPES AND BOLSTERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (204402, N'HULL - HULL CASTINGS - BOLLARDS / BITS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (204403, N'HULL - HULL CASTINGS - CLEATS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (204404, N'HULL - HULL CASTINGS - CHOCKS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (204405, N'HULL - HULL CASTINGS - THRUSTER TUNNEL CASTINGS AND SHELL CASTINGS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (204406, N'HULL - HULL CASTINGS - RUDDER HORN CASTING')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (204407, N'HULL - HULL CASTINGS - STERN STRUTS / ARMS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (204500, N'HULL - RACKS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (204501, N'HULL - RACKS - OXY-ACETYLENE RACKS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (204502, N'HULL - RACKS - COMPRESSED AIR RACKS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (204600, N'HULL - MISCELLANEOUS PADEYES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (204700, N'HULL - WORK BENCHES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (204800, N'HULL - STORAGE BINS AND SHELVES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (204900, N'HULL - STORAGE RACKS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (205000, N'HULL - RIGGING')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (205001, N'HULL - RIGGING - MASTS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (205002, N'HULL - RIGGING - JACK STAFF')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (205003, N'HULL - RIGGING - BOOMS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (205200, N'HULL - BULKHEADS (NON-STRUCTURAL)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (205600, N'HULL - JOINER DOORS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (205700, N'HULL - OVERHEAD')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (205900, N'HULL - DECKING')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (206000, N'HULL - INSULATION')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (206100, N'HULL - LAGGING')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (206200, N'HULL - WOODWORK')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (206300, N'HULL - FABRICATED METAL CABINETS / DRESSERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (206400, N'HULL - CANVAS COVERING')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (206500, N'HULL - DRAPERIES AND BLINDS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (206600, N'HULL - PAINT AND OTHER LIQUID SURFACE PROTECTANTS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (210000, N'MAIN ENGINES / PROPULSION MACHINERY')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (210100, N'MAIN ENGINES / PROPULSION MACHINERY - MISCELLANEOUS ITEMS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (210200, N'MAIN ENGINES / PROPULSION MACHINERY - PROPULSION MOTORS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (210300, N'MAIN ENGINES / PROPULSION MACHINERY - PROPELLERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (210301, N'MAIN ENGINES / PROPULSION MACHINERY - PROPELLERS - CONES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (210302, N'MAIN ENGINES / PROPULSION MACHINERY - PROPELLERS - FAIRWATERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (210400, N'MAIN ENGINES / PROPULSION MACHINERY - PROPULSION SHAFT STRUTS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (210500, N'MAIN ENGINES / PROPULSION MACHINERY - PROPULSION SHAFT BEARINGS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (210600, N'MAIN ENGINES / PROPULSION MACHINERY - PROPULSION SHAFTS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (210700, N'MAIN ENGINES / PROPULSION MACHINERY - PROPULSION STERN TUBE SEAL')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (210800, N'MAIN ENGINES / PROPULSION MACHINERY - STEERING GEAR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (210801, N'MAIN ENGINES / PROPULSION MACHINERY - STEERING GEAR - QUADRANT OR TILLER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (210802, N'MAIN ENGINES / PROPULSION MACHINERY - STEERING GEAR - STEERING GEAR DRIVE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (210803, N'MAIN ENGINES / PROPULSION MACHINERY - STEERING GEAR - BEARINGS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (210804, N'MAIN ENGINES / PROPULSION MACHINERY - STEERING GEAR - RUDDERS / KORT NOZZLE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (210900, N'MAIN ENGINES / PROPULSION MACHINERY - SHAFT GROUNDING SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (211000, N'MAIN ENGINES / PROPULSION MACHINERY - PROPULSION REDUCTION GEARS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (211100, N'MAIN ENGINES / PROPULSION MACHINERY - PROPULSION SHAFT COUPLINGS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (211200, N'MAIN ENGINES / PROPULSION MACHINERY - SHAFT BRAKE ASSEMBLY')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (211300, N'MAIN ENGINES / PROPULSION MACHINERY - STEERING CONTROL SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (211500, N'MAIN ENGINES / PROPULSION MACHINERY - EMERGENCY STEERING')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (212300, N'MAIN ENGINES / PROPULSION MACHINERY - ENGINE AND PUMP ROOM INSTRUMENTS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (212500, N'MAIN ENGINES / PROPULSION MACHINERY - EMERGENCY DIESEL ENGINES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (212600, N'MAIN ENGINES / PROPULSION MACHINERY - AUXILIARY DIESEL ENGINES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (212610, N'MAIN ENGINES / PROPULSION MACHINERY - AUXILIARY DIESEL ENGINES - CAMP GENERATOR ENGINES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (212620, N'MAIN ENGINES / PROPULSION MACHINERY - AUXILIARY DIESEL ENGINES - CRANE ENGINES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (212630, N'MAIN ENGINES / PROPULSION MACHINERY - AUXILIARY DIESEL ENGINES - AIR START ENGINES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (212640, N'MAIN ENGINES / PROPULSION MACHINERY - AUXILIARY DIESEL ENGINES - CEMENT UNIT DIESEL ENGINES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (212650, N'MAIN ENGINES / PROPULSION MACHINERY - AUXILIARY DIESEL ENGINES - LAND-BASE VEHICLE/FORKLIFT DIESEL ENGINES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (212660, N'MAIN ENGINES / PROPULSION MACHINERY - AUXILIARY DIESEL ENGINES - OTHER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (212700, N'MAIN ENGINES / PROPULSION MACHINERY - RADIATORS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (212710, N'MAIN ENGINES / PROPULSION MACHINERY - RADIATORS - MAIN ENGINE RADIATORS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (212720, N'MAIN ENGINES / PROPULSION MACHINERY - RADIATORS - EMERGENCY GENERATOR RADIATOR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (212730, N'MAIN ENGINES / PROPULSION MACHINERY - RADIATORS - DRAWWORKS BRAKE COOLING RADIATORS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (212740, N'MAIN ENGINES / PROPULSION MACHINERY - RADIATORS - CRANE ENGINE RADIATORS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (212750, N'MAIN ENGINES / PROPULSION MACHINERY - RADIATORS - CAMP GENERATOR RATIATORS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (212760, N'MAIN ENGINES / PROPULSION MACHINERY - RADIATORS - HYDRAULIC POWER UNIT RADIATORS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (212770, N'MAIN ENGINES / PROPULSION MACHINERY - RADIATORS - CEMENT UNIT DIESEL ENGINE RADIATOR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (212780, N'MAIN ENGINES / PROPULSION MACHINERY - RADIATORS - LAND-BASED VEHICLE/FORKLIFT ENGINE RADIATORS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (212800, N'MAIN DIESEL ENGINE RADIATOR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (212900, N'MAIN ENGINES / PROPULSION MACHINERY - MAIN DIESEL ENGINES (PRIMARY]')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (212901, N'MAIN ENGINES / PROPULSION MACHINERY - MAIN DIESEL ENGINES (PRIMARY] - MAIN DIESEL ENGINES (SECONDARY)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (212911, N'MAIN ENGINES / PROPULSION MACHINERY - MAIN DIESEL ENGINES (PRIMARY] - JACKET WATER PREHEATING SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (212912, N'MAIN ENGINES / PROPULSION MACHINERY - MAIN DIESEL ENGINES (PRIMARY] - LOW TEMPERATURE HEAT EXCHANGER & CO')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (212913, N'MAIN ENGINES / PROPULSION MACHINERY - MAIN DIESEL ENGINES (PRIMARY] - HIGH TEMPERATURE HEAT EXCHANGER & C')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (212914, N'MAIN ENGINES / PROPULSION MACHINERY - MAIN DIESEL ENGINES (PRIMARY] - LOW TEMPERATURE COOLING PUMP / MOTOR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (212915, N'MAIN ENGINES / PROPULSION MACHINERY - MAIN DIESEL ENGINES (PRIMARY] - HIGH TEMPERATURE COOLING PUMP / MOTOR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (212916, N'MAIN ENGINES / PROPULSION MACHINERY - MAIN DIESEL ENGINES (PRIMARY] - LUBE OIL PRIMING PUMPS / MOTORS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (212917, N'MAIN ENGINES / PROPULSION MACHINERY - MAIN DIESEL ENGINES (PRIMARY] - FUEL OIL SERVICE PUMPS / MOTORS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (213000, N'MAIN ENGINES / PROPULSION MACHINERY - STARTING MOTORS FOR DIESELS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (213100, N'MAIN ENGINES / PROPULSION MACHINERY - ENGINE START AIR SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (213101, N'MAIN ENGINES / PROPULSION MACHINERY - ENGINE START AIR SYSTEM - MISCELLANEOUS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (213102, N'MAIN ENGINES / PROPULSION MACHINERY - ENGINE START AIR SYSTEM - COLD START AIR SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (213103, N'MAIN ENGINES / PROPULSION MACHINERY - ENGINE START AIR SYSTEM - AFTER COOLERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (213104, N'MAIN ENGINES / PROPULSION MACHINERY - ENGINE START AIR SYSTEM - AIR DRYER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (213105, N'MAIN ENGINES / PROPULSION MACHINERY - ENGINE START AIR SYSTEM - AIR RECEIVERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (213106, N'MAIN ENGINES / PROPULSION MACHINERY - ENGINE START AIR SYSTEM - PIPING AND VALVES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (213200, N'MAIN ENGINES / PROPULSION MACHINERY - CRANK CASE VENT SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (230000, N'AUXILIARY SYSTEMS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (230100, N'AUXILIARY SYSTEMS - RIG SERVICE AIR SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (230101, N'AUXILIARY SYSTEMS - RIG SERVICE AIR SYSTEM - MISCELLANEOUS ITEMS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (230102, N'AUXILIARY SYSTEMS - RIG SERVICE AIR SYSTEM - ELECTRIC AIR COMPRESSOR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (230103, N'AUXILIARY SYSTEMS - RIG SERVICE AIR SYSTEM - DIESEL AIR COMPRESSOR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (230104, N'AUXILIARY SYSTEMS - RIG SERVICE AIR SYSTEM - DIESEL EMERGENCY AIR COMPRESSOR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (230105, N'AUXILIARY SYSTEMS - RIG SERVICE AIR SYSTEM - ELECTRIC EMERGENCY AIR COMPRESSOR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (230106, N'AUXILIARY SYSTEMS - RIG SERVICE AIR SYSTEM - AFTER COOLERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (230107, N'AUXILIARY SYSTEMS - RIG SERVICE AIR SYSTEM - AIR DRYER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (230108, N'AUXILIARY SYSTEMS - RIG SERVICE AIR SYSTEM - AIR RECEIVERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (230109, N'AUXILIARY SYSTEMS - RIG SERVICE AIR SYSTEM - PIPING AND VALVES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (230200, N'AUXILIARY SYSTEMS - SEWAGE TREATMENT SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (230201, N'AUXILIARY SYSTEMS - SEWAGE TREATMENT SYSTEM - HOLDING TANKS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (230202, N'AUXILIARY SYSTEMS - SEWAGE TREATMENT SYSTEM - TREATMENT UNITS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (230203, N'AUXILIARY SYSTEMS - SEWAGE TREATMENT SYSTEM - TRANSFER PUMP & MOTOR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (230204, N'AUXILIARY SYSTEMS - SEWAGE TREATMENT SYSTEM - PIPING AND VALVES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (230205, N'AUXILIARY SYSTEMS - SEWAGE TREATMENT SYSTEM - SEWAGE TREATMENT MACERATOR PUMP')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (230300, N'AUXILIARY SYSTEMS - WATER MAKER SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (230301, N'AUXILIARY SYSTEMS - WATER MAKER SYSTEM - WATER MAKER UNITS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (230302, N'AUXILIARY SYSTEMS - WATER MAKER SYSTEM - PUMP WITH MOTOR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (230303, N'AUXILIARY SYSTEMS - WATER MAKER SYSTEM - PIPING AND VALVES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (230304, N'AUXILIARY SYSTEMS - WATER MAKER SYSTEM - ULTRAVIOLET PURIFIER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (230305, N'AUXILIARY SYSTEMS - WATER MAKER SYSTEM - CHEMICAL INJECTION PURIFIER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (230400, N'AUXILIARY SYSTEMS - OILY WATER SEPARATOR SYS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (230401, N'AUXILIARY SYSTEMS - OILY WATER SEPARATOR SYS - OILY WATER SEPARATOR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (230402, N'AUXILIARY SYSTEMS - OILY WATER SEPARATOR SYS - TRANSFER PUMP')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (230403, N'AUXILIARY SYSTEMS - OILY WATER SEPARATOR SYS - PIPING AND VALVES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (230404, N'AUXILIARY SYSTEMS - OILY WATER SEPARATOR SYS - BILGE MONITOR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (230500, N'AUXILIARY SYSTEMS - POLLUTION CONTROL')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (230700, N'AUXILIARY SYSTEMS - MISCELLANEOUS HYDRAULICS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (230701, N'AUXILIARY SYSTEMS - MISCELLANEOUS HYDRAULICS - POWER UNIT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (230702, N'AUXILIARY SYSTEMS - MISCELLANEOUS HYDRAULICS - CONTROLS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (230703, N'AUXILIARY SYSTEMS - MISCELLANEOUS HYDRAULICS - PIPING AND VALVES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (230800, N'AUXILIARY SYSTEMS - BOILERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (230900, N'AUXILIARY SYSTEMS - OIL CENTRIFUGE SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (230901, N'AUXILIARY SYSTEMS - OIL CENTRIFUGE SYSTEM - FUEL OIL')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (230902, N'AUXILIARY SYSTEMS - OIL CENTRIFUGE SYSTEM - LUBE OIL')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (231000, N'AUXILIARY SYSTEMS - HOT WATER HEATERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (231100, N'AUXILIARY SYSTEMS - WATER PRESSURE SETS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (231200, N'AUXILIARY SYSTEMS - DRINKING FOUNTAINS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (231301, N'AUXILIARY SYSTEMS - SLOW ROLL SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (231400, N'AUXILIARY SYSTEMS - AIR CONDITIONING / HEAT (MAIN)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (231401, N'AUXILIARY SYSTEMS - AIR CONDITIONING / HEAT (MAIN) - DEHUMIDIFIERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (231402, N'AUXILIARY SYSTEMS - AIR CONDITIONING / HEAT (MAIN) - AIR CONDITIONING / HEAT UNITS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (231403, N'AUXILIARY SYSTEMS - AIR CONDITIONING / HEAT (MAIN) - PORTABLE AIR CONDITIONER / HEATER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (231404, N'AUXILIARY SYSTEMS - AIR CONDITIONING / HEAT (MAIN) - GENERAL VENTILATION FANS & BLOWERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (231500, N'AUXILIARY SYSTEMS - AIR CONDITIONING (MAIN QUARTERS)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (231501, N'AUXILIARY SYSTEMS - AIR CONDITIONING (MAIN QUARTERS) - AIR CONDITIONING UNIT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (231502, N'AUXILIARY SYSTEMS - AIR CONDITIONING (MAIN QUARTERS) - FANS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (231503, N'AUXILIARY SYSTEMS - AIR CONDITIONING (MAIN QUARTERS) - VENT DUCTING')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (231550, N'AUXILIARY SYSTEMS - CHILLED WATER A/C SYSTEM (MAIN QUARTERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (231551, N'AUXILIARY SYSTEMS - CHILLED WATER A/C SYSTEM (MAIN QUARTERS - RECIPROCATING CHILLER UNIT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (231552, N'AUXILIARY SYSTEMS - CHILLED WATER A/C SYSTEM (MAIN QUARTERS - PUMP')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (231553, N'AUXILIARY SYSTEMS - CHILLED WATER A/C SYSTEM (MAIN QUARTERS - FANS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (231554, N'AUXILIARY SYSTEMS - CHILLED WATER A/C SYSTEM (MAIN QUARTERS - VENT DUCTING')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (231600, N'AUXILIARY SYSTEMS - HEATING SYSTEM (MAIN QUARTERS)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (231601, N'AUXILIARY SYSTEMS - HEATING SYSTEM (MAIN QUARTERS) - CENTRAL HEATING UNIT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (231602, N'AUXILIARY SYSTEMS - HEATING SYSTEM (MAIN QUARTERS) - IN DUCT HEATERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (231603, N'AUXILIARY SYSTEMS - HEATING SYSTEM (MAIN QUARTERS) - VENT DUCTING')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (231700, N'AUXILIARY SYSTEMS - ENGINE ROOM AND MACH VENTILIZATION')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (231701, N'AUXILIARY SYSTEMS - ENGINE ROOM AND MACH VENTILIZATION - FANS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (231702, N'AUXILIARY SYSTEMS - ENGINE ROOM AND MACH VENTILIZATION - VENT DUCTING')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (231800, N'AUXILIARY SYSTEMS - COMPARTMENT VENTILATION')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (231801, N'AUXILIARY SYSTEMS - COMPARTMENT VENTILATION - FANS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (231802, N'AUXILIARY SYSTEMS - COMPARTMENT VENTILATION - VENT DUCTING')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (231803, N'AUXILIARY SYSTEMS - COMPARTMENT VENTILATION - DUCTING FOR DRILL MOTORS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (231900, N'AUXILIARY SYSTEMS - GALLEY EXHAUST')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (232000, N'AUXILIARY SYSTEMS - AIR CONDITIONING (AUX. QUARTERS)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (232001, N'AUXILIARY SYSTEMS - AIR CONDITIONING (AUX. QUARTERS) - AIR CONDITIONING UNIT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (232002, N'AUXILIARY SYSTEMS - AIR CONDITIONING (AUX. QUARTERS) - FANS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (232003, N'AUXILIARY SYSTEMS - AIR CONDITIONING (AUX. QUARTERS) - VENT DUCTING')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (232050, N'AUXILIARY SYSTEMS - CHILLED WATER A/C SYSTEM (AUX. QUARTERS)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (232051, N'AUXILIARY SYSTEMS - CHILLED WATER A/C SYSTEM (AUX. QUARTERS) - RECIPROCATING CHILLER UNIT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (232052, N'AUXILIARY SYSTEMS - CHILLED WATER A/C SYSTEM (AUX. QUARTERS) - PUMP')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (232053, N'AUXILIARY SYSTEMS - CHILLED WATER A/C SYSTEM (AUX. QUARTERS) - FANS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (232054, N'AUXILIARY SYSTEMS - CHILLED WATER A/C SYSTEM (AUX. QUARTERS) - VENT DUCTING')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (232100, N'AUXILIARY SYSTEMS - HEATING SYSTEM (AUX. QUARTERS)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (232101, N'AUXILIARY SYSTEMS - HEATING SYSTEM (AUX. QUARTERS) - HEATING UNIT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (232102, N'AUXILIARY SYSTEMS - HEATING SYSTEM (AUX. QUARTERS) - IN DUCT HEATERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (232103, N'AUXILIARY SYSTEMS - HEATING SYSTEM (AUX. QUARTERS) - VENT DUCTING')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (232200, N'AUXILIARY SYSTEMS - QUARTERS VENTILATION SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (232201, N'AUXILIARY SYSTEMS - QUARTERS VENTILATION SYSTEM - FANS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (232202, N'AUXILIARY SYSTEMS - QUARTERS VENTILATION SYSTEM - DUCTING')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (232300, N'AUXILIARY SYSTEMS - WINTERIZATION SYSTEMS & EQUIPMENT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (232301, N'AUXILIARY SYSTEMS - RIG FLOOR WINTERIZATION')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (232302, N'AUXILIARY SYSTEMS - HEAT TRACING SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (232400, N'AUXILIARY SYSTEMS - TO BE DETERMINED')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (232500, N'AUXILIARY SYSTEMS - CONVECTION HEATERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (232600, N'AUXILIARY SYSTEMS - MISCELLANEOUS REFRIGERATION ITEMS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (232700, N'AUXILIARY SYSTEMS - REFRIGERATION COMPRESSOR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (232800, N'AUXILIARY SYSTEMS - WALK IN COOLERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (232900, N'AUXILIARY SYSTEMS - WALK IN FREEZERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (233000, N'AUXILIARY SYSTEMS - MAIN CONTROLS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (233200, N'AUXILIARY PUMP SYSTEMS ')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (233300, N'AUXILIARY PUMP SYSTEMS  - MAIN CIRCULATING PUMP')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (233301, N'AUXILIARY PUMP SYSTEMS  - MAIN CIRCULATING PUMP - PIPING AND VALVES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (233302, N'AUXILIARY PUMP SYSTEMS  - MAIN CIRCULATING PUMP - WATER CANNON PUMP & MOTOR ')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (233400, N'AUXILIARY PUMP SYSTEMS  - AUXILIARY CIRCULATION PUMP ')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (233401, N'AUXILIARY PUMP SYSTEMS  - AUXILIARY CIRCULATION PUMP  - PIPING AND VALVES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (233500, N'AUXILIARY PUMP SYSTEMS  - STANDBY CIRCULATING PUMP')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (233501, N'AUXILIARY PUMP SYSTEMS  - STANDBY CIRCULATING PUMP - PIPING AND VALVES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (233600, N'AUXILIARY PUMP SYSTEMS  - FORWARD CIRCULATING PUMP')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (233601, N'AUXILIARY PUMP SYSTEMS  - FORWARD CIRCULATING PUMP - PIPING AND VALVES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (233700, N'AUXILIARY PUMP SYSTEMS  - COOLING PUMP FOR HYDRAULIC')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (234300, N'AUXILIARY PUMP SYSTEMS  - GENERAL SERVICE PUMP')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (234301, N'AUXILIARY PUMP SYSTEMS  - GENERAL SERVICE PUMP - PIPING AND VALVES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (234500, N'AUXILIARY PUMP SYSTEMS  - MAIN FIRE PUMP 1&2 WITH MOTOR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (234501, N'AUXILIARY PUMP SYSTEMS  - MAIN FIRE PUMP 1&2 WITH MOTOR - PIPING AND VALVES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (234700, N'AUXILIARY PUMP SYSTEMS  - AUXILIARY FIRE PUMP ')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (234701, N'AUXILIARY PUMP SYSTEMS  - AUXILIARY FIRE PUMP  - PIPING AND VALVES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (234800, N'AUXILIARY PUMP SYSTEMS  - HELICOPTER FIRE PUMP')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (234801, N'AUXILIARY PUMP SYSTEMS  - HELICOPTER FIRE PUMP - PIPING AND VALVES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (234900, N'AUXILIARY PUMP SYSTEMS  - BALLAST PUMP WITH MOTOR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (234901, N'AUXILIARY PUMP SYSTEMS  - BALLAST PUMP WITH MOTOR - PIPING AND VALVES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (235000, N'AUXILIARY PUMP SYSTEMS  - BILGE PUMP WITH MOTOR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (235001, N'AUXILIARY PUMP SYSTEMS  - BILGE PUMP WITH MOTOR - PIPING AND VALVES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (235050, N'AUXILIARY PUMP SYSTEMS  - AIR OPERATED BILGE PUMPS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (235051, N'AUXILIARY PUMP SYSTEMS  - AIR OPERATED BILGE PUMPS - PIPING AND VALVES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (235100, N'AUXILIARY PUMP SYSTEMS  - SANITARY PUMP WITH MOTOR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (235101, N'AUXILIARY PUMP SYSTEMS  - SANITARY PUMP WITH MOTOR - PIPING AND VALVES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (235200, N'AUXILIARY PUMP SYSTEMS  - WASH WATER PUMP WITH MOTOR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (235201, N'AUXILIARY PUMP SYSTEMS  - WASH WATER PUMP WITH MOTOR - PIPING AND VALVES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (235300, N'AUXILIARY PUMP SYSTEMS  - POTABLE WATER PUMP')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (235301, N'AUXILIARY PUMP SYSTEMS  - PIPING AND VALVES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (235302, N'AUXILIARY PUMP SYSTEMS  - POTABLE WATER PUMP - COLD WATER PUMP WITH MOTOR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (235303, N'AUXILIARY PUMP SYSTEMS  - POTABLE WATER PUMP - HOT WATER PUMP WITH MOTOR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (235400, N'AUXILIARY PUMP SYSTEMS  - HYDRAULIC COOLING PUMP')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (235401, N'AUXILIARY PUMP SYSTEMS  - HYDRAULIC COOLING PUMP - PIPING AND VALVES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (235500, N'AUXILIARY PUMP SYSTEMS  - DRILL WATER TRANSFER PUMP')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (235501, N'AUXILIARY PUMP SYSTEMS  - DRILL WATER TRANSFER PUMP - PIPING AND VALVES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (235600, N'AUXILIARY PUMP SYSTEMS  - DRAWWORKS COOLING PUMP ')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (235601, N'AUXILIARY PUMP SYSTEMS  - DRAWWORKS COOLING PUMP  - DRAWWORKS PIPING WITH VALVES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (235700, N'AUXILIARY PUMP SYSTEMS  - TREATED FRESH WATER PUMP')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (235701, N'AUXILIARY PUMP SYSTEMS  - TREATED FRESH WATER PUMP - PIPING AND VALVES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (235800, N'AUXILIARY PUMP SYSTEMS  - FOAM SYSTEM SW PUMP')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (235900, N'AUXILIARY PUMP SYSTEMS  - PORTABLE EMERGENCY PUMP')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (236000, N'AUXILIARY PUMP SYSTEMS  - WATERMAKER FEED PUMP')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (236100, N'AUXILIARY PUMP SYSTEMS  - FUEL OIL TRANSFER PUMP')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (236101, N'AUXILIARY PUMP SYSTEMS  - FUEL OIL TRANSFER PUMP - PIPING AND VALVES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (236200, N'AUXILIARY PUMP SYSTEMS  - WASTE OIL / DRAIN PUMP')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (236300, N'AUXILIARY PUMP SYSTEMS  - FUEL OIL SERVICE PUMP')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (236400, N'AUXILIARY PUMP SYSTEMS  - LUBE OIL TRANSFER PUMP')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (236401, N'AUXILIARY PUMP SYSTEMS  - LUBE OIL TRANSFER PUMP - PIPING AND VALVES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (236500, N'AUXILIARY PUMP SYSTEMS  - DIRTY LUBE OIL PUMP')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (236501, N'AUXILIARY PUMP SYSTEMS  - DIRTY LUBE OIL PUMP - PIPING AND VALVES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (236600, N'AUXILIARY PUMP SYSTEMS  - HELICOPTER FUELING SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (236601, N'AUXILIARY PUMP SYSTEMS  - HELICOPTER FUELING SYSTEM - PIPING AND VALVES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (236602, N'AUXILIARY PUMP SYSTEMS  - HELICOPTER FUELING SYSTEM - TANKS FUEL STORAGE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (236603, N'AUXILIARY PUMP SYSTEMS  - HELICOPTER FUELING SYSTEM - TRANSFER PUMP WITH MOTOR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (236604, N'AUXILIARY PUMP SYSTEMS  - HELICOPTER FUELING SYSTEM - DISPENSING UNIT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (236605, N'AUXILIARY PUMP SYSTEMS  - HELICOPTER FUELING SYSTEM - PORTABLE STARTING SYSTEM (BATTERY)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (236700, N'AUXILIARY PUMP SYSTEMS  - HAND FUEL PUMP')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (236800, N'AUXILIARY PUMP SYSTEMS  - WATER TREATMENT / DESCALER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (236801, N'AUXILIARY PUMP SYSTEMS  - WATER TREATMENT / DESCALER - CHEMICAL FEED PUMP')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (236802, N'AUXILIARY PUMP SYSTEMS  - WATER TREATMENT / DESCALER - COBALT MAGNET')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (236803, N'AUXILIARY PUMP SYSTEMS  - WATER TREATMENT / DESCALER - SILVER ION UNIT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (236804, N'AUXILIARY PUMP SYSTEMS  - WATER TREATMENT / DESCALER - MARINE GROWTH PREVENTION UNIT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (237100, N'AUXILIARY PUMP SYSTEMS  - TANK GAUGING SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (237101, N'AUXILIARY PUMP SYSTEMS  - TANK GAUGING SYSTEM - ELECTRONIC PRELOAD TANKS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (237102, N'AUXILIARY PUMP SYSTEMS  - TANK GAUGING SYSTEM - ELECTRONIC DRILLWATER TANKS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (237103, N'AUXILIARY PUMP SYSTEMS  - TANK GAUGING SYSTEM - ELECTRONIC FUEL AND LUBE TANKS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (237104, N'AUXILIARY PUMP SYSTEMS  - TANK GAUGING SYSTEM - MANOMETERS DRILLWATER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (237105, N'AUXILIARY PUMP SYSTEMS  - TANK GAUGING SYSTEM - MANOMETERS FUEL AND LUBE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (238400, N'AUXILIARY PUMP SYSTEMS  - HEATING COIL SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (238500, N'AUXILIARY PUMP SYSTEMS  - TANK VENT AND SOUNDING TUBE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (238501, N'AUXILIARY PUMP SYSTEMS  - TANK VENT AND SOUNDING TUBE - VENT CHECKS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (238502, N'AUXILIARY PUMP SYSTEMS  - TANK VENT AND SOUNDING TUBE - DECK SOCKETS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (240000, N'ELECTRICAL SYSTEMS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (240100, N'ELECTRICAL SYSTEMS - AC MISCELLANEOUS ITEMS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (240200, N'ELECTRICAL SYSTEMS - MAIN AC SWITCHBOARD')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (240300, N'ELECTRICAL SYSTEMS - DISTRIBUTION AC SWITCH BOARD')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (240400, N'ELECTRICAL SYSTEMS - EMERGENCY AC SWITCH BOARD')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (240500, N'ELECTRICAL SYSTEMS - AC MOTORS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (240600, N'ELECTRICAL SYSTEMS - MAIN AC GENERATOR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (240601, N'ELECTRICAL SYSTEMS - MAIN AC GENERATOR - POWER MANAGEMENT SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (240800, N'ELECTRICAL SYSTEMS - ROTARY CONVERTERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (240900, N'ELECTRICAL SYSTEMS - SPEED VERNORS (ELECTRIC)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (241000, N'ELECTRICAL SYSTEMS - EXCITATION GENERATORS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (241200, N'ELECTRICAL SYSTEMS - EMERGENCY AC GENERATORS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (241300, N'ELECTRICAL SYSTEMS - ELECTRICAL DISTRIBUTION')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (241301, N'ELECTRICAL SYSTEMS - ELECTRICAL DISTRIBUTION - CABLE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (241302, N'ELECTRICAL SYSTEMS - ELECTRICAL DISTRIBUTION - WIREWAYS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (241303, N'ELECTRICAL SYSTEMS - ELECTRICAL DISTRIBUTION - BULKHEAD / DECK PENETRATIONS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (241400, N'ELECTRICAL SYSTEMS - POWER TRANSFORMERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (241600, N'ELECTRICAL SYSTEMS - POWER RECEPTACLES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (241800, N'ELECTRICAL SYSTEMS - SHORE POWER CONNECTION')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (241801, N'ELECTRICAL SYSTEMS - SHORE POWER CONNECTION - CABLE REEL / MOTOR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (242000, N'ELECTRICAL SYSTEMS - POWER / LIGHT PANELS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (242100, N'ELECTRICAL SYSTEMS - UNINTERRUPTABLE POWER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (242200, N'ELECTRICAL SYSTEMS - VOLTAGE REGULATORS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (242300, N'ELECTRICAL SYSTEMS - MOTOR GENERATOR SET')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (242400, N'ELECTRICAL SYSTEMS - AC PROTECTIVE DEVICES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (242410, N'ELECTRICAL SYSTEMS - POWER CIRCUIT BREAKERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (242600, N'ELECTRICAL SYSTEMS - DC MISCELLANEOUS ITEMS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (242700, N'ELECTRICAL SYSTEMS - DC SWITCHBOARD')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (242800, N'ELECTRICAL SYSTEMS - SCRS (THYRISTORS)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (242900, N'ELECTRICAL SYSTEMS - DC GENERATORS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (243100, N'ELECTRICAL SYSTEMS - EXCITATION GENERATOR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (243200, N'ELECTRICAL SYSTEMS - STATIC EXCITERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (243300, N'ELECTRICAL SYSTEMS - PROPULSION CONTROL SWITCH BOARD')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (243400, N'ELECTRICAL SYSTEMS - PROPULSION CONTROL CONSOLE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (243500, N'ELECTRICAL SYSTEMS - DC DRILLING SWITCHBOARD')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (243600, N'ELECTRICAL SYSTEMS - DC DRIVE MOTORS (SPARE)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (243700, N'ELECTRICAL SYSTEMS - DC SYSTEM DYNAMIC BRAKE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (243800, N'ELECTRICAL SYSTEMS - OFFICE CONSOLE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (244000, N'ELECTRICAL SYSTEMS - TACHOMETER GENERATORS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (244200, N'ELECTRICAL SYSTEMS - DC WELDING OUTLETS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (244400, N'ELECTRICAL SYSTEMS - STORAGE BATTERIES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (244500, N'ELECTRICAL SYSTEMS - BATTERY CHARGERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (245200, N'ELECTRICAL SYSTEMS - GENERAL RIG LIGHTING')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (245400, N'ELECTRICAL SYSTEMS - DRILL WELL LIGHTING')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (245600, N'ELECTRICAL SYSTEMS - DERRICK LIGHTING')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (245700, N'ELECTRICAL SYSTEMS - AIRCRAFT WARNING LIGHT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (245800, N'ELECTRICAL SYSTEMS - NAVIGATION AND RUNNING LIGHTS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (245900, N'ELECTRICAL SYSTEMS - UNDERWATER TASK LIGHTS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (246000, N'ELECTRICAL SYSTEMS - HELICOPTER AREA LIGHTS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (246100, N'ELECTRICAL SYSTEMS - OBSTRUCTION LIGHTS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (246600, N'ELECTRICAL SYSTEMS - MISCELLANEOUS ITEMS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (246700, N'ELECTRICAL SYSTEMS - GENERAL ALARM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (246800, N'ELECTRICAL SYSTEMS - MISCELLANEOUS SYSTEM ALARMS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (246900, N'ELECTRICAL SYSTEMS - ENGINE ORDER TELEGRAPH')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (247100, N'ELECTRICAL SYSTEMS - RUDDER ANGLE INDICATOR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (247300, N'ELECTRICAL SYSTEMS - SPEED LOG INDICATOR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (247800, N'ELECTRICAL SYSTEMS - GYRO FAILURE SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (247900, N'ELECTRICAL SYSTEMS - DIAL TELEPHONE SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (248100, N'ELECTRICAL SYSTEMS - SOUND POWER TELEPHONE (ECALL)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (248300, N'ELECTRICAL SYSTEMS - PA (PUBLIC ADDRESS) SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (249000, N'ELECTRICAL SYSTEMS - ENGINE ALARM SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (249001, N'ELECTRICAL SYSTEMS - ENGINE ALARM SYSTEM - ENGINE ROOM WATCH KEEPER SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (250000, N'COMMUNICATIONS / ELECTRONICS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (250100, N'COMMUNICATIONS / ELECTRONICS - MISCELLANEOUS COMMUNICATION')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (250101, N'COMMUNICATIONS / ELECTRONICS - MISCELLANEOUS COMMUNICATION - HAM RADIO')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (250400, N'COMMUNICATIONS / ELECTRONICS - DISTRESS WATCH TRANSCEIVER (2182 KHZ)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (250500, N'COMMUNICATIONS / ELECTRONICS - RADIO TELEGRAPH')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (250600, N'COMMUNICATIONS / ELECTRONICS - ANTENNAS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (250601, N'COMMUNICATIONS / ELECTRONICS - ANTENNAS - WHIP ANTENNA')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (250602, N'COMMUNICATIONS / ELECTRONICS - ANTENNAS - DIPOLE ANTENNA')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (250603, N'COMMUNICATIONS / ELECTRONICS - ANTENNAS - ANTENNA COUPLER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (250700, N'COMMUNICATIONS / ELECTRONICS - MULTI-FREQUENCY TRANSCEIVER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (250800, N'COMMUNICATIONS / ELECTRONICS - SATELLITE COMMUNICATIONS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (250900, N'COMMUNICATIONS / ELECTRONICS - SINGLE SIDE BAND RADIO / RADIO TELEPHONE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (251000, N'COMMUNICATIONS / ELECTRONICS - VHF RADIO')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (251100, N'COMMUNICATIONS / ELECTRONICS - LIFE BOAT RADIO')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (251500, N'COMMUNICATIONS / ELECTRONICS - WALKIE TALKIE UNITS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (251600, N'COMMUNICATIONS / ELECTRONICS - DATA LOGGING SYSTEM (GBSI ONLY)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (251700, N'COMMUNICATIONS / ELECTRONICS - COMPUTER SYSTEMS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (251701, N'COMMUNICATIONS / ELECTRONICS - COMPUTER SYSTEMS - STABILITY COMPUTER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (251702, N'COMMUNICATIONS / ELECTRONICS - COMPUTER SYSTEMS - GENERAL USE,  COMPUTER HARDWARE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (251703, N'COMMUNICATIONS / ELECTRONICS - COMPUTER SYSTEMS - GENERAL USE,  COMPUTER SOFTWARE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (251800, N'COMMUNICATIONS / ELECTRONICS - CELLULAR TELEPHONE SYSTEMS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (252000, N'COMMUNICATIONS / ELECTRONICS - CLOSED CIRCUIT TELEVISION SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (252001, N'COMMUNICATIONS / ELECTRONICS - CLOSED CIRCUIT TELEVISION SYSTEM - MONITOR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (252002, N'COMMUNICATIONS / ELECTRONICS - CLOSED CIRCUIT TELEVISION SYSTEM - CAMERA')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (252003, N'COMMUNICATIONS / ELECTRONICS - CLOSED CIRCUIT TELEVISION SYSTEM - PAN & TILT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (252004, N'COMMUNICATIONS / ELECTRONICS - CLOSED CIRCUIT TELEVISION SYSTEM - CONTROL UNIT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (252600, N'COMMUNICATIONS / ELECTRONICS - MISCELLANEOUS NAVIGATION')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (252700, N'COMMUNICATIONS / ELECTRONICS - RADAR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (252800, N'COMMUNICATIONS / ELECTRONICS - HELICOPTER HOMING BEACON (NON-DIRECTIONAL – NDB)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (253100, N'COMMUNICATIONS / ELECTRONICS - LORAN')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (253200, N'COMMUNICATIONS / ELECTRONICS - OMEGA')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (253300, N'COMMUNICATIONS / ELECTRONICS - FATHOMETER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (253500, N'COMMUNICATIONS / ELECTRONICS - SATELLITE NAVIGATION SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (253600, N'COMMUNICATIONS / ELECTRONICS - GLOBAL MARITIME DISTRESS AND SAFETY SYSTEM (GMDSS)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (253700, N'COMMUNICATIONS / ELECTRONICS - FACSIMILE CONVERTER (WEATHER MAP PRINTER)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (253900, N'COMMUNICATIONS / ELECTRONICS - RADIO DIRECTION FINDER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (254000, N'COMMUNICATIONS / ELECTRONICS - EMERGENCY POSITION INDICATING RADIO BEACON (EPIRB)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (254100, N'COMMUNICATIONS / ELECTRONICS - AUTOMATIC PILOT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (254300, N'COMMUNICATIONS / ELECTRONICS - GYRO COMPASS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (254500, N'COMMUNICATIONS / ELECTRONICS - MAGNETIC COMPASS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (254600, N'COMMUNICATIONS / ELECTRONICS - CURRENT METER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (254700, N'COMMUNICATIONS / ELECTRONICS - WHISTLE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (254800, N'COMMUNICATIONS / ELECTRONICS - WAVE RIDER BUOY SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (254900, N'COMMUNICATIONS / ELECTRONICS - FOG HORN')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (255000, N'COMMUNICATIONS / ELECTRONICS - ANEMOMETER (WIND INDICATOR)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (255010, N'COMMUNICATIONS / ELECTRONICS - ANEMOMETER (WIND INDICATOR) - ENVIRONMENTAL MONITORING')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (255020, N'COMMUNICATIONS / ELECTRONICS - ANEMOMETER (WIND INDICATOR) - TANK TEMP INDICATOR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (255050, N'COMMUNICATIONS / ELECTRONICS - ANEMOMETER (WIND INDICATOR) - ACOUSTIC HOLE POSITION INDICATOR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (255100, N'COMMUNICATIONS / ELECTRONICS - MISCELLANEOUS DYNAMIC POSITION')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (255300, N'COMMUNICATIONS / ELECTRONICS - MAIN COMPUTER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (255500, N'COMMUNICATIONS / ELECTRONICS - POSITIONING CONTROL CONSOLES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (255600, N'COMMUNICATIONS / ELECTRONICS - VESSEL MANAGEMENT SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (255601, N'COMMUNICATIONS / ELECTRONICS - VESSEL MANAGEMENT SYSTEM - EMERGENCY SHUT DOWN SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (255700, N'COMMUNICATIONS / ELECTRONICS - TELETYPE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (255900, N'COMMUNICATIONS / ELECTRONICS - HYDROPHONE-SHIP LOCATION RELATIVE TO HOLE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (256100, N'COMMUNICATIONS / ELECTRONICS - BEACONS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (257500, N'COMMUNICATIONS / ELECTRONICS - RE-ENTRY SYSTEMS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (258000, N'COMMUNICATIONS / ELECTRONICS - TAUT WIRE SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (259000, N'COMMUNICATIONS / ELECTRONICS - BALLAST CONTROL SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (259100, N'COMMUNICATIONS / ELECTRONICS - STRAIN GAUGE SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (259101, N'COMMUNICATIONS / ELECTRONICS - STRAIN GAUGE SYSTEM - READ-OUT PANEL')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (259102, N'COMMUNICATIONS / ELECTRONICS - STRAIN GAUGE SYSTEM - GAUGES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (259103, N'COMMUNICATIONS / ELECTRONICS - STRAIN GAUGE SYSTEM - ICE PANELS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (260000, N'MOORING SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (260100, N'MOORING SYSTEM - MISCELLANEOUS MOORING')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (261000, N'MOORING SYSTEM - CAPSTANS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (261001, N'MOORING SYSTEM - CAPSTANS - CONTROL CONSOLE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (261600, N'MOORING SYSTEM - TOW LINE / BRIDLE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (261601, N'MOORING SYSTEM - TOW LINE / BRIDLE - MAIN LINE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (261602, N'MOORING SYSTEM - TOW LINE / BRIDLE - EMERGENCY LINE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (261603, N'MOORING SYSTEM - TOW LINE / BRIDLE - SMIT BRACKET')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (261800, N'MOORING SYSTEM - HAWSER LINES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (262500, N'MOORING SYSTEM - DOCKING FENDERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (262600, N'MOORING SYSTEM - WORK BOAT TIE-UP SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (262601, N'MOORING SYSTEM - WORK BOAT TIE-UP SYSTEM - WORK BOAT MOORING ROPE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (265100, N'MOORING SYSTEM - MOORING SYSTEM CONTROLS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (265150, N'MOORING SYSTEM - MOORING WINCH / WINDLASS COMBINATION')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (265151, N'MOORING SYSTEM - MOORING WINCH / WINDLASS COMBINATION - DRIVE UNIT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (265152, N'MOORING SYSTEM - MOORING WINCH / WINDLASS COMBINATION - DC MOTOR COOLING SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (265153, N'MOORING SYSTEM - MOORING WINCH / WINDLASS COMBINATION - AUXILIARY BRAKE SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (265154, N'MOORING SYSTEM - MOORING WINCH / WINDLASS COMBINATION - PIPING AND VALVES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (265200, N'MOORING SYSTEM - MOORING WINDLASS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (265300, N'MOORING SYSTEM - MOORING WINDLASS DRIVE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (265301, N'MOORING SYSTEM - MOORING WINDLASS DRIVE - DC MOTOR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (265302, N'MOORING SYSTEM - MOORING WINDLASS DRIVE - AC MOTORS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (265303, N'MOORING SYSTEM - MOORING WINDLASS DRIVE - DIESEL ENGINE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (265400, N'MOORING SYSTEM - MOORING ANCHOR CHAIN')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (265600, N'MOORING SYSTEM - CHAINLINKS / KENTER / CONN LINKS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (265700, N'MOORING SYSTEM - SHACKLES CHAIN')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (265800, N'MOORING SYSTEM - MOORING ANCHORS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (266000, N'MOORING SYSTEM - ANCHOR PILES, PILINGS & CAISSONS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (266001, N'MOORING SYSTEM - ANCHOR PILES, PILINGS & CAISSONS - DRILLED-IN ANCHOR PILES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (266002, N'MOORING SYSTEM - ANCHOR PILES, PILINGS & CAISSONS - PILINGS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (266003, N'MOORING SYSTEM - ANCHOR PILES, PILINGS & CAISSONS - CAISSONS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (266200, N'MOORING SYSTEM - CHAIN FAIRLEADERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (266400, N'MOORING SYSTEM - CHAIN STOPPERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (266600, N'MOORING SYSTEM - CARPENTER STOPPERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (266800, N'MOORING SYSTEM - CHAIN CHOCKS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (267000, N'MOORING SYSTEM - MOORING WINCHES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (267100, N'MOORING SYSTEM - WARPING WINCH')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (267200, N'MOORING SYSTEM - WIRE ROPE FAIRLEADERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (267400, N'MOORING SYSTEM - MOORING WIRE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (267600, N'MOORING SYSTEM - CROWN LINES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (267800, N'MOORING SYSTEM - WIRE LINE HARDWARE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (268000, N'MOORING SYSTEM - BUOYS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (268100, N'MOORING SYSTEM - CHAIN CHASER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (268200, N'MOORING SYSTEM - ANCHOR GRAPPLE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (268400, N'MOORING SYSTEM - CROWN LINE STORAGE REELS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (268500, N'MOORING SYSTEM - EMERGENCY ANCHOR MARKING')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (268600, N'MOORING SYSTEM - MOORING TENSION INDICATOR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (268700, N'MOORING SYSTEM - THRUSTER SYSTEM MISC.')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (268800, N'MOORING SYSTEM - THRUSTERS (FIXED TUNNEL)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (268801, N'MOORING SYSTEM - THRUSTERS (FIXED TUNNEL) - THRUSTERS (AZIMUTHING RETRACTABLE)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (268900, N'MOORING SYSTEM - THRUSTER GEAR BOXES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (269000, N'MOORING SYSTEM - THRUSTER WEAR RINGS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (269100, N'MOORING SYSTEM - THRUSTERS, DRIVE SHAFTS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (269200, N'MOORING SYSTEM - THRUSTERS DRIVE MOTORS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (269201, N'MOORING SYSTEM - THRUSTERS DRIVE MOTORS - THRUSTER DRIVE FREQUENCY CONVERTERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (269202, N'MOORING SYSTEM - THRUSTERS DRIVE MOTORS - THRUSTER DRIVE TRANSFORMERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (269300, N'MOORING SYSTEM - THRUSTER RETRACTION SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (270000, N'QUARTER AND SHOP EQUIPMENT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (270100, N'QUARTER AND SHOP EQUIPMENT - MISCELLANEOUS Q&S EQUIPMENT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (270200, N'QUARTER AND SHOP EQUIPMENT - RIG FURNITURE (ALL QUARTERS)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (270300, N'QUARTER AND SHOP EQUIPMENT - RIG OFFICE EQUIPMENT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (270400, N'QUARTER AND SHOP EQUIPMENT - SANITARY FIXTURES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (271100, N'QUARTER AND SHOP EQUIPMENT - LAUNDRY EQUIPMENT MISC.')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (271300, N'QUARTER AND SHOP EQUIPMENT - CLOTHES WASHER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (271400, N'QUARTER AND SHOP EQUIPMENT - CLOTHES DRYER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (271800, N'QUARTER AND SHOP EQUIPMENT - MISCELLANEOUS PHOTO EQUIPMENT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (271900, N'QUARTER AND SHOP EQUIPMENT - CAMERAS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (272300, N'QUARTER AND SHOP EQUIPMENT - RECREATION EQUIPMENT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (272400, N'QUARTER AND SHOP EQUIPMENT - ATHLETIC EQUIPMENT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (272500, N'QUARTER AND SHOP EQUIPMENT - MOVIE PROJECTORS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (272600, N'QUARTER AND SHOP EQUIPMENT - TELEVISION / VIDEO PLAYERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (272700, N'QUARTER AND SHOP EQUIPMENT - STEREO EQUIPMENT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (272800, N'QUARTER AND SHOP EQUIPMENT - SATELLITE ENTERTAIN DISC')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (272900, N'QUARTER AND SHOP EQUIPMENT - CENTRAL TV / RADIO SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (273100, N'QUARTER AND SHOP EQUIPMENT - MISCELLANEOUS HOSPITAL')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (273200, N'QUARTER AND SHOP EQUIPMENT - OPERATING TABLE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (273400, N'QUARTER AND SHOP EQUIPMENT - SPECIAL HOSPITAL LIGHTING')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (274100, N'QUARTER AND SHOP EQUIPMENT - PILOT-CHART HOUSE EQUIP')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (274200, N'QUARTER AND SHOP EQUIPMENT - NAVIGATION INSTRUMENTS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (274201, N'QUARTER AND SHOP EQUIPMENT - NAVIGATION INSTRUMENTS - SEXTANT, ETC.')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (274202, N'QUARTER AND SHOP EQUIPMENT - NAVIGATION INSTRUMENTS - CHRONOMETER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (274300, N'QUARTER AND SHOP EQUIPMENT - INCLINOMETERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (274600, N'QUARTER AND SHOP EQUIPMENT - MISCELLANEOUS UTILITY EQUIPMENT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (274800, N'QUARTER AND SHOP EQUIPMENT - VACUUM CLEANERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (274900, N'QUARTER AND SHOP EQUIPMENT - FLOOR POLISHERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (275000, N'QUARTER AND SHOP EQUIPMENT - PORTABLE BLOWERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (275100, N'QUARTER AND SHOP EQUIPMENT - GALLEY EQUIPMENT MISC.')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (275200, N'QUARTER AND SHOP EQUIPMENT - STOVES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (275400, N'QUARTER AND SHOP EQUIPMENT - DEEP FRYERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (275500, N'QUARTER AND SHOP EQUIPMENT - GRIDDLE AND GRILLS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (275600, N'QUARTER AND SHOP EQUIPMENT - OVENS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (275601, N'QUARTER AND SHOP EQUIPMENT - OVENS - MICROWAVE OVENS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (275700, N'QUARTER AND SHOP EQUIPMENT - FOOD WARMERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (275800, N'QUARTER AND SHOP EQUIPMENT - FOOD MIXERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (275900, N'QUARTER AND SHOP EQUIPMENT - COFFEE URNS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (276000, N'QUARTER AND SHOP EQUIPMENT - ICE MACHINE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (276100, N'QUARTER AND SHOP EQUIPMENT - ICE CREAM MACHINE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (276200, N'QUARTER AND SHOP EQUIPMENT - MILK DISPENSER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (276300, N'QUARTER AND SHOP EQUIPMENT - STEAM TABLES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (276400, N'QUARTER AND SHOP EQUIPMENT - PORTABLE EQUIPMENT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (276500, N'QUARTER AND SHOP EQUIPMENT - SOFT DRINK DISPENSERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (276600, N'QUARTER AND SHOP EQUIPMENT - FREEZER / REFRIGERATORS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (276601, N'QUARTER AND SHOP EQUIPMENT - FREEZER / REFRIGERATORS - FREEZERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (276602, N'QUARTER AND SHOP EQUIPMENT - FREEZER / REFRIGERATORS - REFRIGERATORS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (276603, N'QUARTER AND SHOP EQUIPMENT - FREEZER / REFRIGERATORS - PORTABLE REFIGERATOR / BOX')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (276700, N'QUARTER AND SHOP EQUIPMENT - POTATO PEELER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (276800, N'QUARTER AND SHOP EQUIPMENT - DISHWASHER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (276900, N'QUARTER AND SHOP EQUIPMENT - IMMERSION HEATERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (277000, N'QUARTER AND SHOP EQUIPMENT - WASTE DISPOSAL')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (277001, N'QUARTER AND SHOP EQUIPMENT - WASTE DISPOSAL - TRASH COMPACTOR UNIT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (277002, N'QUARTER AND SHOP EQUIPMENT - WASTE DISPOSAL - GARBAGE DISPOSAL')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (277003, N'QUARTER AND SHOP EQUIPMENT - WASTE DISPOSAL - INCINERATOR UNIT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (277100, N'QUARTER AND SHOP EQUIPMENT - MACHINE SHOP / ENGINE ROOM EQUIPMENT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (277200, N'QUARTER AND SHOP EQUIPMENT - HOSE CRIMPING UNIT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (277300, N'QUARTER AND SHOP EQUIPMENT - DRILL PRESS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (277400, N'QUARTER AND SHOP EQUIPMENT - LATHE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (277500, N'QUARTER AND SHOP EQUIPMENT - GRINDERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (277600, N'QUARTER AND SHOP EQUIPMENT - WELDING MACHINES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (277700, N'QUARTER AND SHOP EQUIPMENT - PIPE THREADER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (277800, N'QUARTER AND SHOP EQUIPMENT - MILLING MACHINE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (277900, N'QUARTER AND SHOP EQUIPMENT - DIESEL ENGINE TEST EQUIPMENT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (278000, N'QUARTER AND SHOP EQUIPMENT - VALVE GRINDER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (278100, N'QUARTER AND SHOP EQUIPMENT - HYDRAULIC JACKS (PORTABLE)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (278200, N'QUARTER AND SHOP EQUIPMENT - OXYGEN-ACETYLENE EQUIPMENT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (278300, N'QUARTER AND SHOP EQUIPMENT - AIR TOOLS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (278400, N'QUARTER AND SHOP EQUIPMENT - ELECTRIC TOOLS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (278500, N'QUARTER AND SHOP EQUIPMENT - ROD OVEN')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (278600, N'QUARTER AND SHOP EQUIPMENT - POWER HACKSAWS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (278700, N'QUARTER AND SHOP EQUIPMENT - MISCELLANEOUS SHOP')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (278800, N'QUARTER AND SHOP EQUIPMENT - TEST EQUIPMENT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (278801, N'QUARTER AND SHOP EQUIPMENT - TEST EQUIPMENT - OSCILLOSCOPE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (278802, N'QUARTER AND SHOP EQUIPMENT - TEST EQUIPMENT - METERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (278803, N'QUARTER AND SHOP EQUIPMENT - TEST EQUIPMENT - ELECTRICAL TEST PANEL')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (278804, N'QUARTER AND SHOP EQUIPMENT - TEST EQUIPMENT - SONOSCOPE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (279400, N'QUARTER AND SHOP EQUIPMENT - MAINTENANCE EQUIPMENT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (279500, N'QUARTER AND SHOP EQUIPMENT - MAINTENANCE TOOLS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (279501, N'QUARTER AND SHOP EQUIPMENT - MAINTENANCE TOOLS - PAINT EQUIPMENT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (279502, N'QUARTER AND SHOP EQUIPMENT - MAINTENANCE TOOLS - STEAM CLEANERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (279503, N'QUARTER AND SHOP EQUIPMENT - MAINTENANCE TOOLS - SAND BLASTERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (279504, N'QUARTER AND SHOP EQUIPMENT - MAINTENANCE TOOLS - HULL CLEANING TOOLS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (279505, N'QUARTER AND SHOP EQUIPMENT - MAINTENANCE TOOLS - CHIPPING HAMMERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (279506, N'QUARTER AND SHOP EQUIPMENT - MAINTENANCE TOOLS - WATER BLASTER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (279600, N'QUARTER AND SHOP EQUIPMENT - HYDRAULIC FLUSHING UNIT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (279700, N'QUARTER AND SHOP EQUIPMENT - BAGGING MACHINE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (280000, N'MATERIAL HANDLING EQUIPMENT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (280100, N'MATERIAL HANDLING EQUIPMENT - CRANES MISCELLANEOUS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (280200, N'MATERIAL HANDLING EQUIPMENT - CRANE (ELEC / ELEC-HYD / DIESEL)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (282100, N'MATERIAL HANDLING EQUIPMENT - AIR HOIST MISCELLANEOUS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (282200, N'MATERIAL HANDLING EQUIPMENT - AIR HOIST (TUGGERS)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (283000, N'MATERIAL HANDLING EQUIPMENT - MAN RIDING TUGGERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (283500, N'MATERIAL HANDLING EQUIPMENT - FORK LIFTS / PALLET LIFTS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (283700, N'MATERIAL HANDLING EQUIPMENT - MECHANICAL WINCHES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (285100, N'MATERIAL HANDLING EQUIPMENT - BRIDGE CRANES, TROLLEYS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (285200, N'MATERIAL HANDLING EQUIPMENT - CHAIN FALLS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (285300, N'MATERIAL HANDLING EQUIPMENT - UNDERDECK BRIDGE CRANES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (285301, N'MATERIAL HANDLING EQUIPMENT - UNDERDECK BRIDGE CRANES - RISER HANDLING BRIDGE CRANE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (285302, N'MATERIAL HANDLING EQUIPMENT - UNDERDECK BRIDGE CRANES - LMRP / SUBSEA TREE AREA BRIDGE CRANE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (285400, N'MATERIAL HANDLING EQUIPMENT - ELEVATORS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (285401, N'MATERIAL HANDLING EQUIPMENT - ELEVATORS - PASSENGER ELEVATORS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (285402, N'MATERIAL HANDLING EQUIPMENT - ELEVATORS - MATERIAL HANDLING ELEVATORS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (285500, N'MATERIAL HANDLING EQUIPMENT - OVERHEAD TROLLEYS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (285600, N'MATERIAL HANDLING EQUIPMENT - EQUIPMENT CONVEYOR SYSTEMS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (285601, N'MATERIAL HANDLING EQUIPMENT - EQUIPMENT CONVEYOR SYSTEMS - MARINE RISER SKATE / CART')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (285700, N'MATERIAL HANDLING EQUIPMENT - PERSONNEL TRANSFER NET')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (285800, N'MATERIAL HANDLING EQUIPMENT - CAR CONTAINERS (BOX, NET, ETC)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (286100, N'MATERIAL HANDLING EQUIPMENT - MISCELLANEOUS TRANSFER HOSES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (286200, N'MATERIAL HANDLING EQUIPMENT - MUD-CEMENT HOSES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (286400, N'MATERIAL HANDLING EQUIPMENT - OIL-GAS HOSES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (286500, N'MATERIAL HANDLING EQUIPMENT - WATER HOSES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (287100, N'MATERIAL HANDLING EQUIPMENT - ENVIRONMENTAL PROTECTIVE EQUIPMENT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (287101, N'MATERIAL HANDLING EQUIPMENT - ENVIRONMENTAL PROTECTIVE EQUIPMENT - OIL BOOM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (288000, N'MATERIAL HANDLING EQUIPMENT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (290000, N'LIFE / RESCUE / FIRE FIGHTING')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (290100, N'LIFE / RESCUE / FIRE FIGHTING - MISCELLANEOUS L/R/F')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (290300, N'LIFE / RESCUE / FIRE FIGHTING - LIFE BOATS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (290500, N'LIFE / RESCUE / FIRE FIGHTING - LIFE BOAT DAVIT ASSEMBLY')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (290700, N'LIFE / RESCUE / FIRE FIGHTING - LIFE BOAT DAVIT WINCH')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (290800, N'LIFE / RESCUE / FIRE FIGHTING - LIFE CAPSULES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (290900, N'LIFE / RESCUE / FIRE FIGHTING - LIFE CAPSULE DAVIT ASSEMBLY')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (291000, N'LIFE / RESCUE / FIRE FIGHTING - LIFE CAPSULE DAVIT WINCH')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (291200, N'LIFE / RESCUE / FIRE FIGHTING - LIFE RAFTS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (291201, N'LIFE / RESCUE / FIRE FIGHTING - LIFE RAFTS - LIFERAFT DAVIT ASSEMBLY')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (291300, N'LIFE / RESCUE / FIRE FIGHTING - RESCUE BOATS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (291400, N'LIFE / RESCUE / FIRE FIGHTING - RESCUE BOAT DAVIT ASSEMBLY')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (291500, N'LIFE / RESCUE / FIRE FIGHTING - RESCUE BOAT DAVIT WINCH')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (291700, N'LIFE / RESCUE / FIRE FIGHTING - MISCELLANEOUS RESCUE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (291701, N'LIFE / RESCUE / FIRE FIGHTING - MISCELLANEOUS RESCUE - BREECHES BUOY')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (291702, N'LIFE / RESCUE / FIRE FIGHTING - MISCELLANEOUS RESCUE - FIREMAN OUTFIT AND EQUIP')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (291703, N'LIFE / RESCUE / FIRE FIGHTING - MISCELLANEOUS RESCUE - JACOBS LADDERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (291704, N'LIFE / RESCUE / FIRE FIGHTING - MISCELLANEOUS RESCUE - LINE THROWING APPLIANCES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (291705, N'LIFE / RESCUE / FIRE FIGHTING - MISCELLANEOUS RESCUE - PILOTS LADDERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (291706, N'LIFE / RESCUE / FIRE FIGHTING - MISCELLANEOUS RESCUE - PYROTECHNICS / LOCKER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (291707, N'LIFE / RESCUE / FIRE FIGHTING - MISCELLANEOUS RESCUE - STOKES, LITTERS, STRETCHERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (291708, N'LIFE / RESCUE / FIRE FIGHTING - MISCELLANEOUS RESCUE - DAYLIGHT SIGNAL LAMP')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (291709, N'LIFE / RESCUE / FIRE FIGHTING - MISCELLANEOUS RESCUE - OTHER REGULATORY ITEMS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (291800, N'LIFE / RESCUE / FIRE FIGHTING - OXY-BREATHING APPARATUS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (292000, N'LIFE / RESCUE / FIRE FIGHTING - MISCELLANEOUS SAFETY EQUIPMENT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (292001, N'LIFE / RESCUE / FIRE FIGHTING - MISCELLANEOUS SAFETY EQUIPMENT - LIFE LINES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (292002, N'LIFE / RESCUE / FIRE FIGHTING - MISCELLANEOUS SAFETY EQUIPMENT - LIFE PRESERVERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (292003, N'LIFE / RESCUE / FIRE FIGHTING - MISCELLANEOUS SAFETY EQUIPMENT - LIFE PRESERVER BOXES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (292004, N'LIFE / RESCUE / FIRE FIGHTING - MISCELLANEOUS SAFETY EQUIPMENT - LIFE PRESERVER RACKS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (292005, N'LIFE / RESCUE / FIRE FIGHTING - MISCELLANEOUS SAFETY EQUIPMENT - RING BUOYS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (292006, N'LIFE / RESCUE / FIRE FIGHTING - MISCELLANEOUS SAFETY EQUIPMENT - RING BUOY RACKS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (292007, N'LIFE / RESCUE / FIRE FIGHTING - MISCELLANEOUS SAFETY EQUIPMENT - WATER LIGHTS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (292008, N'LIFE / RESCUE / FIRE FIGHTING - MISCELLANEOUS SAFETY EQUIPMENT - WHISTLES AND LANYARD')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (292009, N'LIFE / RESCUE / FIRE FIGHTING - MISCELLANEOUS SAFETY EQUIPMENT - OTHER RELATED ITEMS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (292100, N'LIFE / RESCUE / FIRE FIGHTING - HELICOPTER RESCUE EQUIPMENT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (292200, N'LIFE / RESCUE / FIRE FIGHTING - IMMERSION SUITS (SURVIVAL, EXPOSURE)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (293400, N'LIFE / RESCUE / FIRE FIGHTING - EXPLOSIVE GAS DETECTION SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (293401, N'LIFE / RESCUE / FIRE FIGHTING - EXPLOSIVE GAS DETECTION SYSTEM - PORTABLE EQUIPMENT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (293402, N'LIFE / RESCUE / FIRE FIGHTING - EXPLOSIVE GAS DETECTION SYSTEM - FIXED SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (293500, N'LIFE / RESCUE / FIRE FIGHTING - EXPLOSIVE STORAGE LOCKERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (293600, N'LIFE / RESCUE / FIRE FIGHTING - H2S DETECTION')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (293601, N'LIFE / RESCUE / FIRE FIGHTING - H2S DETECTION - PORTABLE EQUIPMENT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (293602, N'LIFE / RESCUE / FIRE FIGHTING - H2S DETECTION - FIXED SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (293700, N'LIFE / RESCUE / FIRE FIGHTING - H2S PROTECTION EQUIPMENT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (293701, N'LIFE / RESCUE / FIRE FIGHTING - H2S PROTECTION EQUIPMENT - PORTABLE BREATHING EQUIPMENT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (293702, N'LIFE / RESCUE / FIRE FIGHTING - H2S PROTECTION EQUIPMENT - FIXED BREATHING SYSTEMS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (293703, N'LIFE / RESCUE / FIRE FIGHTING - H2S PROTECTION EQUIPMENT - BREATHING AIR COMPRESSORS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (295100, N'LIFE / RESCUE / FIRE FIGHTING - FIRE HOSES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (295101, N'LIFE / RESCUE / FIRE FIGHTING - FIRE HOSES - FIRE HOSE RACKS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (295102, N'LIFE / RESCUE / FIRE FIGHTING - FIRE HOSES - FIRE CARTS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (295103, N'LIFE / RESCUE / FIRE FIGHTING - FIRE HOSES - FIRE HOSE NOZZLES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (295104, N'LIFE / RESCUE / FIRE FIGHTING - FIRE HOSES - VALVES - FIRE HOSES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (295200, N'LIFE / RESCUE / FIRE FIGHTING - FIRE FIGHTING EQUIPMENT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (295300, N'LIFE / RESCUE / FIRE FIGHTING - FIRE DETECTION SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (295500, N'LIFE / RESCUE / FIRE FIGHTING - FIRE EXTINGUISHERS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (295600, N'LIFE / RESCUE / FIRE FIGHTING - DELUGE SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (295601, N'LIFE / RESCUE / FIRE FIGHTING - DELUGE SYSTEM - DELUGE PUMP')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (295602, N'LIFE / RESCUE / FIRE FIGHTING - DELUGE SYSTEM - PIPING AND VALVES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (295603, N'LIFE / RESCUE / FIRE FIGHTING - DELUGE SYSTEM - CONTROL PANEL')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (295700, N'LIFE / RESCUE / FIRE FIGHTING - SPRINKLER SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (295701, N'LIFE / RESCUE / FIRE FIGHTING - SPRINKLER SYSTEM - SPRINKLER PUMP / MOTOR')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (295702, N'LIFE / RESCUE / FIRE FIGHTING - SPRINKLER SYSTEM - SPRINKLER PIPES / VALVES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (295703, N'LIFE / RESCUE / FIRE FIGHTING - SPRINKLER SYSTEM - SPRINKLER CONTROL PANEL')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (296100, N'LIFE / RESCUE / FIRE FIGHTING - FOAM SYSTEM MISC.')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (296200, N'LIFE / RESCUE / FIRE FIGHTING - FOAM GENERATOR SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (296300, N'LIFE / RESCUE / FIRE FIGHTING - CHEMICALS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (296400, N'LIFE / RESCUE / FIRE FIGHTING - HELICOPTER FIRE CONTROL SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (297100, N'LIFE / RESCUE / FIRE FIGHTING - DRY CHEMICAL MISC.')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (297200, N'LIFE / RESCUE / FIRE FIGHTING - DRY CHEMICAL SYSTEM')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (297300, N'LIFE / RESCUE / FIRE FIGHTING - CHEMICALS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (298100, N'LIFE / RESCUE / FIRE FIGHTING - FIXED SYSTEM / CO2 / HALON')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (300000, N'EXPENDABLES (CONSUMABLES)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (300100, N'EXPENDABLES (CONSUMABLES) - FUEL - RIG')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (300102, N'EXPENDABLES (CONSUMABLES) - FUEL - RIG - FUEL - CREW BOAT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (300104, N'EXPENDABLES (CONSUMABLES) - FUEL - RIG - FUEL - SUPPLY / WORK BOAT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (300106, N'EXPENDABLES (CONSUMABLES) - FUEL - RIG - FUEL - OTHER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (300200, N'EXPENDABLES (CONSUMABLES) - DRILLWATER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (300202, N'EXPENDABLES (CONSUMABLES) - DRILLWATER - POTABLE WATER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (300300, N'EXPENDABLES (CONSUMABLES) - LUBRICANTS AND LUBE EQUIP')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (300301, N'EXPENDABLES (CONSUMABLES) - LUBRICANTS AND LUBE EQUIP - ENGINE OILS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (300302, N'EXPENDABLES (CONSUMABLES) - LUBRICANTS AND LUBE EQUIP - OTHER LUBES / GREASES / THREAD COMPOUND')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (300303, N'EXPENDABLES (CONSUMABLES) - LUBRICANTS AND LUBE EQUIP - MISCELLANEOUS LUBE EQUIPMENT')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (300400, N'EXPENDABLES (CONSUMABLES) - INSTRUMENTS, GAUGES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (300500, N'EXPENDABLES (CONSUMABLES) - BOLTS - NUTS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (300700, N'EXPENDABLES (CONSUMABLES) - FERROUS MATERIAL')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (300800, N'EXPENDABLES (CONSUMABLES) - NON-FERROUS MATERIAL')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (301000, N'EXPENDABLES (CONSUMABLES) - FILTER ELEMENTS (OIL, FUEL, ETC.)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (301200, N'EXPENDABLES (CONSUMABLES) - BEARINGS (UNIVERSAL)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (301400, N'EXPENDABLES (CONSUMABLES) - DRIVE BELTS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (301600, N'EXPENDABLES (CONSUMABLES) - LUMBER')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (301800, N'EXPENDABLES (CONSUMABLES) - HAND TOOLS (NON POWER)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (302000, N'EXPENDABLES (CONSUMABLES) - HOSE AND FITTINGS - HOSE AND FITTINGS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (302001, N'EXPENDABLES (CONSUMABLES) - HOSE AND FITTINGS - LOW PRESSURE HOSE FITTING')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (302002, N'EXPENDABLES (CONSUMABLES) - HOSE AND FITTINGS - HIGH PRESSURE HOSE FITTING')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (302003, N'EXPENDABLES (CONSUMABLES) - HOSE AND FITTINGS - HYDRAULIC HOSE FITTING')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (302200, N'EXPENDABLES (CONSUMABLES) - PIPE AND PIPE FITTINGS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (302201, N'EXPENDABLES (CONSUMABLES) - PIPE AND PIPE FITTINGS - STANDARD PIPE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (302202, N'EXPENDABLES (CONSUMABLES) - PIPE AND PIPE FITTINGS - HIGH PRESSURE PIPE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (302203, N'EXPENDABLES (CONSUMABLES) - PIPE AND PIPE FITTINGS - XXH PRESSURE PIPE')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (302400, N'EXPENDABLES (CONSUMABLES) - MISCELLANEOUS DRILLING SUPPLIES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (302600, N'EXPENDABLES (CONSUMABLES) - ELECTRICAL / ELECTRONIC SUPPLIES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (302800, N'EXPENDABLES (CONSUMABLES) - MISCELLANEOUS VESSEL SUPPLIES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (303000, N'EXPENDABLES (CONSUMABLES) - RIGGING / DECK SUPPLIES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (303200, N'EXPENDABLES (CONSUMABLES) - ROPE (WIRE, MANILA, SYNTHETIC)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (303400, N'EXPENDABLES (CONSUMABLES) - ENGINE ROOM SUPPLIES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (303600, N'EXPENDABLES (CONSUMABLES) - VALVES (UNIVERSAL)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (303601, N'EXPENDABLES (CONSUMABLES) - VALVES (UNIVERSAL) - STANDARD VALVES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (303602, N'EXPENDABLES (CONSUMABLES) - VALVES (UNIVERSAL) - HIGH PRESSURE VALVES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (303603, N'EXPENDABLES (CONSUMABLES) - VALVES (UNIVERSAL) - XXH PRESSURE VALVES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (303800, N'EXPENDABLES (CONSUMABLES) - MEDICAL SUPPLIES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (304000, N'EXPENDABLES (CONSUMABLES) - COMMISSARY SUPPLIES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (304400, N'EXPENDABLES (CONSUMABLES) - OFFICE SUPPLIES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (304500, N'EXPENDABLES (CONSUMABLES) - NAVIGATION SUPPLIES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (304600, N'EXPENDABLES (CONSUMABLES) - WORK CLOTHING')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (304800, N'EXPENDABLES (CONSUMABLES) - CHEMICALS')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (305000, N'EXPENDABLES (CONSUMABLES) - WELDING SUPPLIES')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (305200, N'EXPENDABLES (CONSUMABLES) - PAINT SUPPLIES (NON PAINT)')

INSERT [dbo].[IndexCodesStage] ([IndexCode], [Description]) VALUES (999999, N'IMS CONVERSION')
end