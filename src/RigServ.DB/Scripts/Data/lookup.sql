﻿delete from kiosk.Lookup where LookupType = 'EMAIL_TEST_GROUP'
Go


INSERT INTO  kiosk.LOOKUP(LookupType,LookupValue,LookupDescription,LookupSort,Status,CreatedAt,UpdatedAt,Deleted) VALUES ('EMAIL_TEST_GROUP','aadebayo@eminenttechnology.com','Adebayo Dayo','1','1',GETDATE(),GETDATE(),0)
Go

delete from kiosk.Lookup where LookupType = 'MATERIALS_COORDINATOR_EMAIL'
Go


INSERT INTO  kiosk.LOOKUP(LookupType,LookupValue,LookupDescription,LookupSort,Status,CreatedAt,UpdatedAt,Deleted) VALUES ('MATERIALS_COORDINATOR_EMAIL','MaterialsCoordinator.DPN@deepwater.com','Deepwater Pontus','1','1',GETDATE(),GETDATE(),0)
INSERT INTO  kiosk.LOOKUP(LookupType,LookupValue,LookupDescription,LookupSort,Status,CreatedAt,UpdatedAt,Deleted) VALUES ('MATERIALS_COORDINATOR_EMAIL','MaterialsCoordinator.DPS@deepwater.com','Deepwater Poseidon','1','1',GETDATE(),GETDATE(),0)
INSERT INTO  kiosk.LOOKUP(LookupType,LookupValue,LookupDescription,LookupSort,Status,CreatedAt,UpdatedAt,Deleted) VALUES ('MATERIALS_COORDINATOR_EMAIL','MaterialsCoordinator.DTH@deepwater.com','Deepwater Thalassa','1','1',GETDATE(),GETDATE(),0)
INSERT INTO  kiosk.LOOKUP(LookupType,LookupValue,LookupDescription,LookupSort,Status,CreatedAt,UpdatedAt,Deleted) VALUES ('MATERIALS_COORDINATOR_EMAIL','MaterialsSup.DVS@deepwater.com','Deepwater Invictus','1','1',GETDATE(),GETDATE(),0)
INSERT INTO  kiosk.LOOKUP(LookupType,LookupValue,LookupDescription,LookupSort,Status,CreatedAt,UpdatedAt,Deleted) VALUES ('MATERIALS_COORDINATOR_EMAIL','materialscoordinator.dpt@deepwater.com','Deepwater Proteus','1','1',GETDATE(),GETDATE(),0)
Go