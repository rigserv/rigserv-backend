﻿DELETE kiosk.MailTemplate where TemplateCode='ISSUE_NOTIFICATION';
go

INSERT INTO kiosk.MailTemplate (TemplateCode, TemplateName, TemplateSubject, 	TemplateBody, [Priority], IsBodyHTML, CopySender, [Status], CreatedOn, 
	CreatedBy, ModifiedOn, ModifiedBy) 
VALUES ('ISSUE_NOTIFICATION','Daily Issue Report Notification','PARTSERV - Daily Issue Report Notification ',
'
 <div id="page" style="padding: 10px;">
    <div id="content" style="padding: 20px; font-family: Calibri, Myriad Pro, Arial, Sans Serif; font-size: 16px;">
            <br />

            The following are the items issued for facility {FACILITY_NAME} on {DATE}:
			 <br />
			 <br />
			{ITEMS}
            <br />
            <br />
           

           Thanks
            <br />
           
    
        </div>
		<br/>
	</div>
',
'High',1,0,1,GETDATE(),'SYSTEM',GETDATE(),'SYSTEM')
go
