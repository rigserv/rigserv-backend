﻿CREATE FUNCTION [kiosk].[GetProductFacilitiesByCategoryId]
(
  @CategoryId varchar(50)
)
RETURNS @Output TABLE (
	  Id int IDENTITY(1,1),
      FacilityId NVARCHAR(50)
)
AS
BEGIN
     INSERT INTO @Output
	 SELECT DISTINCT FacilityId FROM kiosk.ProductCategory WHERE Path like '%/'+@CategoryId+'/%'

      RETURN
END
GO
