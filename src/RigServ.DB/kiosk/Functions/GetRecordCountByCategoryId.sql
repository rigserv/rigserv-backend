﻿CREATE FUNCTION [kiosk].[GetRecordCountByCategoryId]
(
	@FacilityId varchar(50),
	@CategoryId varchar(50)
)
RETURNS int
AS
BEGIN

	DECLARE @count int
	;With RET As
	(
	
		SELECT c.Id From kiosk.CategoryStage c
		WHERE c.ParentId = @CategoryId
		UNION ALL
		SELECT  t.Id 
		From kiosk.CategoryStage t 
		INNER JOIN
		ret r on t.ParentId = r.Id
	
	)

	select @count = count(1) from
	(
	select ProductId From RET a 
	inner join kiosk.ProductCategory p on (a.Id = p.CategoryId )
	where p.FacilityId = @FacilityID 
	Union ALL
	select ProductId FROM kiosk.ProductCategory p 
	where p.FacilityId = @FacilityID and p.CategoryId = @CategoryId 
	) x
	RETURN @count

END

GO
