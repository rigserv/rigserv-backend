﻿CREATE PROCEDURE [kiosk].[ProductPhoto_LoadData]
	AS
	SET NOCOUNT ON
/* Declare local variables */
DECLARE
	@error int


/* Beginning of procedure */

--Delete all data
UPDATE kiosk.ProductPhoto SET Deleted = 'true', UpdatedAt= GETDATE()
			    FROM kiosk.ProductPhoto ph
				INNER JOIN kiosk.Product p ON p.Id =ph.ProductId
				LEFT OUTER JOIN 
				ItemImages i ON p.RIN = i.Name
				WHERE i.Name IS NULL 

	UPDATE kiosk.ProductPhoto SET
	ImageUrl = i.Url,
	IsDefault = i.IsDefault
    FROM kiosk.ProductPhoto ph
	INNER JOIN kiosk.Product p ON p.Id =ph.ProductId AND p.FacilityId = ph.FacilityId
	INNER JOIN ItemImages i ON i.Name = p.RIN and ph.ItemImagesId = i.Id
	AND ph.IsDefault = i.IsDefault
	WHERE 
	ph.ImageUrl <> i.url OR
	ph.IsDefault <> i.IsDefault



	INSERT INTO kiosk.ProductPhoto
			   (Id, ProductId,ImageUrl,IsDefault,FacilityId,CreatedAt, UpdatedAt,Deleted, ItemImagesId)

	 SELECT CONVERT(varchar(50), NEWID()) Id, p.Id, i.Url, i.IsDefault, p.FacilityId,
			GETDATE() CreatedAt,GETDATE() UpdatedAt,0 Deleted, i.Id
	FROM kiosk.Product p
	INNER JOIN ItemImages i ON i.Name = p.RIN
	LEFT OUTER JOIN kiosk.ProductPhoto ph on ph.ProductId = p.Id 
	where ph.ProductId  is null and p.DefaultImageUrl is not null

		
/* error-handling */
SELECT @error = @@error 
IF (@error <> 0) GOTO ERROR


RETURN 0

/* error-handling */
ERROR:
	RETURN @error

/* End of procedure*/
