﻿CREATE PROCEDURE [kiosk].[Facility_LoadData]
AS
	SET NOCOUNT ON
/* Declare local variables */
DECLARE
	@error int, @rowcount int, @counter int=1,
	@TLAName nvarchar(max), @FacilityDescription nvarchar(max),
	@FacilityDescription_old nvarchar(max)

declare @temp table
(
id int identity(1,1),
TLAName nvarchar(20),
FacilityDescription nvarchar(max)
)

/* Beginning of procedure */

--Delete all data
UPDATE kiosk.Facility SET Deleted = 'true', UpdatedAt= GETDATE()
			    FROM kiosk.Facility f
				LEFT OUTER JOIN  Inventory i ON f.Id = i.TLAName
				WHERE i.TLAName IS NULL



INSERT INTO @temp
	SELECT  Distinct  i.TLAName, i.FacilityDescription FROM Inventory i WHERE ISNULL(i.TLAName,'') <> '' AND ISNULL(i.FacilityDescription,'') <>  ''
	SET @rowcount=@@ROWCOUNT

		WHILE(@counter <= @rowcount)
		BEGIN
			SELECT @TLAName= TLAName, @FacilityDescription = FacilityDescription  FROM @temp WHERE id=@counter
			
			SET @counter=@counter+ 1

			--Insert if indexcode not in Category table
			INSERT INTO kiosk.Facility (Id,Description,CreatedAt,UpdatedAt,Deleted)
			SELECT  @TLAName, @FacilityDescription, GETDATE(), GETDATE(), 'false'
			WHERE @TLAName not in (SELECT Id FROM Facility)

		    --Update if FacilityDescription changes
			SELECT @FacilityDescription_old = Description  FROM kiosk.Facility WHERE Id=@TLAName

			IF(rtrim(ltrim(@FacilityDescription_old)) <> rtrim(ltrim(@FacilityDescription)))
			BEGIN
			UPDATE kiosk.Facility SET Description = @FacilityDescription, UpdatedAt= GETDATE(), Deleted = 'false' WHERE Id= @TLAName
			END

		END	

/* error-handling */
SELECT @error = @@error 
IF (@error <> 0) GOTO ERROR


RETURN 0

/* error-handling */
ERROR:
	RETURN @error

/* End of procedure*/
