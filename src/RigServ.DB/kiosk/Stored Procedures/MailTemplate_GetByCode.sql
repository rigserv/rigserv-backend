﻿CREATE PROCEDURE [kiosk].[MailTemplate_GetByCode]
(
	@TemplateCode	NVARCHAR(100)
)
AS
SET NOCOUNT ON
/* Declare local variables */
DECLARE
	@error int

/* Beginning of procedure */



	SELECT
		T.TemplateCode,
		T.TemplateName,
		T.TemplateSubject,
		T.TemplateBody,
		T.Priority,
		T.IsBodyHTML,
		T.CopySender,
		T.Status,
		T.CreatedOn,
		T.CreatedBy,
		T.ModifiedOn,
		T.ModifiedBy
	FROM kiosk.MailTemplate T
	WHERE
		T.TemplateCode = @TemplateCode;


/* error-handling */
SELECT @error = @@error 
IF (@error <> 0) GOTO ERROR


RETURN 0

/* error-handling */
ERROR:
	RETURN @error

/* End of procedure*/