﻿CREATE PROCEDURE [kiosk].[InventoryLedger_Save]
AS
	SET NOCOUNT ON
/* Declare local variables */
DECLARE
	@error int

/* Beginning of procedure */

Delete FROM kiosk.InventoryLedgerStage

INSERT INTO kiosk.InventoryLedgerStage
SELECT NEWID(), i.Id, i.FacilityId, i.Quantity, s.Qty FROM kiosk.RigInventory i
				INNER JOIN Inventory s ON s.Class = i.Class AND i.FacilityId = s.TLAName
				INNER JOIN kiosk.Product p on p.ICN = s.ICN and i.ProductId = p.Id 
				INNER JOIN kiosk.Location l on l.Name = s.Location and l.Id = i.LocationId 
				WHERE  s.DefaultIndexCode IS NOT NULL AND  s.Qty > 0


INSERT INTO kiosk.InventoryLedger
			   (Id,FacilityId,Quantity,InventoryId,EntityType,EntityID,CreatedAt,UpdatedAt,Deleted)
	SELECT NEWID(), i.FacilityId, i.InventoryStageQuantity - i.InventoryQuantity,  i.InventoryId, 'RigInventory', i.InventoryId,
	GETDATE(),GETDATE(),'false'
	FROM kiosk.InventoryLedgerStage i	WHERE EXISTS (SELECT InventoryId FROM kiosk.InventoryLedger il  WHERE i.InventoryId = il.InventoryId)
	


	--Newly Added inventory record
	INSERT INTO kiosk.InventoryLedger
			   (Id,FacilityId,Quantity,InventoryId,EntityType,EntityID,CreatedAt,UpdatedAt,Deleted)

	SELECT NEWID(), i.FacilityId, i.InventoryQuantity, i.InventoryId,'RigInventory',i.InventoryId,GETDATE(),GETDATE(),'false'
	FROM kiosk.InventoryLedgerStage i
	LEFT OUTER JOIN kiosk.InventoryLedger il ON i.InventoryId = il.InventoryId AND i.FacilityId = il.FacilityId
	WHERE il.InventoryId IS NULL AND il.FacilityId IS NULL

/* error-handling */
SELECT @error = @@error 
IF (@error <> 0) GOTO ERROR


RETURN 0

/* error-handling */
ERROR:
	RETURN @error

/* End of procedure*/
