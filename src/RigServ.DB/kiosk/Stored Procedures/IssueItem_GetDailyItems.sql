﻿CREATE PROCEDURE [kiosk].[IssueItem_GetDailyItems]
@FacilityId varchar(50)
AS
/* Declare local variables */
DECLARE
	@error int, 
	@IssueItems nvarchar(max)='<table width="700"><tr><th>Id</th><th>Product Name</th><th>Quantity</th><th>IssuedTo</th></tr>',
	@IssueItemId varchar(50),
	@ProductName  varchar(50),
	@Quantity int,
	@IssuedTo varchar(50),
	@Id int,
	@Count int,
	@Counter int=1

DECLARE @temp TABLE
	(
	  ID int identity(1,1),
	  IssueItemId varchar(50),
	  ProductName  varchar(50),
	  Quantity int,
	  IssuedTo varchar(50)
	)
	INSERT INTO @temp
	SELECT ii.Id, p.Name, ii.Quantity, ISNULL(i.Lastname,'') + ' ' + ISNULL(i.Firstname,'') IssueTo 
	From kiosk.IssueItem ii
	INNER JOIN kiosk.Issue i ON i.Id = ii.IssueId
	INNER JOIN kiosk.Product p ON p.Id = ii.ProductId
	WHERE ii.FacilityId = @FacilityId
	AND Convert(date,ii.CreatedAt) = Convert(date,GETDATE())

	SELECT @Count = Count(*) FROM @temp

	WHILE @Count >= @Counter
	BEGIN
		SELECT  @Id = ID, @IssueItemId = IssueItemId, @ProductName= ProductName,  @Quantity= Quantity , @IssuedTo = IssuedTo FROM @temp WHERE Id=@Counter
		Set @IssueItems= @IssueItems + '<tr><td align="left">'+ @IssueItemId  + '</td><td align="left">'+ @ProductName  + '</td><td align="center">'+ convert(varchar(10), @Quantity)  + '</td><td align="left">'+ @IssuedTo  + '</td></tr>'
		SET @Counter = @Counter + 1
	END
	Set @IssueItems = @IssueItems + '</table>'

	SELECT Description,  @IssueItems IssueItems, @Count ItemCount From kiosk.Facility WHERE Id = @FacilityId

RETURN 0 
