﻿CREATE PROCEDURE [kiosk].[CategoryStage_LoadData]
	AS
	SET NOCOUNT ON
/* Declare local variables */
DECLARE
	@error int, @rowcount int, @counter int=1, @splitcount int, @categoryname nvarchar(300),
	@parentid nvarchar(50), @parentdesc nvarchar(500), @indexcode nvarchar(50), @description nvarchar(500),
	@categoryname_old nvarchar(500), @nonresetcounter int=1, @imageurl nvarchar(300), @imageurl_old nvarchar(300),
	@facilityid  varchar(50), @facilityid_old  varchar(50)
	

declare @temp table
(
id int identity(1,1),
indexcode nvarchar(20),
description nvarchar(max),
facilityid varchar(50),
ImageUrl nvarchar(max)
)

/* BEGINning of procedure */

--Delete all data
UPDATE kiosk.CategoryStage SET Deleted = 'true', UpdatedAt= GETDATE()
FROM kiosk.CategoryStage c
LEFT OUTER JOIN Inventory i on i.defaultindexcode = c.Id
WHERE i.Defaultindexcode IS NULL

-- Insert and Update records
	INSERT INTO @temp
	SELECT Distinct defaultindexcode, REPLACE(IndexDesc,'–','-'), TLAName, '' Url FROM Inventory 
	WHERE  ISNULL(Defaultindexcode,'') <>  '' AND ISNULL(TLAName,'') <>  ''

	SET @rowcount=@@ROWCOUNT

		WHILE(@counter <= @rowcount)
		BEGIN

			SELECT @indexcode= indexcode, @description =description, @facilityid= facilityId, @imageurl=ImageUrl
		   FROM @temp WHERE id=@counter
			SELECT @splitcount = count(*) FROM split(REPLACE(@description,'–','-'),'-')

				IF(@splitcount = 1)
				BEGIN
					SET  @parentid = null
					SET @categoryname = @description
				END
				ELSE IF(@splitcount = 2)
				BEGIN
					SET @parentdesc=ltrim(rtrim((SELECT item FROM split(@description,'-') WHERE id=@splitcount-1)))
					SET @parentid= (SELECT TOP 1 indexcode FROM @temp WHERE ltrim(rtrim(description))=@parentdesc)
					SET @categoryname = ltrim(rtrim((SELECT item FROM split(@description,'-') WHERE id=@splitcount)))
				END
				ELSE IF(@splitcount = 3)
				BEGIN
					SET @parentdesc=ltrim(rtrim((SELECT item FROM split(@description,'-') WHERE id=@splitcount-2))) + ' - ' +
									ltrim(rtrim((SELECT item FROM split(@description,'-') WHERE id=@splitcount-1)))
					SET @parentid= (SELECT TOP 1 indexcode FROM @temp WHERE ltrim(rtrim(description))=@parentdesc)
					SET @categoryname = ltrim(rtrim((SELECT item FROM split(@description,'-') WHERE id=@splitcount)))

					IF(@parentid IS NULL)
					BEGIN
					SET @parentdesc=ltrim(rtrim((SELECT item FROM split(@description,'-') WHERE id=@splitcount-2))) 
					SET @parentid= (SELECT TOP 1 indexcode FROM @temp WHERE ltrim(rtrim(description))=@parentdesc)
					SET @categoryname = ltrim(rtrim((SELECT item FROM split(@description,'-') WHERE id=@splitcount-1))) + ' - ' +
										ltrim(rtrim((SELECT item FROM split(@description,'-') WHERE id=@splitcount)))
					END
				END
				 ELSE IF(@splitcount = 4)
				 BEGIN
					SET @parentdesc= ltrim(rtrim((SELECT item FROM split(@description,'-') WHERE id=@splitcount-3))) + ' - ' +
									 ltrim(rtrim((SELECT item FROM split(@description,'-') WHERE id=@splitcount-2))) + ' - ' +
									 ltrim(rtrim((SELECT item FROM split(@description,'-') WHERE id=@splitcount-1)))
					SET @parentid= (SELECT TOP 1 indexcode FROM @temp WHERE ltrim(rtrim(description))=@parentdesc)
					SET @categoryname = ltrim(rtrim((SELECT item FROM split(@description,'-') WHERE id=@splitcount)))

					IF (@parentid IS NULL)
					BEGIN
					SET @parentdesc=ltrim(rtrim((SELECT item FROM split(@description,'-') WHERE id=@splitcount-3))) + ' - ' +
									ltrim(rtrim((SELECT item FROM split(@description,'-') WHERE id=@splitcount-2)))
					SET @parentid= (SELECT TOP 1 indexcode FROM @temp WHERE ltrim(rtrim(description))=@parentdesc)

					SET @categoryname = ltrim(rtrim((SELECT item FROM split(@description,'-') WHERE id=@splitcount-1))) + ' - ' +
										ltrim(rtrim((SELECT item FROM split(@description,'-') WHERE id=@splitcount)))

					END
					IF(@parentid IS NULL)
					BEGIN
					SET @parentdesc=ltrim(rtrim((SELECT item FROM split(@description,'-') WHERE id=@splitcount-3))) 
					SET @parentid= (SELECT TOP 1 indexcode FROM @temp WHERE ltrim(rtrim(description))=@parentdesc)
					
					SET @categoryname = ltrim(rtrim((SELECT item FROM split(@description,'-') WHERE id=@splitcount-2))) + ' - ' +
										ltrim(rtrim((SELECT item FROM split(@description,'-') WHERE id=@splitcount-1))) + ' - ' +
										ltrim(rtrim((SELECT item FROM split(@description,'-') WHERE id=@splitcount)))
					END
				END
				 ELSE IF(@splitcount = 5)
				 BEGIN
					SET @parentdesc=ltrim(rtrim((SELECT item FROM split(@description,'-') WHERE id=@splitcount-4))) + ' - ' +
								 ltrim(rtrim((SELECT item FROM split(@description,'-') WHERE id=@splitcount-3))) + ' - ' +
								 ltrim(rtrim((SELECT item FROM split(@description,'-') WHERE id=@splitcount-2))) + ' - ' +
								 ltrim(rtrim((SELECT item FROM split(@description,'-') WHERE id=@splitcount-1)))
					SET @parentid= (SELECT TOP 1 indexcode FROM @temp WHERE ltrim(rtrim(description))=@parentdesc)
				
					SET @categoryname = ltrim(rtrim((SELECT item FROM split(@description,'-') WHERE id=@splitcount)))

					IF (@parentid IS NULL)
					BEGIN
					SET @parentdesc=ltrim(rtrim((SELECT item FROM split(@description,'-') WHERE id=@splitcount-4))) + ' - ' +
									ltrim(rtrim((SELECT item FROM split(@description,'-') WHERE id=@splitcount-3))) + ' - ' +
									ltrim(rtrim((SELECT item FROM split(@description,'-') WHERE id=@splitcount-2)))
					SET @parentid= (SELECT TOP 1 indexcode FROM @temp WHERE ltrim(rtrim(description))=@parentdesc)

					SET @categoryname = ltrim(rtrim((SELECT item FROM split(@description,'-') WHERE id=@splitcount-1))) + ' - ' +
										ltrim(rtrim((SELECT item FROM split(@description,'-') WHERE id=@splitcount)))
					END
					IF (@parentid IS NULL)
					BEGIN
					SET @parentdesc=ltrim(rtrim((SELECT item FROM split(@description,'-') WHERE id=@splitcount-4))) + ' - ' +
									ltrim(rtrim((SELECT item FROM split(@description,'-') WHERE id=@splitcount-3)))
					SET @parentid= (SELECT TOP 1 indexcode FROM @temp WHERE ltrim(rtrim(description))=@parentdesc)

					SET @categoryname = ltrim(rtrim((SELECT item FROM split(@description,'-') WHERE id=@splitcount-2))) + ' - ' +
										ltrim(rtrim((SELECT item FROM split(@description,'-') WHERE id=@splitcount-1))) + ' - ' +
										ltrim(rtrim((SELECT item FROM split(@description,'-') WHERE id=@splitcount)))

					END
					IF(@parentid IS NULL)
					BEGIN
					SET @parentdesc=ltrim(rtrim((SELECT item FROM split(@description,'-') WHERE id=@splitcount-4))) 
					SET @parentid= (SELECT TOP 1 indexcode FROM @temp WHERE ltrim(rtrim(description))=@parentdesc)

					SET @categoryname = ltrim(rtrim((SELECT item FROM split(@description,'-') WHERE id=@splitcount-3))) + ' - ' +
										ltrim(rtrim((SELECT item FROM split(@description,'-') WHERE id=@splitcount-2))) + ' - ' +
										ltrim(rtrim((SELECT item FROM split(@description,'-') WHERE id=@splitcount-1))) + ' - ' +
										ltrim(rtrim((SELECT item FROM split(@description,'-') WHERE id=@splitcount)))
					END
				 END
			
			SET @counter=@counter+ 1
 
			--Insert if indexcode not in CategoryStage table
			
					INSERT INTO kiosk.CategoryStage(Id, Name, ImageUrl, ParentId, FacilityId, CreatedAt, UpdatedAt, Deleted)
					SELECT  @indexcode, @categoryname, @imageurl, @parentid, @facilityid, GETDATE(), GETDATE(), 'false'
					WHERE @indexcode not in (SELECT Id FROM kiosk.CategoryStage) AND @facilityid not in (SELECT FacilityId FROM kiosk.CategoryStage)
			
			

		    --Update if description changes
			SELECT @categoryname_old = Name, @imageurl_old=ImageUrl, @facilityid_old = ISNULL(FacilityId,'')  
			FROM kiosk.CategoryStage WHERE Id=@indexcode AND FacilityId = @facilityid

			IF(rtrim(ltrim(isnull(@categoryname_old,''))) <> rtrim(ltrim(@categoryname)) OR
			   rtrim(ltrim(isnull(@imageurl_old,''))) <> rtrim(ltrim(@imageurl)) OR 
			   rtrim(ltrim(isnull(@facilityid_old,''))) <> rtrim(ltrim(isnull(@facilityid,''))))
			BEGIN
			UPDATE kiosk.CategoryStage SET Name = @categoryname, ImageUrl=@imageurl, FacilityId=@facilityid,
			UpdatedAt= GETDATE(), Deleted = 'false' WHERE Id= @indexcode
			END

			

		END

/* error-handling */
SELECT @error = @@error 
IF (@error <> 0) GOTO ERROR


RETURN 0

/* error-handling */
ERROR:
	RETURN @error

/* END of procedure*/