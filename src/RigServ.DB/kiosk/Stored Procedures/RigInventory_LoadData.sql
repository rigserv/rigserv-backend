﻿CREATE PROCEDURE [kiosk].[RigInventory_LoadData]
AS
	SET NOCOUNT ON
/* Declare local variables */
DECLARE
	@error int


/* Beginning of procedure */
--RigInventory Type
--O(old), N(new)

--Delete all data
UPDATE kiosk.RigInventory SET Deleted = 'true', UpdatedAt= GETDATE()
			    FROM kiosk.RigInventory iv
				INNER JOIN  kiosk.Location l ON l.id = iv.LocationId 
				INNER JOIN  kiosk.Product p ON p.Id = iv.ProductId
				LEFT OUTER JOIN Inventory i ON (iv.Class = i.Class AND iv.FacilityId = i.TLAName 
													AND p.ICN = i.ICN AND l.Name = i.Location)
				WHERE i.Class IS NULL OR i.TLAName IS NULL OR i.ICN IS NULL OR i.Location IS NULL AND i.Qty > 0


DELETE FROM kiosk.Inventory_Temp

INSERT INTO kiosk.Inventory_Temp(InventoryId,LocationId,ProductId,FacilityId,Class,InventoryQuantity,
						   InventoryStageQuantity,Type)
--Old Data
SELECT 
		i.Id InventoryId, i.LocationId, i.ProductId, i.FacilityId, i.Class, i.Quantity InventoryQuantity,
		s.Qty InventoryStageQuantity, 'O' Type FROM RigInventory i
				INNER JOIN Inventory s ON s.Class = i.Class AND i.FacilityId = s.TLAName
				INNER JOIN kiosk.Product p on p.ICN = s.ICN and i.ProductId = p.Id 
				INNER JOIN kiosk.Location l on l.Name = s.Location and l.Id = i.LocationId 
				WHERE  s.DefaultIndexCode IS NOT NULL AND  s.Qty <> i.Quantity AND  s.Qty > 0

UNION ALL

--NewData
 SELECT 
		CONVERT(varchar(50), NEWID()) InventoryId, l.Id LocationId,  pd.id ProductId,
					i.TLAName FacilityId, i.Class, i.Qty InventoryQuantity, i.Qty InventoryStageQuantity, 'N' Type
				FROM Inventory i
				INNER JOIN kiosk.Product pd on pd.CatalogId = i.CatalogId and pd.FacilityId = i.TLAName AND pd.ICN = i.ICN
				INNER JOIN kiosk.Location l on l.Name = i.Location 
				and l.FacilityId = i.TLAName
				WHERE
				 NOT EXISTS(SELECT 'x' FROM kiosk.RigInventory WHERE kiosk.RigInventory.FacilityId = i.TLAName AND 
				kiosk.RigInventory.Class = i.Class AND kiosk.RigInventory.LocationId = l.Id AND kiosk.RigInventory.ProductId = pd.Id 
				)
				AND 
				 i.DefaultIndexCode IS NOT NULL AND i.Qty > 0

				 --Insert both new and modified inventory record in inventory_temp
				INSERT INTO kiosk.InventoryLedger
			   (Id,FacilityId,Quantity,InventoryId,EntityType,EntityID,CreatedAt,UpdatedAt,Deleted)
			    SELECT NEWID(), i.FacilityId, i.InventoryQuantity, i.InventoryId,'RigInventory',i.InventoryId,GETDATE(),GETDATE(),'false'
				FROM kiosk.Inventory_Temp i 

				
				--Insert new record into inventory
				INSERT INTO kiosk.RigInventory
					   (Id, LocationId, ProductId, FacilityId, Class, Quantity, CreatedAt, UpdatedAt, Deleted)
				SELECT  i.InventoryId, i.LocationId,  i.ProductId,i.FacilityId, i.Class, i.InventoryQuantity, 
						GETDATE() CreatedAt,GETDATE() UpdatedAt,'false' Deleted 
				FROM kiosk.Inventory_Temp i WHERE Type='N'

				Update kiosk.RigInventory SET Quantity=t.InventoryStageQuantity
				FROM kiosk.RigInventory i
				JOIN kiosk.Inventory_Temp t ON i.Id=t.InventoryId
				WHERE Type='O'