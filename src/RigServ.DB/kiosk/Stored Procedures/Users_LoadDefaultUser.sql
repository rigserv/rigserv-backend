﻿CREATE PROCEDURE [kiosk].[Users_LoadDefaultUser]
AS

DECLARE
	@error int

	INSERT INTO kiosk.Users(Id, FacilityId, Username, [Password], CreatedAt, UpdatedAt, Deleted)
	SELECT CONVERT(VARCHAR(50),NEWID()), f.Id, 'User_'+ f.Id ,'Password1',  GETDATE(), GETDATE(), 'false'
	FROM kiosk.Facility f

/* error-handling */
SELECT @error = @@error 
IF (@error <> 0) GOTO ERROR


RETURN 0

/* error-handling */
ERROR:
	RETURN @error

/* End of procedure*/