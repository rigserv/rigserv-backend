﻿CREATE PROCEDURE [kiosk].[UpdateFirstRecord]
	AS
	SET NOCOUNT ON
/* Declare local variables */
DECLARE @error int
DECLARE @Id varchar(50)
Declare @firstId varchar(50)

DECLARE _cursor CURSOR FOR
SELECT Id FROM kiosk.FACILITY

Open _cursor
FETCH NEXT FROM _cursor
INTO @Id
WHILE @@FETCH_STATUS = 0
BEGIN

    

    SELECT @firstId = Id
    FROM
    (
        SELECT Top 1 Id FROM kiosk.Category WHERE FacilityId = @Id
    ) x

    
    
    Print  @Id + ' = ' + @firstId

    

    UPDATE kiosk.Category
        SET UpdatedAt = GETUTCDate()
    WHERE Id = @firstId

    
    SELECT @firstId = Id
    FROM
    (
        SELECT Top 1 Id FROM kiosk.ProductCategory WHERE FacilityId = @Id
    ) x

    
    
    Print  @Id + ' = ' + @firstId

    

    UPDATE kiosk.ProductCategory
        SET UpdatedAt = GETUTCDate()
    WHERE Id = @firstId


    SELECT @firstId = Id
    FROM
    (
        SELECT Top 1 Id FROM kiosk.Product WHERE FacilityId = @Id
    ) x

    
    
    Print  @Id + ' = ' + @firstId

    

    UPDATE kiosk.Product
        SET UpdatedAt = GETUTCDate()
    WHERE Id = @firstId

    SELECT @firstId = Id
    FROM
    (
        SELECT Top 1 Id FROM kiosk.Location WHERE FacilityId = @Id
    ) x

    
    
    Print  @Id + ' = ' + @firstId

    

    UPDATE kiosk.Location
        SET UpdatedAt = GETUTCDate()
    WHERE Id = @firstId


	   SELECT @firstId = Id
    FROM
    (
        SELECT Top 1 Id FROM kiosk.RigInventory WHERE FacilityId = @Id
    ) x

    
    
    Print  @Id + ' = ' + @firstId

    

    UPDATE kiosk.RigInventory
        SET UpdatedAt = GETUTCDate()
    WHERE Id = @firstId


FETCH NEXT FROM _cursor
INTO @Id

END
CLOSE _cursor
DEALLOCATE _cursor

/* error-handling */
SELECT @error = @@error 
IF (@error <> 0) GOTO ERROR


RETURN 0

/* error-handling */
ERROR:
	RETURN @error

/* END of procedure*/
