﻿CREATE PROCEDURE [kiosk].[Location_LoadData]
AS
	SET NOCOUNT ON
/* Declare local variables */
DECLARE
	@error int

/* Beginning of procedure */

--Delete all data
	UPDATE kiosk.Location SET Deleted = 'true', UpdatedAt= GETDATE()
			    FROM kiosk.Location l
				LEFT OUTER JOIN  Inventory i ON l.Name = i.Location
				WHERE i.Location IS NULL

	
	UPDATE kiosk.Location SET 
	IsDefaultReturnLocation = i.IsDefaultLocation,
	UpdatedAt= GETDATE(),
	Deleted = 'false'
	FROM Inventory AS i
	INNER JOIN kiosk.Location l ON i.Location = l.Name AND i.TLAName = l.FacilityId
	WHERE l.IsDefaultReturnLocation <> i.IsDefaultLocation
	
	--WHERE clause to be added when the appropriate location for the IsDefaultReturnLocation and Email is determined
	


	INSERT INTO kiosk.Location
			   (Id, Name, FacilityId,IsDefaultReturnLocation,Email,CreatedAt,UpdatedAt,Deleted)

	SELECT NEWID(), i.Location, i.TLAName, i.IsDefaultLocation,
					'' Email, -- No email field yet
					GETDATE(),GETDATE(),'false'
	FROM Inventory i
	LEFT OUTER JOIN kiosk.Location l ON l.Name = i.location AND i.TLAName = l.FacilityId
	WHERE l.Name IS NULL AND l.FacilityId IS NULL AND  ISNULL(i.TLAName,'') <> '' 
	GROUP BY	 i.Location, i.TLAName, i.IsDefaultLocation


	

/* error-handling */
SELECT @error = @@error 
IF (@error <> 0) GOTO ERROR


RETURN 0

/* error-handling */
ERROR:
	RETURN @error

/* End of procedure*/
