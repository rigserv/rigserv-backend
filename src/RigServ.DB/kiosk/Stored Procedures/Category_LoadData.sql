﻿CREATE PROCEDURE [kiosk].[Category_LoadData]
	AS
	SET NOCOUNT ON
/* Declare local variables */
DECLARE
	@error int, @rowcount int, @counter int=1, @splitcount int, @desc nvarchar(max),
	@parentid nvarchar(20), @parentdesc nvarchar(max), @indexcode nvarchar(max), @description nvarchar(max),
	@description_old nvarchar(max), @nonresetcounter int=1, @ImageUrl nvarchar(500)


/* BEGINning of procedure */

DELETE FROM kiosk.Category_Temp

INSERT INTO kiosk.Category_Temp
select distinct c.FacilityId,c.Id, c.Name, c.ImageUrl, c.ParentId  from kiosk.CategoryStage c
where kiosk.GetRecordCountByCategoryId(c.FacilityId, c.Id)  > 0

--Delete all data
UPDATE kiosk.Category SET Deleted = 'true', UpdatedAt= GETDATE()
FROM kiosk.Category c
LEFT OUTER JOIN kiosk.Category_Temp i on i.CategoryId = c.IndexCode AND i.FacilityId = c.FacilityId
WHERE i.CategoryId IS NULL and i.FacilityId is null

--Update existing records
UPDATE kiosk.Category SET UpdatedAt=GETDATE(),
					Name= ct.Name,
					ImageUrl = ct.ImageUrl
					FROM kiosk.Category c
					INNER JOIN kiosk.Category_Temp ct ON C.IndexCode = ct.CategoryId and ct.FacilityId = c.FacilityId
					WHERE c.Name <> ct.Name OR ISNULL(c.ImageUrl,'') <> ISNULL(ct.ImageUrl,'')

--Insert new records
INSERT INTO kiosk.Category(Id, IndexCode, Name, ImageUrl, ParentId, FacilityId, CreatedAt, UpdatedAt, Deleted)
SELECT CONVERT(varchar(50), NEWID()), ct.CategoryId, ct.Name, ct.ImageUrl, ct.ParentId, ct.FacilityId,
	   GETDATE(), GETDATE(), 'false'
FROM kiosk.Category_Temp ct 
LEFT OUTER JOIN  kiosk.Category c ON c.FacilityId = ct.FacilityId AND c.IndexCode = ct.CategoryId
WHERE c.FacilityId IS NULL AND c.IndexCode IS NULL

	


/* error-handling */
SELECT @error = @@error 
IF (@error <> 0) GOTO ERROR


RETURN 0

/* error-handling */
ERROR:
	RETURN @error

/* END of procedure*/