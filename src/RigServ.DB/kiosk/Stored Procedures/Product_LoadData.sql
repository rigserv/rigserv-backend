﻿CREATE PROCEDURE [kiosk].[Product_LoadData]
	AS
	SET NOCOUNT ON
/* Declare local variables */
DECLARE
	@error int, @rowcount int, @counter int=1, @facilityId varchar(50)

	
declare @temp table
(
id int identity(1,1),
facilityId varchar(50)
)


/* Beginning of procedure */

--Delete all data
UPDATE kiosk.Product SET Deleted = 'true', UpdatedAt= GETDATE()
			    FROM kiosk.Product p
				LEFT OUTER JOIN 
				Inventory i ON p.CatalogId = i.CatalogId
				WHERE i.CatalogId IS NULL 
	
	UPDATE kiosk.Product SET 
	Name = i.CatalogDescription,
	RIN= i.RIN,
	PartNumber = i.PartNumber,
	Manufacturer = i.Manufacturer,
	ManufacturerCode = i.ManufacturerCode,
	ICN = i.ICN,
	UOM = i.ICNUOM,
	DefaultImageUrl = m.Url,
	UpdatedAt= GETDATE(),
	Deleted = 'false'
	FROM Inventory AS i
	INNER JOIN kiosk.Product p ON p.CatalogId = i.CatalogId
	LEFT OUTER JOIN ItemImages m ON m.Name = p.RIN AND m.IsDefault = 1 
	WHERE
	p.Name <> i.CatalogDescription OR
	p.RIN <> i.RIN OR
	p.PartNumber <> i.PartNumber OR
	p.Manufacturer <> i.Manufacturer OR
	p.ManufacturerCode <> i.ManufacturerCode OR
	p.ICN <> i.ICN OR
	p.UOM <> i.ICNUOM OR
	ISNULL(p.DefaultImageUrl,'') <> m.Url


	INSERT INTO kiosk.Product
			   (Id, Name,FacilityId,RIN,PartNumber,Manufacturer,
			   ManufacturerCode,ICN,DefaultImageUrl, CreatedAt, UpdatedAt,Deleted, CatalogId, UOM)

	
	 SELECT CONVERT(varchar(50), NEWID()) Id, i.CatalogDescription Name,i.TLAName FacilityId,
			i.RIN, i.PartNumber, i.Manufacturer, i.ManufacturerCode,i.ICN, m.Url DefaultImageUrl, GETDATE() CreatedAt,GETDATE() UpdatedAt,0
			Deleted, i.CatalogId, i.ICNUOM
	FROM Inventory as i
	LEFT OUTER JOIN ItemImages m on m.Name = i.RIN AND m.IsDefault=1 AND m.IsDeleted=0
	LEFT OUTER JOIN kiosk.Product p on p.CatalogId = i.CatalogId 
	where p.CatalogId is null and i.Qty > 0 AND  ISNULL(i.TLAName,'') <> '' 
		 
	 

	
		
/* error-handling */
SELECT @error = @@error 
IF (@error <> 0) GOTO ERROR


RETURN 0

/* error-handling */
ERROR:
	RETURN @error

/* End of procedure*/