﻿CREATE PROCEDURE [kiosk].[Lookup_GetItem]
	(
	@LookupType nvarchar(100),
	@LookupValue nvarchar(500)

)
AS
SET NOCOUNT ON
/* Declare local variables */
DECLARE
	@error int

/* Beginning of procedure */


SELECT

	l.LookupType,
	l.LookupValue,
	l.LookupDescription,
	l.LookupSort,
	l.Status,
	l.CreatedAt,
	l.UpdatedAt
FROM 
	Lookup l
WHERE
	
	l.LookupType = @LookupType AND
	l.LookupValue = @LookupValue


/* error-handling */
SELECT @error = @@error 
IF (@error <> 0) GOTO ERROR


RETURN 0

/* error-handling */
ERROR:
	RETURN @error

/* End of procedure*/
