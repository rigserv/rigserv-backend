﻿CREATE PROCEDURE [kiosk].[Lookup_GetActiveItemsByType]
(
	@LookupType nvarchar(100)
)
AS
SET NOCOUNT ON
/* Declare local variables */
DECLARE
	@error int

/* Beginning of procedure */


SELECT

	l.LookupType,
	l.LookupValue,
	l.LookupDescription,
	l.LookupSort,
	l.Status,
	l1.LookupDescription StatusName,
	l.CreatedAt,
	l.UpdatedAt
FROM 
	kiosk.Lookup l
	LEFT OUTER JOIN kiosk.Lookup l1
		on l1.LookupValue = l.Status AND l1.LookupType = 'DEFAULT_STATUS'
WHERE
	l.LookupType = @LookupType AND
		l.Status = 1
ORDER BY
	l.LookupSort

/* error-handling */
SELECT @error = @@error 
IF (@error <> 0) GOTO ERROR


RETURN 0

/* error-handling */
ERROR:
	RETURN @error

/* End of procedure*/