﻿CREATE PROCEDURE [kiosk].[ReturnItem_GetDailyItems]
@FacilityId varchar(50)

AS
/* Declare local variables */
DECLARE
	@error int, 
	@ReturnItems nvarchar(max)='<table width="700"><tr><th>Id</th><th>Product Name</th><th>Quantity</th><th>Location</th></tr>',
	@ReturnItemId varchar(50),
	@ProductName  varchar(50),
	@Quantity int,
	@Location varchar(50),
	@Id int,
	@Count int,
	@Counter int=1


DECLARE @temp TABLE
	(
	  ID int identity(1,1),
	  ReturnItemId varchar(50),
	  ProductName  varchar(50),
	  Quantity int,
	  Location varchar(50)
	)
	INSERT INTO @temp
	SELECT r.Id, p.Name, r.Quantity, l.Name
	From kiosk.ReturnItem r
	INNER JOIN kiosk.IssueItem ii ON ii.Id = r.IssueItemId
	INNER JOIN kiosk.Product p ON p.Id = ii.ProductId
	INNER JOIN kiosk.Location l ON l.Id = r.LocationId
	WHERE r.FacilityId = @FacilityId
	AND Convert(date,r.CreatedAt) = Convert(date,GETDATE())

	SELECT @Count = Count(*) FROM @temp

	WHILE @Count >= @Counter
	BEGIN
		SELECT @Id = ID, @ReturnItemId = ReturnItemId, @ProductName= ProductName,  @Quantity= Quantity, @Location = Location FROM @temp  WHERE Id=@Counter
		Set @ReturnItems= @ReturnItems + '<tr><td align="left">'+ @ReturnItemId  + '</td><td align="left">'+ @ProductName  + '</td><td align="center">'+ convert(varchar(10), @Quantity)  + '</td><td align="left">'+ @Location  + '</td></tr>'
		SET @Counter = @Counter + 1
	END
	Set @ReturnItems = @ReturnItems + '</table>'
	SELECT Description, @ReturnItems ReturnItems, @Count ItemCount From kiosk.Facility WHERE Id = @FacilityId

RETURN 0 

