﻿CREATE PROCEDURE [kiosk].[ProductCategory_LoadData]
	AS
	SET NOCOUNT ON
/* Declare local variables */
DECLARE
	@error int

	
declare @rowcount int, @counter int=1, @splitcount int,
	@parentid nvarchar(20), @parentdesc nvarchar(max), @indexcode nvarchar(max), @description nvarchar(max)

	declare @temp table
(
id int identity(1,1),
indexcode nvarchar(20),
description nvarchar(max)
)

declare @finaltable table(
indexcode nvarchar(20),
description nvarchar(500),
path nvarchar(max)
)

--Constructing path values
INSERT INTO @temp
	SELECT DISTINCT defaultindexcode, IndexDesc FROM Inventory WHERE  ISNULL(Defaultindexcode,'') <>  ''
	SET @rowcount=@@ROWCOUNT

			WHILE(@counter <= @rowcount)
		BEGIN
		declare @path nvarchar(max)=''
			SELECT @indexcode= indexcode, @description =description  FROM @temp WHERE id=@counter
			SELECT @splitcount = count(*) FROM kiosk.split(@description,'-')

				IF(@splitcount = 1)
				BEGIN
					SET	 @path = @path + '/' + @indexcode + '/'
					 
				END
				ELSE IF(@splitcount = 2)
				BEGIN
					SET @parentdesc=ltrim(rtrim((SELECT item FROM kiosk.split(@description,'-') WHERE id=@splitcount-1)))
					SELECT @parentid= indexcode FROM @temp WHERE ltrim(rtrim(description))=@parentdesc

					IF(@parentid IS NOT NULL)
					SET @path= @path + '/' + ISNULL(@parentid,'') +'/' + @indexcode + '/' 
					ELSE
					SET @path= @path + '/'  + @indexcode + '/' 
					
				END
				ELSE IF(@splitcount = 3)
				BEGIN
				 
				 SET @parentdesc=ltrim(rtrim((SELECT item FROM kiosk.split(@description,'-') WHERE id=@splitcount-2)))
				 SELECT @parentid= indexcode FROM @temp WHERE ltrim(rtrim(description))=@parentdesc
					IF (@parentid IS NOT NULL)
						SET @path= @path + '/' + ISNULL(@parentid,'') +'/' 

				SET @parentdesc=ltrim(rtrim((SELECT item FROM kiosk.split(@description,'-') WHERE id=@splitcount-2))) + ' - ' +
								ltrim(rtrim((SELECT item FROM kiosk.split(@description,'-') WHERE id=@splitcount-1)))
				SELECT @parentid= indexcode FROM @temp WHERE ltrim(rtrim(description))=@parentdesc
					
					IF (@parentid IS NOT NULL)					
						SET @path= @path + '/' + ISNULL(@parentid,'') +'/' + @indexcode + '/'
					ELSE
						SET @path= @path + '/' + @indexcode + '/'
				 
				END
				 ELSE IF(@splitcount = 4)
				 BEGIN

				 SET @parentdesc=ltrim(rtrim((SELECT item FROM kiosk.split(@description,'-') WHERE id=@splitcount-3)))
				 SELECT @parentid= indexcode FROM @temp WHERE ltrim(rtrim(description))=@parentdesc
				 IF (@parentid IS NOT NULL)
						SET @path= @path + '/' + ISNULL(@parentid,'') +'/' 

				SET @parentdesc=ltrim(rtrim((SELECT item FROM kiosk.split(@description,'-') WHERE id=@splitcount-3))) + ' - ' +
								 ltrim(rtrim((SELECT item FROM kiosk.split(@description,'-') WHERE id=@splitcount-2))) 
				SELECT @parentid= indexcode FROM @temp WHERE ltrim(rtrim(description))=@parentdesc

				IF (@parentid IS NOT NULL)
						SET @path= @path + '/' + ISNULL(@parentid,'') +'/' 

				 SET @parentdesc=ltrim(rtrim((SELECT item FROM kiosk.split(@description,'-') WHERE id=@splitcount-3))) + ' - ' +
								 ltrim(rtrim((SELECT item FROM kiosk.split(@description,'-') WHERE id=@splitcount-2))) + ' - ' +
								 ltrim(rtrim((SELECT item FROM kiosk.split(@description,'-') WHERE id=@splitcount-1)))
				SELECT @parentid= indexcode FROM @temp WHERE ltrim(rtrim(description))=@parentdesc

				IF (@parentid IS NOT NULL)					
						SET @path= @path + '/' + ISNULL(@parentid,'') +'/' + @indexcode + '/'
					ELSE
						SET @path= @path + '/' + @indexcode + '/'
				
				 END
				 ELSE IF(@splitcount = 5)
				 BEGIN
				 SET @parentdesc=ltrim(rtrim((SELECT item FROM kiosk.split(@description,'-') WHERE id=@splitcount-4)))
				SELECT @parentid= indexcode FROM @temp WHERE ltrim(rtrim(description))=@parentdesc
				  IF (@parentid IS NOT NULL)
						SET @path= @path + '/' + ISNULL(@parentid,'') +'/' 


				SET @parentdesc=ltrim(rtrim((SELECT item FROM kiosk.split(@description,'-') WHERE id=@splitcount-4))) + ' - ' +
								 ltrim(rtrim((SELECT item FROM kiosk.split(@description,'-') WHERE id=@splitcount-3))) 
				SELECT @parentid= indexcode FROM @temp WHERE ltrim(rtrim(description))=@parentdesc
				 
				 IF (@parentid IS NOT NULL)
						SET @path= @path + '/' + ISNULL(@parentid,'') +'/' 


				SET @parentdesc=ltrim(rtrim((SELECT item FROM kiosk.split(@description,'-') WHERE id=@splitcount-4))) + ' - ' +
								 ltrim(rtrim((SELECT item FROM kiosk.split(@description,'-') WHERE id=@splitcount-3))) + ' - ' +
								 ltrim(rtrim((SELECT item FROM kiosk.split(@description,'-') WHERE id=@splitcount-2))) 
				SELECT @parentid= indexcode FROM @temp WHERE ltrim(rtrim(description))=@parentdesc
				  IF (@parentid IS NOT NULL)
						SET @path= @path + '/' + ISNULL(@parentid,'') +'/' 


				 SET @parentdesc=ltrim(rtrim((SELECT item FROM kiosk.split(@description,'-') WHERE id=@splitcount-4))) + ' - ' +
								 ltrim(rtrim((SELECT item FROM kiosk.split(@description,'-') WHERE id=@splitcount-3))) + ' - ' +
								 ltrim(rtrim((SELECT item FROM kiosk.split(@description,'-') WHERE id=@splitcount-2))) + ' - ' +
								 ltrim(rtrim((SELECT item FROM kiosk.split(@description,'-') WHERE id=@splitcount-1)))
				SELECT @parentid= indexcode FROM @temp WHERE ltrim(rtrim(description))=@parentdesc
				IF (@parentid IS NOT NULL)					
						SET @path= @path + '/' + ISNULL(@parentid,'') +'/' + @indexcode + '/'
					ELSE
						SET @path= @path + '/' + @indexcode + '/'

				 
				 END
			
			SET @counter=@counter+ 1
			insert into @finaltable values(@indexcode, @description, @path)
		   end

/* Beginning of procedure */
--Delete all data
UPDATE kiosk.ProductCategory SET Deleted = 'true', UpdatedAt= GETDATE()
			    FROM kiosk.ProductCategory pc
				LEFT OUTER JOIN 
				kiosk.CategoryStage c ON pc.CategoryId = c.Id
				WHERE c.Id IS NULL 


--Update existing data
	UPDATE kiosk.ProductCategory
	SET CategoryName = cs.Name,
		ProductName = p.Name,
		DefaultCategoryImageUrl = cs.ImageUrl,
		DefaultProductImageUrl = p.DefaultImageUrl,
		Path = f.path
	FROM kiosk.ProductCategory pc
	INNER JOIN kiosk.Product p ON p.Id = pc.ProductId
	INNER JOIN kiosk.CategoryStage cs on cs.Id = pc.CategoryId
	INNER JOIN @finaltable f on f.indexcode = pc.CategoryId
    WHERE pc.ProductName <> p.Name or pc.CategoryName <> cs.Name 
		  OR ISNULL(pc.DefaultProductImageUrl,'') <> ISNULL(p.DefaultImageUrl,'')
		  OR ISNULL(pc.DefaultCategoryImageUrl,'') <> ISNULL(cs.ImageUrl,'')
		  OR ISNULL(pc.Path,'') <> ISNULL(f.path,'')


	INSERT INTO kiosk.ProductCategory
			   (Id, CategoryId, ProductId, FacilityId, CategoryName, ProductName,
			    DefaultCategoryImageUrl, DefaultProductImageUrl, [Path],
			    CreatedAt, UpdatedAt,Deleted)

	SELECT	CONVERT(varchar(50), NEWID()) Id, i.DefaultIndexCode CategoryId,  p.Id ProductId,
		    p.FacilityId, c.Name, p.Name, c.ImageUrl, p.DefaultImageUrl, f.path,
			GETDATE() CreatedAt,GETDATE() UpdatedAt,'false' Deleted 
	FROM Inventory i
	INNER JOIN kiosk.CategoryStage c on c.Id = i.DefaultIndexCode
	INNER JOIN @finaltable f on f.indexcode = c.id
	INNER JOIN kiosk.Product p ON p.CatalogId = i.CatalogId
	LEFT OUTER JOIN kiosk.ProductCategory pc ON (pc.CategoryId =i.DefaultIndexCode AND pc.ProductId = p.Id)
	WHERE (pc.CategoryId IS NULL OR pc.ProductId IS NULL) AND
		  (i.DefaultIndexCode IS NOT NULL  AND i.DefaultIndexCode <> 0)
	GROUP BY i.catalogid, i.DefaultIndexcode, p.Id, p.FacilityId, c.Name, p.Name, c.ImageUrl,
	         p.DefaultImageUrl, f.path


/* error-handling */
SELECT @error = @@error 
IF (@error <> 0) GOTO ERROR


RETURN 0

/* error-handling */
ERROR:
	RETURN @error

/* End of procedure*/