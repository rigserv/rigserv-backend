﻿CREATE TABLE [kiosk].[RigInventory]
(
	[Id] varchar(50) NOT NULL PRIMARY KEY,
	[LocationId] varchar(50) NOT NULL,
	[ProductId] varchar(50) NOT NULL,
	[FacilityId] varchar(50) NOT NULL,
	[Class] nvarchar(25) NOT NULL,
	[Quantity] int NOT NULL,
	[Version] TIMESTAMP NOT NULL,
	[CreatedAt] DATETIMEOFFSET NOT NULL DEFAULT (sysutcdatetime()),
	[UpdatedAt] DATETIMEOFFSET NULL,
    [Deleted]  bit NOT NULL,
	CONSTRAINT [FK_RigInventory_Location] FOREIGN KEY (LocationId) REFERENCES kiosk.Location(Id),
	CONSTRAINT [FK_RigInventory_Product]  FOREIGN KEY (ProductId) REFERENCES kiosk.Product(Id),
	CONSTRAINT [FK_RigInventory_Facility] FOREIGN KEY ([FacilityId]) REFERENCES kiosk.Facility(Id),
)
