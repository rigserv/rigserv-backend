﻿CREATE TABLE [kiosk].[Inventory_Temp]
(
	[InventoryId] varchar(50) NOT NULL PRIMARY KEY,
	[LocationId] varchar(50) NOT NULL,
	[ProductId] varchar(50) NOT NULL,
	[FacilityId] varchar(50) NOT NULL,
	[Class] nvarchar(25) NOT NULL,
	[InventoryQuantity] int NOT NULL,
	[InventoryStageQuantity] int NOT NULL,
	[Type]	nvarchar(1) NOT NULL
)
