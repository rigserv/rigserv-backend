﻿CREATE TABLE [kiosk].[ProductCategory]
(
	[Id] varchar(50) NOT NULL PRIMARY KEY,
	[CategoryId] varchar(50) NOT NULL,
	[ProductId] varchar(50) NOT NULL,
	[FacilityId] varchar(50) NOT NULL,
	[CategoryName] nvarchar(500) NOT NULL,
	[ProductName] nvarchar(max) NOT NULL,
	[DefaultCategoryImageUrl] nvarchar(500) NULL,
	[DefaultProductImageUrl] nvarchar(500) NULL, 
	[Path] NVARCHAR(MAX) NULL,
	[Version] TIMESTAMP NOT NULL,
	[CreatedAt] DATETIMEOFFSET NOT NULL DEFAULT (sysutcdatetime()),
	[UpdatedAt] DATETIMEOFFSET NULL,
    [Deleted]  bit NOT NULL,
	CONSTRAINT [FK_ProductCategory_CategoryStage] FOREIGN KEY (CategoryId) REFERENCES kiosk.CategoryStage(Id),
	CONSTRAINT [FK_ProductCategory_Product] FOREIGN KEY (ProductId) REFERENCES kiosk.Product(Id),
	CONSTRAINT [FK_[ProductCategory_Facility] FOREIGN KEY (FacilityId) REFERENCES kiosk.Facility(Id),
)
