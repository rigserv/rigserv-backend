﻿CREATE TABLE [kiosk].[Issue]
(
	[Id] varchar(50) NOT NULL PRIMARY KEY,
	[FacilityId] varchar(50) NOT NULL,
	[EmployeeId] varchar(50) NULL,
	[Firstname] nvarchar(200) NOT NULL,
	[Lastname] nvarchar(200) NOT NULL,
	[WorkOrder] nvarchar(25) NULL,
	[IssueNo] int NOT NULL,
	[Version] TIMESTAMP NOT NULL,
	[CreatedAt] DATETIMEOFFSET NOT NULL DEFAULT (sysutcdatetime()),
	[UpdatedAt] DATETIMEOFFSET NULL,
    [Deleted]  bit NOT NULL,
	CONSTRAINT [FK_Issue_Facility] FOREIGN KEY (FacilityId) REFERENCES kiosk.Facility(Id)
)
