﻿CREATE TABLE [kiosk].[ReturnItem]
(
	[Id] varchar(50) NOT NULL PRIMARY KEY,
	[FacilityId] varchar(50) NOT NULL,
	[Quantity] int NOT NULL,
	[IssueItemId] varchar(50) NOT NULL,
	[LocationId] varchar(50) NOT NULL,
	[Version] TIMESTAMP NOT NULL,
	[CreatedAt] DATETIMEOFFSET NOT NULL DEFAULT (sysutcdatetime()),
	[UpdatedAt] DATETIMEOFFSET NULL,
    [Deleted]  bit NOT NULL,
	CONSTRAINT [FK_ReturnItem_IssueItem] FOREIGN KEY ([IssueItemId]) REFERENCES kiosk.IssueItem(Id),
	CONSTRAINT [FK_ReturnItem_Facility] FOREIGN KEY (FacilityId) REFERENCES kiosk.Facility(Id)
)
