﻿CREATE TABLE [kiosk].[PickList]
(
	[Id] varchar(50) NOT NULL PRIMARY KEY,
	[FacilityId] varchar(50) NOT NULL,
	[Version] TIMESTAMP NOT NULL,
	[CreatedAt] DATETIMEOFFSET NOT NULL DEFAULT (sysutcdatetime()),
	[UpdatedAt] DATETIMEOFFSET NULL,
    [Deleted]  bit NOT NULL,
	CONSTRAINT [FK_PickList_Facility] FOREIGN KEY (FacilityId) REFERENCES kiosk.Facility(Id),
)
