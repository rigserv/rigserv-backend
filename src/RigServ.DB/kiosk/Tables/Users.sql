﻿CREATE TABLE [kiosk].[Users]
(
	[Id] varchar(50) NOT NULL PRIMARY KEY,
	[FacilityId] varchar(50) NOT NULL,
	[Username] varchar(50) NOT NULL,
	[Password] varchar(50) NOT NULL,
	[Version] TIMESTAMP NOT NULL,
	[CreatedAt] DATETIMEOFFSET NOT NULL DEFAULT (sysutcdatetime()),
	[UpdatedAt] DATETIMEOFFSET NULL,
    [Deleted]  bit NOT NULL,
	CONSTRAINT RigUsers_FacilityId FOREIGN KEY (FacilityId) REFERENCES kiosk.Facility(Id)
)
