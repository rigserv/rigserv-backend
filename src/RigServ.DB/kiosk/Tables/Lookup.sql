﻿CREATE TABLE [kiosk].[Lookup]
(
	[LookupType] nvarchar(500) NOT NULL,
	[LookupValue] nvarchar(500) NOT NULL,
	[LookupDescription] nvarchar(500) NOT NULL,
	[LookupSort] [nvarchar](100) NOT NULL,
	[Status] [int] NOT NULL CONSTRAINT [DF_Lookup_Status]  DEFAULT ((1)),
	[Version] TIMESTAMP NOT NULL,
	[CreatedAt] DATETIMEOFFSET NOT NULL DEFAULT (sysutcdatetime()),
	[UpdatedAt] DATETIMEOFFSET NULL,
    [Deleted]  bit NOT NULL,
	CONSTRAINT [PK_Lookup] PRIMARY KEY  
(
	[LookupType] ASC,
	[LookupValue] ASC
)
)
