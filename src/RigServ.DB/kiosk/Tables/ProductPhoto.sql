﻿CREATE TABLE [kiosk].[ProductPhoto]
(
	[Id] varchar(50) NOT NULL PRIMARY KEY,
	[ProductId] varchar(50) NOT NULL,
	[ImageUrl] nvarchar(500) NULL ,
	[IsDefault] bit NOT NULL,
	[FacilityId] varchar(50) NOT NULL,
	[Version] TIMESTAMP NOT NULL,
	[CreatedAt] DATETIMEOFFSET NOT NULL DEFAULT (sysutcdatetime()),
	[UpdatedAt] DATETIMEOFFSET NULL,
    [Deleted]  bit NOT NULL,
	[ItemImagesId] VARCHAR(50) NULL, 
    CONSTRAINT [FK_ProductPhoto_Product] FOREIGN KEY (ProductId) REFERENCES kiosk.Product(Id),
	CONSTRAINT [FK_ProductPhoto_Facility] FOREIGN KEY ([FacilityId]) REFERENCES kiosk.Facility(Id),
)
