﻿CREATE TABLE [kiosk].[MailTemplate]
(
	[TemplateCode] [nvarchar](100) NOT NULL,
	[TemplateName] [nvarchar](100) NOT NULL,
	[TemplateSubject] [nvarchar](600) NULL,
	[TemplateBody] [nvarchar](max) NULL,
	[Priority] [nvarchar](100) NULL,
	[IsBodyHTML] [int] NOT NULL,
	[CopySender] [int] NOT NULL,
	[Status] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](255) NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[ModifiedBy] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_MailTemplate] PRIMARY KEY 
(
	[TemplateCode] ASC
)
)
