﻿CREATE TABLE [kiosk].[InventoryLedger]
(
	[Id] varchar(50) NOT NULL PRIMARY KEY,
	[FacilityId] varchar(50) NOT NULL,
	[Quantity] int NOT NULL,
	[InventoryId] varchar(100) NOT NULL,
	[EntityType] nvarchar(25) NOT NULL,
	[EntityID] nvarchar(100) NOT NULL,
	[Version] TIMESTAMP NOT NULL,
	[CreatedAt] DATETIMEOFFSET NOT NULL DEFAULT (sysutcdatetime()),
	[UpdatedAt] DATETIMEOFFSET NULL,
    [Deleted]  bit NOT NULL,
	CONSTRAINT [FK_InventoryLedger_Facility] FOREIGN KEY (FacilityId) REFERENCES kiosk.Facility(Id)
)

