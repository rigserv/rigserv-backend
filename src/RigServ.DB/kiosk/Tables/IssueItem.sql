﻿CREATE TABLE [kiosk].[IssueItem]
(
	[Id] varchar(50) NOT NULL PRIMARY KEY,
	[Quantity] int NOT NULL,
	[InventoryId] nvarchar(50) NOT NULL,
	[IssueId] varchar(50) NOT NULL,
	[FacilityId] varchar(50) NOT NULL,
	[ProductId] varchar(50) NOT NULL ,
	[Version] TIMESTAMP NOT NULL,
	[CreatedAt] DATETIMEOFFSET NOT NULL DEFAULT (sysutcdatetime()),
	[UpdatedAt] DATETIMEOFFSET NULL,
    [Deleted]  bit NOT NULL,
	CONSTRAINT [FK_IssueItem_Issue] FOREIGN KEY (IssueId) REFERENCES kiosk.Issue(Id),
	CONSTRAINT [FK_IssueItem_Facility] FOREIGN KEY (FacilityId) REFERENCES kiosk.Facility(Id),
	CONSTRAINT [FK_IssueItem_Product] FOREIGN KEY (ProductId) REFERENCES kiosk.Product(Id),
)
