﻿CREATE TABLE [kiosk].[CategoryStage]
(
	[Id] VARCHAR(50) NOT NULL PRIMARY KEY,
	[Name] nvarchar(500) NOT NULL ,
	[ImageUrl] nvarchar(500) NULL ,
	[ParentId] nvarchar(100) NULL,
	[FacilityId] varchar(50) NULL,
	[Version] TIMESTAMP NOT NULL,
	[CreatedAt] DATETIMEOFFSET NOT NULL DEFAULT (sysutcdatetime()),
	[UpdatedAt] DATETIMEOFFSET NULL,
    [Deleted]  bit NOT NULL
)
