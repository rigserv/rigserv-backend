﻿CREATE TABLE [kiosk].[Location]
(
	[Id] varchar(50) NOT NULL PRIMARY KEY,
	[Name] nvarchar(200) NOT NULL,
	[FacilityId] varchar(50) NOT NULL,
	[IsDefaultReturnLocation] bit NOT NULL,
	[Email] nvarchar(255) NOT NULL,
	[Version] TIMESTAMP NOT NULL,
	[CreatedAt] DATETIMEOFFSET NOT NULL DEFAULT (sysutcdatetime()),
	[UpdatedAt] DATETIMEOFFSET NULL,
    [Deleted]  bit NOT NULL,
	CONSTRAINT [FK_Location_Facility] FOREIGN KEY (FacilityId) REFERENCES kiosk.Facility(Id),
)
