﻿CREATE TABLE [kiosk].[Facility]
(
	[Id] varchar(50) NOT NULL PRIMARY KEY,
	[Description] nvarchar(250) NOT NULL ,
	[EmailAddress] nvarchar(250) NULL ,
	[Version] TIMESTAMP NOT NULL,
	[CreatedAt] DATETIMEOFFSET NOT NULL DEFAULT (sysutcdatetime()),
	[UpdatedAt] DATETIMEOFFSET NULL,
    [Deleted]  bit NOT NULL,
)
