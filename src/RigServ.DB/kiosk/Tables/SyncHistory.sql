﻿CREATE TABLE [kiosk].[SyncHistory]
(
	[Id] varchar(50) NOT NULL PRIMARY KEY,
	[FacilityId] varchar(50) NOT NULL,
	[SyncDate] DateTime NOT NULL,
	[Status] int NOT NULL,
	[Description]  nvarchar(max) NOT NULL,
	[Version] TIMESTAMP NOT NULL,
	[CreatedAt] DATETIMEOFFSET NOT NULL DEFAULT (sysutcdatetime()),
	[UpdatedAt] DATETIMEOFFSET NULL,
    [Deleted]  bit NOT NULL,
	CONSTRAINT [FK_SyncHistory_Facility] FOREIGN KEY (FacilityId) REFERENCES kiosk.Facility(Id)
)
