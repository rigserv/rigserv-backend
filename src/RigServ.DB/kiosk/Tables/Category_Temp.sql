﻿CREATE TABLE [kiosk].[Category_Temp]
(
	[FacilityId] varchar(50) NOT NULL,
	[CategoryId] VARCHAR(50) NOT NULL,
	[Name] nvarchar(500) NOT NULL,
	[ImageUrl] nvarchar(500) NULL ,
	[ParentId] nvarchar(100) NULL,

	CONSTRAINT PK_CategoryId_FacilityId PRIMARY KEY (
	FacilityId,
	CategoryId
 )

)
