﻿CREATE TABLE [kiosk].[Product]
(
	[Id] varchar(50) NOT NULL PRIMARY KEY,
	[Name] nvarchar(max) NOT NULL,
	[FacilityId] varchar(50) NOT NULL,
	[RIN] nvarchar(200) NOT NULL,
	[PartNumber] nvarchar(200) NOT NULL,
	[Manufacturer] nvarchar(max) NOT NULL,
	[ManufacturerCode] nvarchar(200) NOT NULL,
	[ICN] NVARCHAR(50) NOT NULL,
	[DefaultImageUrl] nvarchar(500) NULL,
	[Version] TIMESTAMP NOT NULL,
	[CreatedAt] datetimeoffset NOT NULL,
	[UpdatedAt] datetimeoffset NOT NULL,
	[Deleted]  BIT NOT NULL,
	[CatalogId] UNIQUEIDENTIFIER NULL,
	[UOM] VARCHAR(20) NULL, 
    CONSTRAINT [FK_Product_Facility] FOREIGN KEY ([FacilityId]) REFERENCES kiosk.Facility(Id),
)
