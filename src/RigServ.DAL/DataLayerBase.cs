﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.SqlClient;

namespace RigServe.DAL
{
    public class DataLayerBase
    {
        public static DbCommand GetStoredProcCommand(string CommandText)
        {
            DbCommand command = new SqlCommand();
            command.CommandText = CommandText;
            command.CommandType = CommandType.StoredProcedure;

            return command;
        }

        public static void AddInParameter(DbCommand command, string ParameterName, DbType DBType, object value)
        {
            DbParameter p;

            p = command.CreateParameter();
            p.ParameterName = ParameterName;
            p.DbType = DBType;
            p.Value = value;

            command.Parameters.Add(p);
        }

        public static void AddTableValueParameter(Database db, DbCommand command, string ParameterName, string TypeName, DataTable value)
        {
            if (value != null)
            {

                System.Data.SqlClient.SqlParameter p = new System.Data.SqlClient.SqlParameter();

                p.ParameterName = ParameterName;
                p.SqlDbType = SqlDbType.Structured;
                p.TypeName = TypeName;
                p.Value = value;



                command.Parameters.Add(p);
            }
        }

        public static DbConnection GetConnection(Database db, string connectionString)
        {

            DbConnection retVal = null;

            if (String.IsNullOrEmpty(connectionString))
            {
                retVal = db.CreateConnection();
            }
            else
            {
                retVal = new SqlConnection(connectionString);
            }
            return retVal;
        }

        #region ExecuteDataSet
        public static DataSet ExecuteDataSet(Database db, DbTransaction tran, DbCommand cmd)
        {
            //declare variables
            DataSet ret = null;

            //check whether DBTransaction is null
            if (db != null)
            {
                if (tran == null)
                {
                    ret = db.ExecuteDataSet(cmd);
                }
                else
                {
                    ret = db.ExecuteDataSet(cmd, tran);
                }
            }
            else
            {
                SqlDataAdapter da = new SqlDataAdapter();

                if (tran == null)
                {
                    ret = new DataSet();
                    da.SelectCommand = (SqlCommand)cmd;
                    da.Fill(ret);
                }
                else
                {
                    cmd.Transaction = tran;
                    cmd.Connection = cmd.Transaction.Connection;
                    ret = new DataSet();
                    da.SelectCommand = (SqlCommand)cmd;
                    da.Fill(ret);
                }

            }

            return ret;

        }
        #endregion

        #region ExecuteDataTable
        public static DataTable ExecuteDataTable(Database db, DbTransaction tran, DbCommand cmd)
        {
            //declare variables
            DataTable ret = null;

            ret = ExecuteDataSet(db, tran, cmd).Tables[0];

            return ret;

        }
        #endregion

        #region ExecuteNonQuery
        public static int ExecuteNonQuery(Database db, DbTransaction tran, DbCommand cmd)
        {
            //declare variable
            int retVal = 0;
            try
            {

                if (db != null)
                {
                    if (tran == null)
                    {
                        retVal = db.ExecuteNonQuery(cmd);

                    }
                    else
                    {

                        retVal = db.ExecuteNonQuery(cmd, tran);
                    }
                }
                else
                {
                    if (tran == null)
                    {

                        retVal = cmd.ExecuteNonQuery();


                    }
                    else
                    {
                        cmd.Transaction = tran;
                        cmd.Connection = cmd.Transaction.Connection;
                        retVal = cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return retVal;
        }

        #endregion

        #region ExecuteXml
        public static XmlDocument ExecuteXml(Database db, DbTransaction tran, DbCommand cmd)
        {
            //declare variables
            XmlDocument ret = new XmlDocument();
            SqlDatabase sqlDb = (SqlDatabase)db;


            if (tran == null)
            {
                using (XmlReader reader = sqlDb.ExecuteXmlReader(cmd))
                {
                    ret.Load(reader);
                }
            }
            else
            {
                using (XmlReader reader = sqlDb.ExecuteXmlReader(cmd, tran))
                {
                    ret.Load(reader);
                }
            }


            return ret;

        }
        #endregion

        #region "LoadTable"
        public static int LoadTable(DbConnection connection, DbTransaction tran, string TableName, DataTable dtTableData)
        {
            int retVal = -1;

            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(connection as SqlConnection, SqlBulkCopyOptions.Default, tran as SqlTransaction))
            {

                bulkCopy.DestinationTableName = TableName;
                bulkCopy.BatchSize = dtTableData.Rows.Count;

                foreach (DataColumn column in dtTableData.Columns)
                {
                    bulkCopy.ColumnMappings.Add(column.ColumnName, column.ColumnName);
                }

                bulkCopy.WriteToServer(dtTableData);
                retVal = 1;
            }

            return retVal;
        }
        #endregion
    }
}
