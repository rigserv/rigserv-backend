﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RigServe.DAL
{
    public class MailDB : DataLayerBase
    {
        #region "GetTemplateByCode"
        public static DataTable GetTemplateByCode(string TemplateCode)
        {
            DbTransaction tran = null;
            DataTable retVal = new DataTable();

            Database db = DatabaseFactory.CreateDatabase("MS_TableConnectionString");

            DbCommand command = db.GetStoredProcCommand("kiosk.MailTemplate_GetByCode");

            db.AddInParameter(command, "@TemplateCode", DbType.String, TemplateCode);

            retVal = ExecuteDataTable(db, tran, command);

            return retVal;
        }
        #endregion ' GetTemplateByCode

    }
}
