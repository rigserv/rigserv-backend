﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RigServe.DAL
{
  public  class FacilityDB : DataLayerBase
    {
        public static int LoadData(Database db, DbTransaction tran)
        {
            int retVal;
           

            //create command and specify stored procedure name
            DbCommand command = db.GetStoredProcCommand("kiosk.Facility_LoadData");
            command.CommandTimeout = 0;
            //execute command
            retVal = ExecuteNonQuery(db, tran, command);

            return retVal;
        }

        public static DataTable GetAllFacilities()
        {
            DbTransaction tran = null;
            Database db = DatabaseFactory.CreateDatabase("MS_TableConnectionString");

            //create command and specify stored procedure name
            DbCommand command = db.GetStoredProcCommand("kiosk.Facility_Get");

            //execute command
            return ExecuteDataTable(db, tran, command);
        }

        public static DataTable GetFacility(string FacilityId)
        {
            DbTransaction tran = null;
            Database db = DatabaseFactory.CreateDatabase("MS_TableConnectionString");

            //create command and specify stored procedure name
            DbCommand command = db.GetStoredProcCommand("kiosk.Facility_GetById");

            // specify stored procedure parameters
            db.AddInParameter(command, "@FacilityId", DbType.AnsiString, FacilityId);

            //execute command
            return ExecuteDataTable(db, tran, command);
        }
    }
}
