﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RigServe.DAL
{
  public  class GeneralDB : DataLayerBase
    {
        public static int UpdateFirstRecord(Database db, DbTransaction tran)
        {
            int retVal;
           
            //create command and specify stored procedure name
            DbCommand command = db.GetStoredProcCommand("kiosk.UpdateFirstRecord");
            command.CommandTimeout = 0;
            //execute command
            retVal = ExecuteNonQuery(db, tran, command);

            return retVal;
        }


    }
}
