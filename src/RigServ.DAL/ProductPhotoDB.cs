﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RigServe.DAL
{
  public  class ProductPhotoDB : DataLayerBase
    {
        public static int LoadData(Database db, DbTransaction tran)
        {
            int retVal;
            //create command and specify stored procedure name
            DbCommand command = db.GetStoredProcCommand("kiosk.ProductPhoto_LoadData");

            //execute command
            retVal = ExecuteNonQuery(db, tran, command);

            return retVal;
        }
    }
}
