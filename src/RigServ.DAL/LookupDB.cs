﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RigServe.DAL
{
    public class LookupDB : DataLayerBase
    {
        
        public static DataTable GetActiveItemsByType(string LookupType)
        {
            DbTransaction tran = null;
            DataTable retVal;
            Database db = DatabaseFactory.CreateDatabase("MS_TableConnectionString");

            //create command and specify stored procedure name
            DbCommand command = db.GetStoredProcCommand("kiosk.Lookup_GetActiveItemsByType");

            // specify stored procedure parameters
            db.AddInParameter(command, "@LookupType", DbType.AnsiString, LookupType);


            //execute command
            retVal = ExecuteDataTable(db, tran, command);

            return retVal;
        }

        public static DataTable GetItem(string LookupType, string LookupValue)
        {
            DbTransaction tran = null;
            DataTable retVal;
            Database db = DatabaseFactory.CreateDatabase("MS_TableConnectionString");

            //create command and specify stored procedure name
            DbCommand command = db.GetStoredProcCommand("kiosk.Lookup_GetItem");

            // specify stored procedure parameters
            db.AddInParameter(command, "@LookupType", DbType.AnsiString, LookupType);
            db.AddInParameter(command, "@LookupValue", DbType.AnsiString, LookupValue);


            //execute command
            retVal = ExecuteDataTable(db, tran, command);

            return retVal;
        }

    }

}
