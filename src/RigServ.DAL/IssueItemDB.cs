﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RigServe.DAL
{
    public class IssueItemDB : DataLayerBase
    {
        public static DataTable GetDailyIssues(string FacilityId)
        {
            DbTransaction tran = null;
            Database db = DatabaseFactory.CreateDatabase("MS_TableConnectionString");

            //create command and specify stored procedure name
            DbCommand command = db.GetStoredProcCommand("kiosk.IssueItem_GetDailyItems");

            // specify stored procedure parameters
            db.AddInParameter(command, "@FacilityId", DbType.AnsiString, FacilityId);

            //execute command
            return ExecuteDataTable(db, tran, command);
        }
    }
}
