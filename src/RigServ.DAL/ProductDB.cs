﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RigServe.DAL
{
  public  class ProductDB : DataLayerBase
    {
        public static int LoadData(Database db, DbTransaction tran)
        {
            int retVal;
           
            //create command and specify stored procedure name
            DbCommand command = db.GetStoredProcCommand("kiosk.Product_LoadData");
            command.CommandTimeout = 0;
            //execute command
            retVal = ExecuteNonQuery(db, tran, command);

            return retVal;
        }


        //public static DataTable GetProductData()
        //{
        //    Database db = DatabaseFactory.CreateDatabase("ConnectionString");
        //    DbCommand command = db.GetStoredProcCommand("kiosk.Product_LoadData");
        //    return db.ExecuteDataSet(command).Tables[0];
        //}


        //public static int LoadProductData(DbConnection connection, DbTransaction trans, DataTable table)
        //{
        //    return LoadTable(connection, trans, "Product", table);
        //}
    }
}
