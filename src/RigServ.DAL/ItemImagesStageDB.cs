﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RigServe.DAL
{
  public  class ItemImagesStageDB : DataLayerBase
    {

        public static DataTable GetItemImagesStageData()
        {
            Database db = DatabaseFactory.CreateDatabase("ConnectionStringTwo");
            string sqlQuery = "SELECT * FROM ItemImages";
            DbCommand command = db.GetSqlStringCommand(sqlQuery);
            return db.ExecuteDataSet(command).Tables[0];
        }

        public static int DeleteData(Database db, DbTransaction tran)
        {
            int retVal;

            //create command and specify stored procedure name
            DbCommand command = db.GetStoredProcCommand("kiosk.ItemImagesStage_DeleteData");

            //execute command
            retVal = ExecuteNonQuery(db, tran, command);

            return retVal;
        }

        public static int LoadItemImagesStageData(DbConnection connection, DbTransaction trans, DataTable table)
        {
            return LoadTable(connection, trans, "ItemImagesStage", table);
        }

    }
}
