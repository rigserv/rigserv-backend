using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using Microsoft.Azure.Mobile.Server;
using Microsoft.Azure.Mobile.Server.Tables;
using RigServ.mobile.DataObjects;

namespace RigServ.mobile.Models
{
    public class MobileServiceContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to alter your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx
        //
        // To enable Entity Framework migrations in the cloud, please ensure that the 
        // service name, set by the 'MS_MobileServiceName' AppSettings in the local 
        // Web.config, is the same as the service name when hosted in Azure.

        private const string connectionStringName = "Name=MS_TableConnectionString";

        public MobileServiceContext() : base(connectionStringName)
        {
        }

       
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            /*
            modelBuilder.Conventions.Add(
                new AttributeToColumnAnnotationConvention<TableColumnAttribute, string>(
                    "ServiceTableColumn", (property, attributes) => attributes.Single().ColumnType.ToString()));
                    */
        }
      

        public System.Data.Entity.DbSet<Category> Category { get; set; }

        public System.Data.Entity.DbSet<RigServ.mobile.DataObjects.Product> Product { get; set; }

        public System.Data.Entity.DbSet<RigServ.mobile.DataObjects.Facility> Facilities { get; set; }

        public System.Data.Entity.DbSet<RigServ.mobile.DataObjects.RigInventory> Inventories { get; set; }

        public System.Data.Entity.DbSet<RigServ.mobile.DataObjects.InventoryLedger> InventoryLedgers { get; set; }

        public System.Data.Entity.DbSet<RigServ.mobile.DataObjects.Issue> Issues { get; set; }

        public System.Data.Entity.DbSet<RigServ.mobile.DataObjects.IssueItem> IssueItems { get; set; }

        public System.Data.Entity.DbSet<RigServ.mobile.DataObjects.Location> Locations { get; set; }

        public System.Data.Entity.DbSet<RigServ.mobile.DataObjects.LookUp> LookUps { get; set; }

        public System.Data.Entity.DbSet<RigServ.mobile.DataObjects.PickList> PickLists { get; set; }

        public System.Data.Entity.DbSet<RigServ.mobile.DataObjects.PickListItem> PickListItems { get; set; }

     
        public System.Data.Entity.DbSet<RigServ.mobile.DataObjects.ProductCategory> ProductCategories { get; set; }

        public System.Data.Entity.DbSet<RigServ.mobile.DataObjects.ProductPhoto> ProductPhotoes { get; set; }

        public System.Data.Entity.DbSet<RigServ.mobile.DataObjects.ReturnItem> ReturnItems { get; set; }


        public System.Data.Entity.DbSet<RigServ.mobile.DataObjects.Users> Users { get; set; }

        public System.Data.Entity.DbSet<RigServ.mobile.DataObjects.SyncHistory> SyncHistories { get; set; }
    }
}
