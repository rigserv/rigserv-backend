﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(RigServ.mobile.Startup))]

namespace RigServ.mobile
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureMobileApp(app);
        }
    }
}