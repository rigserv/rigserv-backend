﻿using Microsoft.Azure.Mobile.Server;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RigServ.mobile.DataObjects
{
    [Table("ProductCategory", Schema = "kiosk")]
    public class ProductCategory : EntityData
    {
        public string CategoryId { get; set; }
        public string ProductId { get; set; }
        public string FacilityId { get; set; }

        public string CategoryName { get; set; }
        public string ProductName { get; set; }

        public string DefaultProductImageUrl { get; set; }
        public string DefaultCategoryImageUrl { get; set; }

        public string Path { get; set; }
    }
}