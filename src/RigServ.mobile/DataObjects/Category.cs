﻿using Microsoft.Azure.Mobile.Server;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RigServ.mobile.DataObjects
{
    [Table("Category", Schema = "kiosk")]

    public class Category : EntityData
    {
        public string IndexCode { get; set; }

        public string Name { get; set; }

        public string ImageUrl { get; set; }

        public string ParentId { get; set; }

        public string FacilityId { get; set; }
    }
}