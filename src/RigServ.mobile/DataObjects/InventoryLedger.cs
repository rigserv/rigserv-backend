﻿using Microsoft.Azure.Mobile.Server;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RigServ.mobile.DataObjects
{
    [Table("InventoryLedger", Schema = "kiosk")]
    public class InventoryLedger : EntityData
    {
       
        public string FacilityId { get; set; }
        public int Quantity { get; set; }
        public string InventoryId { get; set; }
        public string EntityType { get; set; }
        public string EntityID { get; set; }

    }
}