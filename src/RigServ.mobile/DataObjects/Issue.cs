﻿using Microsoft.Azure.Mobile.Server;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RigServ.mobile.DataObjects
{
    [Table("Issue", Schema = "kiosk")]
    public class Issue : EntityData
    {
        public string EmployeeId { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string WorkOrder { get; set; }
        public string FacilityId { get; set; }
        public int IssueNo { get; set; }

        [NotMapped]
        public DateTimeOffset? LocalUpdatedDate { get; set; }
    }
}