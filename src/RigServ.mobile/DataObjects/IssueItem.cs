﻿using Microsoft.Azure.Mobile.Server;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RigServ.mobile.DataObjects
{
    [Table("IssueItem", Schema = "kiosk")]
    public class IssueItem : EntityData
    {
        public int Quantity { get; set; }
        public string InventoryId { get; set; }
        public string IssueId { get; set; }

        public string FacilityId { get; set; }
        public string ProductId { get; set; }
    }
}