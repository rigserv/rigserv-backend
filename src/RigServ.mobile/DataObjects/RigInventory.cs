﻿using Microsoft.Azure.Mobile.Server;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RigServ.mobile.DataObjects
{
    [Table("RigInventory", Schema = "kiosk")]
    public class RigInventory : EntityData
    {
        public string LocationId { get; set; }
        public string ProductId { get; set; }
        public string FacilityId { get; set; }
        public string Class { get; set; }
        public int Quantity { get; set; }        
    }
}