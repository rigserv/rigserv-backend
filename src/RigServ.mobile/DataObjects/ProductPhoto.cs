﻿using Microsoft.Azure.Mobile.Server;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RigServ.mobile.DataObjects
{
    [Table("ProductPhoto", Schema = "kiosk")]
    public class ProductPhoto : EntityData
    {
        public string ProductId { get; set; }

        public string ImageUrl { get; set; }

        public bool IsDefault { get; set; }

        public string FacilityId { get; set; }


        [NotMapped]
        public string ItemImagesId { get; set; }
    }
}