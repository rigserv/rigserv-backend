﻿using Microsoft.Azure.Mobile.Server;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RigServ.mobile.DataObjects
{
    [Table("PickListItem", Schema = "kiosk")]
    public class PickListItem : EntityData
    {
        public int Quantity { get; set; }
        public string PickListId { get; set; }
        public string ProductId { get; set; }
        public string Class { get; set; }
        public string FacilityId { get; set; }
        public string ProductName { get; set; }
        public string ICN { get; set; }
        public string RIN { get; set; }
        public int InventoryQuantity { get; set; }
        public string ProductImageUrl { get; set; }
    }
}