﻿using Microsoft.Azure.Mobile.Server;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RigServ.mobile.DataObjects
{
    [Table("Facility", Schema = "kiosk")]
    public class Facility : EntityData
    {
     
        public string Description { get; set; }
        public string EmailAddress { get; set; }

    }
}