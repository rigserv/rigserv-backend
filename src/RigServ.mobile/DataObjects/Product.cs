﻿using Microsoft.Azure.Mobile.Server;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RigServ.mobile.DataObjects
{
    [Table("Product", Schema = "kiosk")]
    public class Product : EntityData
    {
        public string Name { get; set; }
        public string RIN { get; set; }
        public string PartNumber { get; set; }
        public string Manufacturer { get; set; }
        public string ManufacturerCode { get; set; }
        public string DefaultImageUrl { get; set; }
        public string FacilityId { get; set; }
        public string ICN { get; set; }
        public string UOM { get; set; }

        [NotMapped]
        public string CatalogId { get; set; }
    }
}