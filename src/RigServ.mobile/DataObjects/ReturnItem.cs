﻿using Microsoft.Azure.Mobile.Server;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RigServ.mobile.DataObjects
{
    [Table("ReturnItem", Schema = "kiosk")]
    public class ReturnItem : EntityData
    {
        public int Quantity { get; set; }
        public string IssueItemId { get; set; }
        public string LocationId { get; set; }
        public string FacilityId { get; set; }
    }
}