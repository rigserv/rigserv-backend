﻿using Microsoft.Azure.Mobile.Server;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RigServ.mobile.DataObjects
{
    [Table("Users", Schema = "kiosk")]
    public class Users : EntityData
    {
        public string FacilityId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}