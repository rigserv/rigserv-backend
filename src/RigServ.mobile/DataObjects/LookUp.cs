﻿using Microsoft.Azure.Mobile.Server;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RigServ.mobile.DataObjects
{
    [Table("LookUp", Schema = "kiosk")]
    public class LookUp : EntityData
    {
        public string LookupType { get; set; }
        public string LookupValue { get; set; }
        public string LookupDesc { get; set; }
    }
}