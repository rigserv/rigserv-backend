﻿using Microsoft.Azure.Mobile.Server;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RigServ.mobile.DataObjects
{
    [Table("Location", Schema = "kiosk")]
    public class Location : EntityData
    {
        public string Name { get; set; }
        public string FacilityId { get; set; }
        public bool IsDefaultReturnLocation { get; set; }
        public string Email { get; set; }
    }
}