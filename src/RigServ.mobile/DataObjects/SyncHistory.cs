﻿using Microsoft.Azure.Mobile.Server;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RigServ.mobile.DataObjects
{
    [Table("SyncHistory", Schema = "kiosk")]
    public class SyncHistory: EntityData
    {
        public DateTime SyncDate { get; set; }
        public int Status { get; set; }
        public string Description { get; set; }

        public string FacilityId { get; set; }
    }
}