﻿using Microsoft.Azure.Mobile.Server;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RigServ.mobile.DataObjects
{
    [Table("PickList", Schema = "kiosk")]
    public class PickList : EntityData
    {
        public string FacilityId { get; set; }
    }
}