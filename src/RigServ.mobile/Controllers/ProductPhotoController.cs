﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using RigServ.mobile.DataObjects;
using RigServ.mobile.Models;
using System.Collections.Generic;
using System.Security.Claims;

namespace RigServ.mobile.Controllers
{
    //[Authorize]
    [EnableQuery(PageSize = 1000)]
    public class ProductPhotoController : TableController<ProductPhoto>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            MobileServiceContext context = new MobileServiceContext();
            DomainManager = new EntityDomainManager<ProductPhoto>(context, Request, enableSoftDelete: true);
        }

        // GET tables/ProductPhoto
        public IQueryable<ProductPhoto> GetAllProductPhoto()
        {
            //var identity = (ClaimsIdentity)User.Identity;
            //IEnumerable<Claim> claims = identity.Claims;

            //var subclaim = claims.First();

            //var FacilityId = subclaim.Value;




            //return Query().Where(x => x.FacilityId == FacilityId);
            return Query();
        }

        // GET tables/ProductPhoto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<ProductPhoto> GetProductPhoto(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/ProductPhoto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<ProductPhoto> PatchProductPhoto(string id, Delta<ProductPhoto> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/ProductPhoto
        public async Task<IHttpActionResult> PostProductPhoto(ProductPhoto item)
        {
            ProductPhoto current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/ProductPhoto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteProductPhoto(string id)
        {
             return DeleteAsync(id);
        }
    }
}
