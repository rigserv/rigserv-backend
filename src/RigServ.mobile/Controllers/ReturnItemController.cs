﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using RigServ.mobile.DataObjects;
using RigServ.mobile.Models;
using System.Collections.Generic;
using System.Security.Claims;

namespace RigServ.mobile.Controllers
{
    [Authorize]
    [EnableQuery(PageSize = 1000)]
    public class ReturnItemController : TableController<ReturnItem>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            MobileServiceContext context = new MobileServiceContext();
            DomainManager = new EntityDomainManager<ReturnItem>(context, Request, enableSoftDelete: true);
        }

        // GET tables/ReturnItem
        public IQueryable<ReturnItem> GetAllReturnItem()
        {
            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> claims = identity.Claims;

            var subclaim = claims.First();

            var FacilityId = subclaim.Value;




            return Query().Where(x => x.FacilityId == FacilityId);
        }

        // GET tables/ReturnItem/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<ReturnItem> GetReturnItem(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/ReturnItem/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<ReturnItem> PatchReturnItem(string id, Delta<ReturnItem> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/ReturnItem
        public async Task<IHttpActionResult> PostReturnItem(ReturnItem item)
        {
            ReturnItem current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/ReturnItem/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteReturnItem(string id)
        {
             return DeleteAsync(id);
        }
    }
}
