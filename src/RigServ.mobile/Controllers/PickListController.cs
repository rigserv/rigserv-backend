﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using RigServ.mobile.DataObjects;
using RigServ.mobile.Models;
using System.Collections.Generic;
using System.Security.Claims;

namespace RigServ.mobile.Controllers
{
    [Authorize]
    [EnableQuery(PageSize = 1000)]
    public class PickListController : TableController<PickList>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            MobileServiceContext context = new MobileServiceContext();
            DomainManager = new EntityDomainManager<PickList>(context, Request, enableSoftDelete: true);
        }

        // GET tables/PickList
        public IQueryable<PickList> GetAllPickList()
        {
            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> claims = identity.Claims;


            var subclaim = claims.First();

            var FacilityId = subclaim.Value;

            return Query().Where(x => x.FacilityId == FacilityId);
        }

        // GET tables/PickList/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<PickList> GetPickList(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/PickList/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<PickList> PatchPickList(string id, Delta<PickList> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/PickList
        public async Task<IHttpActionResult> PostPickList(PickList item)
        {
            PickList current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/PickList/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeletePickList(string id)
        {
             return DeleteAsync(id);
        }
    }
}
