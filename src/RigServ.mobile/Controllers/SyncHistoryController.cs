﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using RigServ.mobile.DataObjects;
using RigServ.mobile.Models;
using System.Collections.Generic;
using System.Security.Claims;

namespace RigServ.mobile.Controllers
{
    [Authorize]
    [EnableQuery(PageSize = 1000)]
    public class SyncHistoryController : TableController<SyncHistory>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            MobileServiceContext context = new MobileServiceContext();
            DomainManager = new EntityDomainManager<SyncHistory>(context, Request, enableSoftDelete: true);
        }

        // GET tables/SyncHistory
        public IQueryable<SyncHistory> GetAllSyncHistory()
        {
            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> claims = identity.Claims;

            var subclaim = claims.First();

            var FacilityId = subclaim.Value;




            return Query().Where(x => x.FacilityId == FacilityId);
        }

        // GET tables/SyncHistory/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<SyncHistory> GetSyncHistory(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/SyncHistory/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<SyncHistory> PatchSyncHistory(string id, Delta<SyncHistory> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/SyncHistory
        public async Task<IHttpActionResult> PostSyncHistory(SyncHistory item)
        {
            SyncHistory current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/SyncHistory/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteSyncHistory(string id)
        {
             return DeleteAsync(id);
        }
    }
}
