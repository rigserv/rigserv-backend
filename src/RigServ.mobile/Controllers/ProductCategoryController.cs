﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using RigServ.mobile.DataObjects;
using RigServ.mobile.Models;
using System.Security.Claims;
using System.Collections.Generic;

namespace RigServ.mobile.Controllers
{
    [Authorize]
    [EnableQuery(PageSize = 1000)]
    public class ProductCategoryController : TableController<ProductCategory>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            MobileServiceContext context = new MobileServiceContext();
            DomainManager = new EntityDomainManager<ProductCategory>(context, Request, enableSoftDelete: true);
        }

        // GET tables/ProductCategory
        public IQueryable<ProductCategory> GetAllProductCategory()
        {
            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> claims = identity.Claims;


            var subclaim = claims.First();

            var FacilityId = subclaim.Value;

            return Query().Where(x => x.FacilityId == FacilityId); 
        }

        // GET tables/ProductCategory/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<ProductCategory> GetProductCategory(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/ProductCategory/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<ProductCategory> PatchProductCategory(string id, Delta<ProductCategory> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/ProductCategory
        public async Task<IHttpActionResult> PostProductCategory(ProductCategory item)
        {
            ProductCategory current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/ProductCategory/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteProductCategory(string id)
        {
             return DeleteAsync(id);
        }
    }
}
