﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using RigServ.mobile.DataObjects;
using RigServ.mobile.Models;
using System.Collections.Generic;
using System.Security.Claims;

namespace RigServ.mobile.Controllers
{
    [Authorize]
    [EnableQuery(PageSize = 1000)]
    public class IssueItemController : TableController<IssueItem>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            MobileServiceContext context = new MobileServiceContext();
            DomainManager = new EntityDomainManager<IssueItem>(context, Request, enableSoftDelete: true);
        }

        // GET tables/IssueItem
        public IQueryable<IssueItem> GetAllIssueItem()
        {
            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> claims = identity.Claims;

            var subclaim = claims.First();

            var FacilityId = subclaim.Value;




            return Query().Where(x => x.FacilityId == FacilityId);
        }

        // GET tables/IssueItem/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<IssueItem> GetIssueItem(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/IssueItem/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<IssueItem> PatchIssueItem(string id, Delta<IssueItem> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/IssueItem
        public async Task<IHttpActionResult> PostIssueItem(IssueItem item)
        {
            IssueItem current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/IssueItem/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteIssueItem(string id)
        {
             return DeleteAsync(id);
        }
    }
}
