﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using RigServ.mobile.DataObjects;
using RigServ.mobile.Models;
using System.Security.Claims;
using System.Collections.Generic;

namespace RigServ.mobile.Controllers
{
    [Authorize]
    [EnableQuery(PageSize =1000)]
    public class RigInventoryController : TableController<RigInventory>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            MobileServiceContext context = new MobileServiceContext();
            DomainManager = new EntityDomainManager<RigInventory>(context, Request, enableSoftDelete: true);
        }

        // GET tables/Inventory
        public IQueryable<RigInventory> GetAllInventory()
        {
            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> claims = identity.Claims;

            var subclaim = claims.First();

            var FacilityId = subclaim.Value;

              
         

            return Query().Where(x => x.FacilityId == FacilityId);
           
        }

        // GET tables/Inventory/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<RigInventory> GetInventory(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/Inventory/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<RigInventory> PatchInventory(string id, Delta<RigInventory> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/Inventory
        public async Task<IHttpActionResult> PostInventory(RigInventory item)
        {
            RigInventory current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/Inventory/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteInventory(string id)
        {
             return DeleteAsync(id);
        }
    }
}
