﻿using System.Web.Http;
using RigServ.mobile.Models;
using System;
using Newtonsoft.Json;
using System.Security.Claims;
using System.IdentityModel.Tokens;
using Microsoft.Azure.Mobile.Server.Login;
using System.Configuration;

namespace RigServ.mobile.Controllers
{
    [Route(".auth/login/custom")]
    public class CustomAuthController : ApiController
    {
        private MobileServiceContext db;
        private string signingKey, audience, issuer;

        public CustomAuthController()
        {
            db = new MobileServiceContext();

            var website = string.Empty;

#if DEBUG
            signingKey = ConfigurationManager.AppSettings["WEBSITE_AUTH_SIGNING_KEY"];
            website = ConfigurationManager.AppSettings["WEBSITE_HOSTNAME"];
            
            audience = website;
            issuer = website;
#else
            signingKey = Environment.GetEnvironmentVariable("WEBSITE_AUTH_SIGNING_KEY");
            website = Environment.GetEnvironmentVariable("WEBSITE_HOSTNAME");

            //audience = $"https://{website}/";
            //issuer = $"https://{website}/";
            audience = string.Format("https://{0}/", website);
            issuer = string.Format("https://{0}/", website);
            
#endif


        }

        [HttpPost]
        public IHttpActionResult Post([FromBody] User body)
        {
            JwtSecurityToken token = null;
            try
            {
                if (body == null || body.password == null || body.username == null ||
                body.password.Length == 0 || body.username.Length == 0)
                {
                    return BadRequest(); ;
                }

                if (!IsValidUser(body))
                {
                    return Unauthorized();
                }

                var claims = new Claim[]
                {
                new Claim(JwtRegisteredClaimNames.Sub, body.password),
                new Claim(JwtRegisteredClaimNames.Email, body.username)
                };

                token = AppServiceLoginHandler.CreateToken(
                    claims, signingKey, audience, issuer, TimeSpan.FromDays(300) );

                //DateTime(9999, 12, 31).Subtract(DateTime.Now);


            }
            catch(Exception ex)
            {

                throw ex;
            }

            return Ok(new LoginResult()
            {
                AuthenticationToken = token.RawData,
                User = new LoginResultUser { Email = body.username, FacilityId = body.password }
            });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool IsValidUser(User user)
        {
            return true;
            //return db.Users.Count(u => u.Username.Equals(user.Username) && u.Password.Equals(user.Password)) > 0;
        }
    }

    public class LoginResult
    {
        [JsonProperty(PropertyName = "authenticationToken")]
        public string AuthenticationToken { get; set; }

        [JsonProperty(PropertyName = "facility")]
        public LoginResultUser User { get; set; }
    }

    public class LoginResultUser
    {
        [JsonProperty(PropertyName = "FacilityId")]
        public string FacilityId { get; set; }
        public string Email { get; set; }
    }
}
