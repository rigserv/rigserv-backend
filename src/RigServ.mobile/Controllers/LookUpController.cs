﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using RigServ.mobile.DataObjects;
using RigServ.mobile.Models;

namespace RigServ.mobile.Controllers
{
    [Authorize]
    [EnableQuery(PageSize = 1000)]
    public class LookUpController : TableController<LookUp>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            MobileServiceContext context = new MobileServiceContext();
            DomainManager = new EntityDomainManager<LookUp>(context, Request, enableSoftDelete: true);
        }

        // GET tables/LookUp
        public IQueryable<LookUp> GetAllLookUp()
        {
            return Query(); 
        }

        // GET tables/LookUp/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<LookUp> GetLookUp(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/LookUp/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<LookUp> PatchLookUp(string id, Delta<LookUp> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/LookUp
        public async Task<IHttpActionResult> PostLookUp(LookUp item)
        {
            LookUp current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/LookUp/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteLookUp(string id)
        {
             return DeleteAsync(id);
        }
    }
}
