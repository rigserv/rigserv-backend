﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using RigServ.mobile.DataObjects;
using RigServ.mobile.Models;
using System.Collections.Generic;
using System.Security.Claims;

namespace RigServ.mobile.Controllers
{
    public class IssueController : TableController<Issue>
    {
        [Authorize]
        [EnableQuery(PageSize = 1000)]
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            MobileServiceContext context = new MobileServiceContext();
            DomainManager = new EntityDomainManager<Issue>(context, Request, enableSoftDelete: true);
        }

        // GET tables/Issue
        public IQueryable<Issue> GetAllIssue()
        {
            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> claims = identity.Claims;

            var subclaim = claims.First();

            var FacilityId = subclaim.Value;




            return Query().Where(x => x.FacilityId == FacilityId);
        }

        // GET tables/Issue/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<Issue> GetIssue(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/Issue/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<Issue> PatchIssue(string id, Delta<Issue> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/Issue
        public async Task<IHttpActionResult> PostIssue(Issue item)
        {
            Issue current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/Issue/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteIssue(string id)
        {
             return DeleteAsync(id);
        }
    }
}
