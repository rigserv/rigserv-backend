﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using RigServ.mobile.DataObjects;
using RigServ.mobile.Models;
using System.Security.Claims;
using System.Collections.Generic;

namespace RigServ.mobile.Controllers
{
    [Authorize]
    [EnableQuery(PageSize = 1000)]
    public class InventoryLedgerController : TableController<InventoryLedger>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            MobileServiceContext context = new MobileServiceContext();
            DomainManager = new EntityDomainManager<InventoryLedger>(context, Request, enableSoftDelete: true);
        }

        // GET tables/InventoryLedger
        public IQueryable<InventoryLedger> GetAllInventoryLedger()
        {
            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> claims = identity.Claims;

            var subclaim = claims.First();

            var FacilityId = subclaim.Value;


            return Query().Where(x => x.FacilityId == FacilityId);
        }

        // GET tables/InventoryLedger/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<InventoryLedger> GetInventoryLedger(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/InventoryLedger/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<InventoryLedger> PatchInventoryLedger(string id, Delta<InventoryLedger> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/InventoryLedger
        public async Task<IHttpActionResult> PostInventoryLedger(InventoryLedger item)
        {
            InventoryLedger current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/InventoryLedger/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteInventoryLedger(string id)
        {
             return DeleteAsync(id);
        }
    }
}
