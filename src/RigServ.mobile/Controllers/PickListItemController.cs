﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using RigServ.mobile.DataObjects;
using RigServ.mobile.Models;
using System.Collections.Generic;
using System.Security.Claims;

namespace RigServ.mobile.Controllers
{
    [Authorize]
    [EnableQuery(PageSize = 1000)]
    public class PickListItemController : TableController<PickListItem>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            MobileServiceContext context = new MobileServiceContext();
            DomainManager = new EntityDomainManager<PickListItem>(context, Request, enableSoftDelete: true);
        }

        // GET tables/PickListItem
        public IQueryable<PickListItem> GetAllPickListItem()
        {
            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> claims = identity.Claims;

            var subclaim = claims.First();

            var FacilityId = subclaim.Value;

            return Query().Where(x => x.FacilityId == FacilityId);
        }

        // GET tables/PickListItem/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<PickListItem> GetPickListItem(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/PickListItem/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<PickListItem> PatchPickListItem(string id, Delta<PickListItem> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/PickListItem
        public async Task<IHttpActionResult> PostPickListItem(PickListItem item)
        {
            PickListItem current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/PickListItem/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeletePickListItem(string id)
        {
             return DeleteAsync(id);
        }
    }
}
