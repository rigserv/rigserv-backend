﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Eminent.Job.Helper;
using log4net;

namespace RigServe.Job
{
    class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            log4net.Config.XmlConfigurator.Configure();


            if (args.Length > 0)
            {
                switch (args[0].ToLower())
                {
                    case "/debug":
                        JobRunner runnerForm = new JobRunner(args, GetSwitches());
                        ((log4net.Repository.Hierarchy.Hierarchy)log4net.LogManager.GetRepository()).Root.AddAppender(runnerForm);
                        Application.Run(runnerForm);
                        break;
                    case "/?":
                    case "/help":
                        JobRunner.DisplayHelp(GetSwitches());
                        break;
                    default:
                        JobRunner.RunJob(args, GetSwitches());
                        break;
                }

            }
            else
            {
                JobRunner.RunJob(args, GetSwitches());
            }

        }

        public static Dictionary<string, JobSwitch> GetSwitches()
        {
            Dictionary<string, JobSwitch> switches = new Dictionary<string, JobSwitch>();

            JobSwitch defaultSwitch = new JobSwitch("RigServeLoad", "Load RigServe tables", new RigServLoadStageData());
            JobSwitch IssueEmail = new JobSwitch("IssueEmail", "Send Daily Issues and Returns Report Email", new IssueDailyEmail());

            switches.Add(defaultSwitch.Switch, defaultSwitch);
            switches.Add(IssueEmail.Switch, IssueEmail);
            return switches;

        }

        public static void LogException(Exception ex)
        {
            LogException(ex, true);
        }


        public static void LogException(Exception ex, bool rethrow)
        {
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            log.Error(ex);

            if (rethrow)
            {
                throw ex;
            }
        }

        public static void LogInfo(string Message)
        {
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            log.Info(Message);

        }

        public static void LogDebug(string Message)
        {

            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            log.Debug(Message);

        }

    }
}
