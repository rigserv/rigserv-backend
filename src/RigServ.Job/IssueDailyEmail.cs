﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RigServ.BLL;
using Eminent.Job.Helper;
using System.Data;

namespace RigServe.Job
{
    public class IssueDailyEmail : IJob
    {

        public void Execute(string[] args)
        {

            try
            {
                Program.LogInfo("Start - Daily Issue Report Notification");
                IssueEmail.RunDailyIssueNotification();
                Program.LogInfo("Finished - Daily Issue Report Notification");
            }
            catch (Exception ex)
            {
                Program.LogException(ex, false);
            }
        }
    }
}
