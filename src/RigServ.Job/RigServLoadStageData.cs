﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RigServ.BLL;
using Eminent.Job.Helper;
using System.Data;

namespace RigServe.Job
{
 public class RigServLoadStageData : IJob
    {
        #region IJob Members

        public void Execute(string[] args)
        {

            DataTable dt=new DataTable();
            try
            {
                Program.LogInfo("Start - Loading all RigServ tables");


                Program.LogInfo("Begin Load Category Stage Table");
                RigServLoad.CategoryStageLoad();
                Program.LogInfo("End Load Category Stage Table");

                Program.LogInfo("Begin Load Facility Table");
                RigServLoad.FacilityLoad();
                Program.LogInfo("End Load Facility Table");

                Program.LogInfo("Begin Load Location Table");
                RigServLoad.LocationLoad();
                Program.LogInfo("End Load Location Table");

                Program.LogInfo("Begin Load Product Table");
                RigServLoad.ProductLoad();
                Program.LogInfo("End Load Product Table");

                Program.LogInfo("Begin Load Product Photo Table");
                RigServLoad.ProductPhotoLoad();
                Program.LogInfo("End Load Product Photo Table");

                Program.LogInfo("Begin Load ProductCategory Table");
                RigServLoad.ProductCategoryLoad();
                Program.LogInfo("End Load ProductCategory Table");

                Program.LogInfo("Begin Load RigInventory Table");
                RigServLoad.RigInventoryLoad();
                Program.LogInfo("End Load RigInventory Table");

                Program.LogInfo("Begin Load Category Table");
                RigServLoad.CategoryLoad();
                Program.LogInfo("End Load Category Table");

                Program.LogInfo("Begin Load Users Table");
                RigServLoad.UsersLoad();
                Program.LogInfo("End Load Users Table");

                Program.LogInfo("Begin Update First Record on Tables");//Category, ProductCategory, Product, Location, RigInventory
                RigServLoad.UpdateFirstRecord();
                Program.LogInfo("Begin Update First Record on Tables");


                Program.LogInfo("Finished - Loading all RigServ tables"); ;
            }
            catch (Exception ex)
            {
                Program.LogException(ex, false);
            }
        }

        #endregion
    }
}
